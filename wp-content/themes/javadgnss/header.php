<!DOCTYPE html>
<html <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
															  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
					'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
									})(window,document,'script','dataLayer','GTM-K8K8D5F');</script>
		<!-- End Google Tag Manager -->

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K8K8D5F"
						  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		<?php wp_body_open(); ?>
		<a class="screen-reader-text" href="#content"><?php _e( 'Skip to Main Content', 'bp-theme' ); ?></a>
		<div class="wrapper">
			<div class="offcanvas offcanvas-end" tabindex="-1" id="rightFunnel" aria-labelledby="offcanvasRightLabel">
				<div class="offcanvas-header">
					<button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
				</div>
				<div class="offcanvas-body">
					<?php wp_nav_menu( array( 'container_class' => 'menuLink', 'theme_location' => 'primary_menu','walker' => new submenu_wrap() ) ); ?>
				</div>
			</div>
			<header class="header">
				<div class="head_top">
					<div class="container">
						<div class="d-flex">
							<div class="head_socialLinks">
								<ul>
									<?php if(get_field('linked_in','option')):?>
									<li><a href="<?php echo get_field('linked_in','option');?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
									<?php endif;
									if(get_field('twitter','option')):?>
									<li><a href="<?php echo get_field('twitter','option');?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
									<?php endif;
									if(get_field('facebook','option')):?>
									<li><a href="<?php echo get_field('facebook','option');?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
									<?php endif;?>
								</ul>
							</div>	
							<div class="header_right">
								<ul>
									<?php $link = get_field('header_top_button','option');
									if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
									?>
									 <li><a href="<?php// echo esc_url( $link_url ); ?>"target="<?php //echo esc_attr( $link_target ); ?>"><?php //echo esc_html( $link_title ); ?></a></li>
									<?php endif;?>
									<!-- 								<li><a href="javascript:void(0);"><i class="fas fa-search"></i></a></li> -->
									<li>
										<div class="search-wrapper">
											<div class="input-holder">
												<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>" >
													<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="search-input" placeholder="Search" />
													<input type="submit" id="searchsubmit" value="" class="search-icon"/>
												</form>

											</div>
											<span class="close" onclick="searchToggle(this, event);"></span>
										</div>
									</li>
									<li>
										<?php $acc_link = get_field('account_login_link','option');?>
										<a href="<?php echo esc_url( $acc_link ); ?>" target=”_blank”><i class="fas fa-user-alt"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="head_bottom">
					<div class="container">
						<div class="menu_grid">
							<a class="brand_logo" href="<?php echo home_url( '/' ); ?>"><?php 
								$main_logo = get_field('header_logo','option');
								if($main_logo):
									?>
									<!-- <img src="<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) );?>" alt="<?php echo get_bloginfo( 'name' );?>"> -->
									<span class="black-logo-svg"><?php echo file_get_contents( $main_logo['url'] ); ?></span>
								<?php endif;
								
								$alt_logo = get_field('header_alternate_logo','option');
								if($alt_logo):
									?>
									<!-- <img src="<?php echo $alt_logo['url'];?>" alt="<?php echo $alt_logo['alt'];?>" style="height: 360px;"> -->
									<span class="white-logo-svg"><?php echo file_get_contents( $alt_logo['url'] ); ?></span>
								<?php endif;?>
							</a>
							<nav class = "navbar">
							<?php wp_nav_menu( array( 'container_class' => 'menuLink', 'theme_location' => 'primary_menu','walker' => new submenu_wrap() ) ); ?>
							<div class="butgurBtn">
									<div class="burgurline" type="button" data-bs-toggle="offcanvas" data-bs-target="#rightFunnel" aria-controls="rightFunnel">
										<span></span>
										<span></span>
										<span></span>
									</div>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</header>
			<main id="content">