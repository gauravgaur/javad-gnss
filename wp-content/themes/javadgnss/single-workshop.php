<?php get_header();
$single_workshop_back_cta = get_field('single_workshop_back_cta', 'option');
$small_heading_workshop = get_field('small_heading_workshop');
//$select_date = get_field('select_date');
$categories = get_the_terms( $post->ID, workshop_categories);

$banner_cta_workshop = get_field('banner_cta_workshop');
$workshop_highlights_heading = get_field('workshop_highlights_heading');
$workshop_highlights_copy = get_field('workshop_highlights_copy');
$workshop_presenter_heading = get_field('workshop_presenter_heading');
$info_heading_workshop = get_field('info_heading_workshop');
$info_copy_workshop = get_field('info_copy_workshop');
$info_cta_workshop = get_field('info_cta_workshop');
$ws_start_date = get_field('ws_start_date');
$ws_end_date = get_field('ws_end_date');
$cal_ws_start = date("Ymd", strtotime($ws_start_date));
$cal_ws_end = date("Ymd", strtotime($ws_end_date));
$ws_desc = get_the_excerpt();

?>
<section class="simple_banner">
			<div class="container">
				<div class="banner_content press_banner" data-aos="fade-up" data-aos-duration="1500">
					<?php if($small_heading_workshop): ?><span><?php echo $small_heading_workshop; ?></span><?php endif; ?>
					<h1><?php echo get_the_title(); ?></h1>
					<?php if($ws_start_date): ?><p><?php echo $ws_start_date; ?></p><?php endif; ?>
					<?php if(!empty($banner_cta_workshop)): ?><a class="green_btn" href="<?php echo $banner_cta_workshop['url']; ?>" target="<?php echo $banner_cta_workshop['target']; ?>"> <?php echo $banner_cta_workshop['title']; ?> </a><?php endif; ?>
					<ul class="bread_nav">
						<li><a href="javascript:void(0);">Workshops <i class="fas fa-chevron-right"></i></a></li>
						<li><?php echo get_the_title(); ?></li>
					</ul>
				</div>
			</div>
		</section>



		<section class="press_contents">
			<div class="container">
				<div class="press_information" data-aos="fade-up" data-aos-duration="1500">
					<div class="button_grid">
						<?php if(!empty($single_workshop_back_cta)): ?><div class="left_btn">
							<a class="green_btn" href="<?php echo $single_workshop_back_cta['url']; ?>"><i class="fas fa-chevron-left"></i> <?php echo $single_workshop_back_cta['title']; ?></a>
						</div><?php endif; ?>
						<?php if($categories[0]->slug !='recorded'):?>
						<div class="right_btnGroup">
							<a class="gray_btn" href="http://www.google.com/calendar/event?action=TEMPLATE&text=<?php echo urlencode(get_the_title()); ?>&dates=<?php echo $cal_ws_start; ?>T010000Z/<?php echo $cal_ws_end; ?>T020000Z&details=<?php echo urlencode($ws_desc); ?>&trp=false&sprop=&sprop=name:" target="_blank" rel="nofollow"> + Google Calendar</a>
							
							<a class="gray_btn" download="" title="+ Add to iCalendar" href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0ADTSTART:<?php echo $cal_ws_start; ?>T134500Z%0ADTEND:<?php echo $cal_ws_end; ?>T141500Z%0ASUMMARY:<?php echo rawurlencode(get_the_title()); ?>%0ADESCRIPTION:<?php echo rawurlencode($ws_desc); ?>%0AEND:VEVENT%0AEND:VCALENDAR%0A" target="_blank" rel="noopener noreferrer" >+ Add to iCalendar</a> 
						</div><?php endif; ?>
					</div>
					<div class="workshops_section">
						<h3><?php echo get_the_title(); ?></h3>
						<?php if($ws_start_date): ?><h4><?php echo $ws_start_date; ?></h4><?php endif; ?>
						<p><?php echo get_the_content(); ?></p>
						<?php if($workshop_highlights_heading): ?><h5><?php echo $workshop_highlights_heading; ?></h5><?php endif; ?>
						<?php if($workshop_highlights_copy): echo $workshop_highlights_copy; endif; ?>
												
						<div class="Presenters_grid">
							<?php if($workshop_presenter_heading): ?><h5><?php echo $workshop_presenter_heading; ?></h5><?php endif; ?>
							<?php if( have_rows('add_speakers') ): ?>
							<ul>
								<?php while( have_rows('add_speakers') ) : the_row();
									$speaker_name = get_sub_field('speaker_name');
									$speaker_title = get_sub_field('speaker_title');
									$speaker_image = get_sub_field('speaker_image');
								?>
								<li>
									<div class="pro_inner">
										<?php if(!empty($speaker_image)): ?><div class="profile_img">
											<img src="<?php echo $speaker_image['url']; ?>" alt="<?php echo $speaker_image['alt']; ?>">
										</div><?php endif; ?>
										<div class="user_name">
											<?php if($speaker_name): ?><h6><?php echo $speaker_name; ?></h6><?php endif; ?>
											<?php if($speaker_title): ?><p><?php echo $speaker_title; ?></p><?php endif; ?>
										</div>
									</div>
								</li>
								<?php endwhile; ?>
							</ul>
							<?php endif; ?>
						</div>

						<?php if($info_heading_workshop): ?><h5><?php echo $info_heading_workshop; ?></h5><?php endif; ?>
						<?php if($info_copy_workshop): echo $info_copy_workshop; endif; ?>
						<?php if($info_cta_workshop): ?><a class="green_btn" href="<?php echo $info_cta_workshop['url']; ?>"><?php echo $info_cta_workshop['title']; ?></a><?php endif; ?>
					</div>
				</div>
			</div>
		</section>

<?php get_footer(); ?>