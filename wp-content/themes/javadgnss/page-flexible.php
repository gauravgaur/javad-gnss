<?php
/*Template Name: Flexible Template*/
/*Template Post Type: page, applications, products*/
get_header();
if(have_rows('page_content')):
    $block_count = 1;
    while(have_rows('page_content')): the_row();
        ACF_Layout::render(get_row_layout(), $block_count);
        $block_count++;     
    endwhile;
endif;
/**
*   Datasheets
*/
/*if(is_array(get_field('files_list', get_the_ID()))){
    $get_files = array_filter(get_field('files_list', get_the_ID()));        
    if(!empty($get_files)){
        echo '<section class="datasheets"><div class="container"><div class="user_profile_gallery"><ul>'; 
        foreach($get_files as $key => $file) {
            foreach( $file as $key1 => $sub_file):?>
                <?php 
                  $pdf_id = '';
                  $pdf_name =''; 
                  if($key == 'data_sheets_group'){
                      $tileName = 'Data Sheet';
                      $pdf_name = $sub_file['data_sheets'];
                      $pdf_id = $sub_file['data_sheets_file_upload'];
                  } elseif($key == 'user_manual_group'){
                      $tileName = 'User Manual';
                      $pdf_name = $sub_file['user_manual'];
                      $pdf_id = $sub_file['user_manual_file_upload'];
                  } else {
                      $tileName = 'Software Updates';
                      $pdf_name = $sub_file['software_updates'];
                      $pdf_id = $sub_file['software_updates_file_upload'];
                  }?>
                <li class="file_items <?php echo (empty($pdf_name) ? 'd-none' : '');?>" >
                  <div class="main_box">
                    <div class="user_head">
                      <div class="cross_img">
                        <?php if(has_post_thumbnail(get_the_ID())):?>
                            <?php $imageUrl = get_the_post_thumbnail_url(get_the_ID(), 'full');?>
                            <img src="<?php echo $imageUrl;?>" alt="<?php echo get_the_title(get_the_ID());?>" />
                          <?php else:?>
                            <img src="http://javadgnss.com/wp-content/uploads/2022/03/jdrop.png" alt="img">
                          <?php endif;?>
                      </div>              
                      <span>
                          <?php echo $tileName;?>
                      </span>
                    </div>
                    <div class="about_user">
                      <p><a class="btn green_btn px-3 py-1" href="<?php echo wp_get_attachment_url($pdf_id);?>" target="_blank">
                      <?php echo ($pdf_name) ? $pdf_name : 'Download Pdf';?>
                      </a></p>
                    </div>
                  </div>
                </li>    
            <?php
            endforeach;
        }
        echo '</ul></div></div></section>';  
    }
}*/

get_footer();?>