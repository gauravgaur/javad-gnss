<?php if(get_sub_field('display_videos')):?>
<section>
<div class="container">
<?php
$show_popup = get_sub_field('show_video_in_popup');
$args = array(
    'taxonomy' => 'video_categories',
    'orderby' => 'name',
    'order'   => 'ASC',
    'parent' => 0,

);
$categories = get_categories($args);
    foreach($categories as $category):
        ?>
		<div class="mt-5">
			<div class="video_title">
				<h4><?php echo $category->name;?></h4>
				<p><?php echo $category->description;?></p>
			</div>
			<div class="video_grid scrollBarNew">
				<ul>
                <?php
				
				$args = array(
					'post_type' => 'product_training',
					'tax_query' => array(
						array(
							'taxonomy' => 'video_categories',
							'field' => 'term_id',
							'terms' => $category->term_id
						),			
	 				),
 					'posts_per_page' => -1
				);
                $pro_videos = get_posts( $args );
                foreach ( $pro_videos as $post ) : setup_postdata( $post ); ?>
				<li>
                    <?php 
                    $videoURL = get_field('video_url',$post->ID);
					$urlArr = explode("/",$videoURL);
					$urlArrNum = count($urlArr);

					// Youtube video ID
					$youtubeVideoId = $urlArr[$urlArrNum - 1];

					// Generate youtube thumbnail url
					$thumbURL = 'http://img.youtube.com/vi/'.$youtubeVideoId.'/0.jpg';
							
					$embed_code = convertYoutube($videoURL);
                    ?>
					<div class="video_box">
						<div class="video_img">
							<img src="<?php echo $thumbURL;?>" alt="img">
							<?php if($show_popup == true):?> 
								<a data-bs-toggle="modal" href="#video_popup_<?php echo $youtubeVideoId;?>" role="button">
							<?php else:?>
								<a href="<?php echo $videoURL;?>" target="_blank">
							<?php endif;?>	
								<span><img src="<?php echo get_template_directory_uri();?>/_images/video_icon.png" alt="video_icon"></span>
							</a>
						</div>									
						<div class="video_title_inner">
							<?php echo  get_the_title($post->ID);?>
						</div>
					</div>
				</li>
				<div class="modal fade" id="video_popup_<?php echo $youtubeVideoId;?>" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
					<div class="modal-dialog modal-dialog-centered modal-lg">
						<div class="modal-content">
							<div class="modal_header">
								<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
							</div>
							<div class="modal_body">
								<div class="o-video">
									<?php echo $embed_code;?>
								</div>

							</div>
						</div>
					</div>
				</div>
                <?php endforeach;?>
			</ul>
		</div>
	</div>
<?php endforeach;?>
</div>
</section>
<?php endif;?>