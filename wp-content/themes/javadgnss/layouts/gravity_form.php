<?php $formId = get_sub_field('forms');
if($formId){?>
<section class="mt-5">
	<div class="container">
        <div class="form_grid">
            <?php echo do_shortcode('[gravityform id="'.$formId.'" title="false" ajax="true"]');?>
        </div>
    </div>
</section>
<?php }?>