<?php
$main_title = get_sub_field('title');
$img_url = get_sub_field('image');
$image = aq_resize($img_url['url'],980,980, true);
//print_r($img_url);
$price = get_sub_field('price');
$description = get_sub_field('description');
$details_list_title = get_sub_field('details_list_title');
$data_sheet = get_sub_field('data_sheet');
$software_update = get_sub_field('software_update');
$top_left_button = get_sub_field('top_left_button');

?>
<section class="press_contents">
	<div class="container">
		<div class="press_information" data-aos="fade-up" data-aos-duration="1500">
			<div class="button_grid">
				<div class="left_btn">					
					<?php if ($top_left_button) :
						$link_url = $top_left_button['url'];
						$link_title = $top_left_button['title'];
						$link_target = $top_left_button['target'] ? $top_left_button['target'] : '_self';
					?>
						<a class="green_btn" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><i class="fas fa-chevron-left"></i> <?php echo esc_html($link_title); ?></a>
					<?php endif; ?>
				</div>
				<div class="right_btnGroup d-sm-flex">
					<?php
					$files_lists = get_field('files_list');
					echo '<pre>';
					print_r($files_lists);
					echo '</pre>';
						// Data Sheet list
						if ($files_lists['data_sheets_group'] && count($files_lists['data_sheets_group']) > 1) { ?>

							<nav class="navbar me-3" role="navigation" aria-label="menu">
								<ul class="button-list menu">
									<li class="button-list-item">
										<?php foreach ($files_lists['data_sheets_group'] as $index => $list) :
											$title = $list['data_sheets'] ? $list['data_sheets'] : 'Data Sheet';
											$url = $list['data_sheets_file_upload']['url'];
											if ($index === 0) :
										?>
												<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
													<?php echo esc_html($title); ?>
													<!--SVG dropdown icon-->
													<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
														<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
													</svg>
												</a>
												<ul class="button-list-inner-list">
												<?php else : ?>
													<li>
														<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
															<?php echo esc_html($title); ?>
														</a>
													</li>
												<?php endif;
											if ($index === count($files_lists['data_sheets_group']) - 1) :
												?>
												</ul>
											<?php endif; ?>
										<?php endforeach; ?>
									</li>
								</ul>
							</nav>

						<?php } elseif ($files_lists['data_sheets_group'] && count($files_lists['data_sheets_group']) === 1) {

							$title = $files_lists['data_sheets_group'][0]['data_sheets'] ? $files_lists['data_sheets_group'][0]['data_sheets'] : 'Data Sheet';
							$url = $files_lists['data_sheets_group'][0]['data_sheets_file_upload']['url'];
						?>
							<a class="gray_btn" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_html($title); ?></a>
						<?php }

						// Software update list
						if ($files_lists['software_updates_group'] && count($files_lists['software_updates_group']) > 1) { ?>

							<nav class="navbar me-3" role="navigation" aria-label="menu">
								<ul class="button-list menu">
									<li class="button-list-item">
										<?php foreach ($files_lists['software_updates_group'] as $index => $list) :
											$title = $list['software_updates'] ? $list['software_updates'] : 'Software Update';
											$url = $list['software_updates_file_upload']['url'];
											if ($index === 0) :
										?>
												<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
													<?php echo esc_html($title); ?>
													<!--SVG dropdown icon-->
													<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
														<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
													</svg>
												</a>
												<ul class="button-list-inner-list">
												<?php else : ?>
													<li>
														<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
															<?php echo esc_html($title); ?>
														</a>
													</li>
												<?php endif;
											if ($index === count($files_lists['software_updates_group']) - 1) :
												?>
												</ul>
											<?php endif; ?>
										<?php endforeach; ?>
									</li>
								</ul>
							</nav>

						<?php } elseif ($files_lists['software_updates_group'] && count($files_lists['software_updates_group']) === 1) {
							$title = $files_lists['software_updates_group'][0]['software_updates'] ? $files_lists['software_updates_group'][0]['software_updates'] : 'Software Update';
							$url = $files_lists['software_updates_group'][0]['software_updates_file_upload']['url'];
						?>
							<a class="gray_btn" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_html($title); ?></a>
					<?php }

					?>

				</div>
			</div>

			<div class="inner_design row">
				<div class="col-md-7">
					<?php if ($main_title) : ?>
						<h3><?php echo $main_title; ?></h3>
					<?php endif;
					if ($price) : ?>
						<h5><?php echo $price; ?></h5>
					<?php endif;
					if ($description) echo $description; ?>
					<?php if ($details_list_title) : ?>
						<h5><?php echo $details_list_title; ?></h5>
					<?php endif;
					if (have_rows('detail_list')) :
					?>
						<ul class="dotted_para column-2">
							<?php while (have_rows('detail_list')) : the_row();
								$list = get_sub_field('list');
							?>
								<li><?php echo $list; ?></li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>

					<div class="d-sm-flex">

						<?php if (have_rows('buttons')) : while (have_rows('buttons')) : the_row();
								$buttons_list = get_sub_field('buttons_list');
						?>
								<?php if ($buttons_list && count($buttons_list) > 1) : ?>
									<nav class="navbar me-3" role="navigation" aria-label="menu">
										<ul class="button-list menu">
											<li class="button-list-item">
												<?php foreach ($buttons_list as $index => $list) :
													$title = $list['link']['title'];
													$url = $list['link']['url'];
													$target = $list['link']['target'];
													if ($index === 0) :
												?>
														<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
															<?php echo esc_html($title); ?>
															<!--SVG dropdown icon-->
															<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
																<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
															</svg>
														</a>
														<ul class="button-list-inner-list">
														<?php else : ?>
															<li>
																<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
																	<?php echo esc_html($title); ?>
																</a>
															</li>
														<?php endif;
													if ($index === count($buttons_list) - 1) :
														?>
														</ul>
													<?php endif; ?>
												<?php endforeach; ?>
											</li>
										</ul>
									</nav>
								<?php
								elseif ($buttons_list && count($buttons_list) === 1) :
									$title = $buttons_list[0]['link']['title'];
									$url = $buttons_list[0]['link']['url'];
									$target = $buttons_list[0]['link']['target'];
								?>
									<a class="green_btn me-3 mt-0" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
										<?php echo esc_html($title); ?>
									</a>
								<?php endif; ?>

						<?php
							endwhile;
						endif; ?>

					</div>

				</div>
				<div class="col-md-5">
					<?php 
					/**
					*   Datasheets
					*/
					if(is_array(get_field('files_list', get_the_ID()))){
					    $get_files = array_filter(get_field('files_list', get_the_ID()));        
					    if(!empty($get_files)){
					        echo '<div class="datasheets_gallery"><ul>'; 
					        foreach($get_files as $key => $file) {
					            foreach( $file as $key1 => $sub_file):?>
									<?php 
									$pdf_id = '';
									$pdf_name =''; 
									if($key == 'data_sheets_group'){
										$tileName = 'Data Sheet';
										$pdf_name = $sub_file['data_sheets'];
										$pdf_id = $sub_file['data_sheets_file_upload'];
									} elseif($key == 'user_manual_group'){
										$tileName = 'User Manual';
										$pdf_name = $sub_file['user_manual'];
										$pdf_id = $sub_file['user_manual_file_upload'];
									} else {
										$tileName = 'Software Updates';
										$pdf_name = $sub_file['software_updates'];
										$pdf_id = $sub_file['software_updates_file_upload'];
									}

									if($pdf_id):?>
									<li class="file_items" >
										<a class="btn green_btn darkColor" href="<?php echo wp_get_attachment_url($pdf_id);?>" target="_blank"><?php echo $tileName;?></a>
									</li>    
					            <?php endif;
					            endforeach;
					        }
					        echo '</ul></div>';  
					    }
					}
					?>


					<div class="img_right">
						<?php if ($image) : ?>
							<img src="<?php echo $image; ?>" alt="<?php echo $image_url['alt']; ?>">
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>