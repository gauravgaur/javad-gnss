<?php
$image = get_sub_field('image');
$reverse_columns = get_sub_field('reverse_columns');
$title = get_sub_field('title');
$description = get_sub_field('description');
// $link = get_sub_field('button');
$buttons_list = get_sub_field('buttons_list');

?>
<div class="button_grid">
	<div class="left_btn">
	</div>
	<div class="right_btnGroup d-sm-flex">
		<?php
		$files_lists = get_sub_field('files_lists');
			// Data Sheet list
			if ($files_lists['data_sheets_group'] && count($files_lists['data_sheets_group']) > 1) { ?>

				<nav class="navbar me-3" role="navigation" aria-label="menu">
					<ul class="button-list menu">
						<li class="button-list-item">
							<?php foreach ($files_lists['data_sheets_group'] as $index => $list) :
								$title = $list['data_sheets'] ? $list['data_sheets'] : 'Data Sheet';
								$url = $list['data_sheets_file_upload']['url'];
								if ($index === 0) :
							?>
									<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
										<?php echo esc_html($title); ?>
										<!--SVG dropdown icon-->
										<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
											<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
										</svg>
									</a>
									<ul class="button-list-inner-list">
									<?php else : ?>
										<li>
											<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
												<?php echo esc_html($title); ?>
											</a>
										</li>
									<?php endif;
								if ($index === count($files_lists['data_sheets_group']) - 1) :
									?>
									</ul>
								<?php endif; ?>
							<?php endforeach; ?>
						</li>
					</ul>
				</nav>

			<?php } elseif ($files_lists['data_sheets_group'] && count($files_lists['data_sheets_group']) === 1) {

				$title = $files_lists['data_sheets_group'][0]['data_sheets'] ? $files_lists['data_sheets_group'][0]['data_sheets'] : 'Data Sheet';
				$url = $files_lists['data_sheets_group'][0]['data_sheets_file_upload']['url'];
			?>
				<a class="gray_btn" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_html($title); ?></a>
			<?php }

			// Software update list
			if ($files_lists['software_updates_group'] && count($files_lists['software_updates_group']) > 1) { ?>

				<nav class="navbar me-3" role="navigation" aria-label="menu">
					<ul class="button-list menu">
						<li class="button-list-item">
							<?php foreach ($files_lists['software_updates_group'] as $index => $list) :
								$title = $list['software_updates'] ? $list['software_updates'] : 'Software Update';
								$url = $list['software_updates_file_upload']['url'];
								if ($index === 0) :
							?>
									<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
										<?php echo esc_html($title); ?>
										<!--SVG dropdown icon-->
										<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
											<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
										</svg>
									</a>
									<ul class="button-list-inner-list">
									<?php else : ?>
										<li>
											<a class="green_btn" href="<?php echo esc_url($url); ?>" target="_blank">
												<?php echo esc_html($title); ?>
											</a>
										</li>
									<?php endif;
								if ($index === count($files_lists['software_updates_group']) - 1) :
									?>
									</ul>
								<?php endif; ?>
							<?php endforeach; ?>
						</li>
					</ul>
				</nav>

			<?php } elseif ($files_lists['software_updates_group'] && count($files_lists['software_updates_group']) === 1) {
				$title = $files_lists['software_updates_group'][0]['software_updates'] ? $files_lists['software_updates_group'][0]['software_updates'] : 'Software Update';
				$url = $files_lists['software_updates_group'][0]['software_updates_file_upload']['url'];
			?>
				<a class="gray_btn" href="<?php echo esc_url($url); ?>" target="_blank"><?php echo esc_html($title); ?></a>
		<?php }

		?>

	</div>
</div>
<section class="mt-5 <?php if ($reverse_columns) echo 'rotated_row'; ?>">
	<div class="container">
		<div class="row aline_center">
			<div class="col-md-12 col-lg-6">
				<div class="box_content2" data-aos="fade-up" data-aos-duration="1500">
					<?php if ($title) : ?>
						<h4><?php echo $title; ?></h4>
					<?php endif;
					if ($description) echo $description;
// 					if ($link) :
// 						$link_url = $link['url'];
// 						$link_title = $link['title'];
// 						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<!--<a class="green_btn" href="<?php // echo esc_url($link_url); ?>" target="<?php // echo esc_attr($link_target); ?>"><?php // echo esc_html($link_title); ?></a> -->
					<?php // endif; ?>

					<?php
					if ($buttons_list && count($buttons_list) > 1) :
					?>
						<nav class="navbar" role="navigation" aria-label="menu">
							<ul class="button-list menu">
								<li class="button-list-item">
									<?php foreach ($buttons_list as $index => $list) :
										$title = $list['link']['title'];
										$url = $list['link']['url'];
										$target = $list['link']['target'];
										$unique_id = $list['unique_ids'];
										if ($index === 0) :
									?>
											<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
												<?php echo esc_html($title); ?>
												<!--SVG dropdown icon-->
												<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
													<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
												</svg>
											</a>
											<ul class="button-list-inner-list">
											<?php else : ?>
												<li>
													<a class="green_btn" id="<?php echo $unique_id;?>" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
														<?php echo esc_html($title); ?>
													</a>
												</li>
											<?php endif;
										if ($index === count($buttons_list) - 1) :
											?>
											</ul>
										<?php endif; ?>
									<?php endforeach; ?>
								</li>
							</ul>
						</nav>
					<?php
					elseif ($buttons_list && count($buttons_list) === 1) :
						$title = $buttons_list[0]['link']['title'];
						$url = $buttons_list[0]['link']['url'];
						$target = $buttons_list[0]['link']['target'];
					?>
						<a class="green_btn 7" id="asasasas" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
							<?php echo esc_html($title); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-12 col-lg-6">
				<div class="box_img">
					<?php if ($image) : ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>