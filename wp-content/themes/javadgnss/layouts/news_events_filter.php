<?php
$add_news_tab_title = get_sub_field('add_news_tab_title');
$add_events_tab_title = get_sub_field('add_events_tab_title');
$news_and_events_page_cta = get_field('news_and_events_page_cta', 'option'); 
?>
<section class="tabs_grid" data-aos="fade-up" data-aos-duration="1500">
			<div class="full_content">
				<ul class="nav">
					<li><button class="active" data-bs-toggle="pill" data-bs-target="#news_tab" type="button" role="tab" aria-selected="true"><?php echo $add_news_tab_title; ?></button></li>
					<li><button class="" data-bs-toggle="pill" data-bs-target="#events_tab" type="button" role="tab" aria-selected="false"><?php echo $add_events_tab_title; ?></button></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane fade show active" id="news_tab" role="tabpanel" aria-labelledby="pills-home-tab">
                <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'post_type' => 'news',
                        'post_status' => 'publish',
                        'posts_per_page' => 5,
                        'paged' => $paged,
						'meta_key' => 'news_article_start_date',
						'orderby' => 'meta_value',
						'order'	=> 'DESC'
                    );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ):
                    ?>
					<div class="container">
						<div class="row load_news_html">
						<?php  ?>
                             <?php $i=0; while ( $loop->have_posts() ) : $loop->the_post(); $i++;
							 $news_article_start_date = get_field('news_article_start_date', $loop->ID);
							 //$news_article_end_date = get_field('news_article_end_date', $loop->ID);
							 $news_excerpt = get_the_excerpt($loop->ID);
							 ?>
							<div class="col-md-12 <?php if($i==1): ?>col-lg-12<?php else: ?>col-lg-6<?php endif; ?>">
								<div class="row g-0 dark_gray_bg">
									<div class="col-sm-6 col-md-6">
									<?php if (has_post_thumbnail( $loop->ID ) ): ?>
										<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->ID ), 'single-post-thumbnail' ); ?>
										<div class="news_img"><a href="<?php echo get_the_permalink($loop->ID); ?>"><img src="<?php echo $image[0]; ?>" alt="img"></a></div>
										<?php endif; ?>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="info_content_box">
											<?php if($news_article_start_date): ?><h5><?php echo date("F j, Y", strtotime($news_article_start_date)); ?></h5><?php endif; ?>
											<h4><a href="<?php echo get_the_permalink($loop->ID); ?>"><?php echo get_the_title($loop->ID); ?></a></h4>
											<?php if($news_excerpt): ?><p><?php echo $news_excerpt; ?></p><?php endif; ?>
											<a class="arrow_btn" href="<?php echo get_the_permalink($loop->ID); ?>">
												<span class="linkText">Read More <i class="fas fa-arrow-right"></i></span>
											</a>
										</div>
									</div>
								</div>
							</div>
                            <?php endwhile; wp_reset_postdata(); ?>
							                            
							<div class="col-md-12 col-lg-12">
								<div class="text-center more_post">
									<div class="breadcrum_grid">
									<nav class="pagination">
										<?php echo javad_news_pagination( $loop->max_num_pages,2, $paged ); ?>
									</nav>
									</div>
									<?php if(!empty($news_and_events_page_cta)): ?><a class="green_btn" href="<?php echo $news_and_events_page_cta['url']; ?>"><?php echo $news_and_events_page_cta['title']; ?></a><?php endif; ?>
								</div>
							</div>
						</div>
					</div>
                    <?php endif; ?>
				</div>
				
				<div class="tab-pane fade" id="events_tab" role="tabpanel" aria-labelledby="pills-profile-tab">
				<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$args = array(
						'post_type' => 'events',
						'post_status' => 'publish',
						'posts_per_page' => 3,
						'paged' => $paged,
						'meta_key' => 'event_start_date',
						'orderby' => 'meta_value',
						'order'	=> 'DESC'
					);
					$loop = new WP_Query( $args );
					if ( $loop->have_posts() ):
					?>
					<div class="container">
						<div class="row load_events_html">
							<?php $i=0; while ( $loop->have_posts() ) : $loop->the_post(); $i++;
							 $event_start_date = get_field('event_start_date', $loop->ID);
							 $event_end_date = get_field('event_end_date', $loop->ID);
							 $events_excerpt = get_the_excerpt($loop->ID);
							 ?>
							<div class="col-md-12 <?php if($i==1): ?>col-lg-12<?php else: ?>col-lg-6<?php endif; ?>">
								<div class="row g-0 dark_gray_bg">
									<div class="col-sm-6 col-md-6">
										<?php if (has_post_thumbnail( $loop->ID ) ): ?>
											<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->ID ), 'single-post-thumbnail' ); ?>
											<div class="news_img"><a href="<?php echo get_the_permalink($loop->ID); ?>"><img src="<?php echo $image[0]; ?>" alt="img"></a></div>
										<?php endif; ?>										
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="info_content_box">
										<?php if($event_start_date): ?><h5><?php echo date("F j", strtotime($event_start_date)); ?> - <?php echo $event_end_date; ?></h5><?php endif; ?>
											<h4><a href="<?php echo get_the_permalink($loop->ID); ?>"><?php echo get_the_title($loop->ID); ?></a></h4>
											<?php if($events_excerpt): ?><p><?php echo $events_excerpt; ?></p><?php endif; ?>
											<a class="events_excerpt" href="<?php echo get_the_permalink($loop->ID); ?>">
												<span class="linkText">Read More <i class="fas fa-arrow-right"></i></span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<?php endwhile;  wp_reset_postdata(); ?>

							<div class="col-md-12 col-lg-12">
								<div class="text-center more_post">
									<div class="breadcrum_grid">
										<nav class="pagination">
											<?php echo javad_events_pagination( $loop->max_num_pages,2, $paged ); ?>
										</nav>
									</div>
									<?php if(!empty($news_and_events_page_cta)): ?><a class="green_btn" href="<?php echo $news_and_events_page_cta['url']; ?>"><?php echo $news_and_events_page_cta['title']; ?></a><?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php endif; ?> 
				</div> 
			</div>
		</section>