<?php 
$title = get_sub_field('title');?>
<section class="tabs_grid" data-aos="fade-up" data-aos-duration="1500">
			<div class="full_content">
				<?php if($title):?>
			<h4 class="tab_title"><?php echo $title;?></h4>
			<?php endif;?>
				<ul class="nav">
					<li><button class="active" data-bs-toggle="pill" data-bs-target="#news_tab" type="button" role="tab" aria-selected="true">News</button></li>
					<li><button class="" data-bs-toggle="pill" data-bs-target="#events_tab" type="button" role="tab" aria-selected="false">Events</button></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane fade show active" id="news_tab" role="tabpanel" aria-labelledby="pills-home-tab">
					<div class="container">
                        <?php
                        $news = get_sub_field('news');
                        if( $news ): ?>
						<div class="row">
                        <?php foreach( $news as $single_news ): 
                            $permalink = get_permalink( $single_news->ID );
                            $title = get_the_title( $single_news->ID );
                            $excerpt = get_the_excerpt($single_news->ID );
                            $date = get_the_date('Y-m-d',$single_news->ID );
                            ?>

							<div class="col-md-12 col-lg-6">
								<div class="row g-0 dark_bg">
									<div class="col-sm-6 col-md-6">
										<div class="news_img"><?php echo get_the_post_thumbnail($single_news->ID);?></div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="info_content_box">
											<h5><?php echo $date;?></h5>
											<h4><?php echo $title;?></h4>
											<p><?php echo $excerpt;?></p>
											<a class="arrow_btn" href="<?php echo $permalink;?>">
												<span class="linkText">Read More <i class="fas fa-arrow-right"></i></span>
											</a>
										</div>
									</div>
								</div>
							</div>
                            <?php endforeach;?>
						</div>
                        <?php endif;?>
					</div>
				</div>
				<div class="tab-pane fade" id="events_tab" role="tabpanel" aria-labelledby="pills-profile-tab">
					<div class="container">
					<?php
                        $events = get_sub_field('events');
                        if( $events ): ?>
						<div class="row">
                        <?php foreach( $events as $single_events ): 
                            $permalink = get_permalink( $single_events->ID );
                            $title = get_the_title( $single_events->ID );
                            $excerpt = get_the_excerpt($single_events->ID );
                            $date = get_the_date('Y-m-d',$single_events->ID );
                            ?>

							<div class="col-md-12 col-lg-6">
								<div class="row g-0 dark_bg">
									<div class="col-sm-6 col-md-6">
										<div class="news_img"><?php echo get_the_post_thumbnail($single_events->ID);?></div>
									</div>
									<div class="col-sm-6 col-md-6">
										<div class="info_content_box">
											<h5><?php echo $date;?></h5>
											<h4><?php echo $title;?></h4>
											<p><?php echo $excerpt;?></p>
											<a class="arrow_btn" href="<?php echo $permalink;?>">
												<span class="linkText">Read More <i class="fas fa-arrow-right"></i></span>
											</a>
										</div>
									</div>
								</div>
							</div>
                            <?php endforeach;?>
						</div>
                        <?php endif;?>
					</div>
				</div>
				<div class="event_tab_btn">
					<div class="text-center more_post">
						<?php 
						$button = get_sub_field('button');
						if( $button ): 
						$button_url = $button['url'];
						$button_title = $button['title'];
						$button_target = $button['target'] ? $button['target'] : '_self';
						?>
						<a class="green_btn" href="<?php echo esc_url( $button_url ); ?>" target="<?php echo esc_attr( $button_target ); ?>"><?php echo esc_html( $button_title ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>