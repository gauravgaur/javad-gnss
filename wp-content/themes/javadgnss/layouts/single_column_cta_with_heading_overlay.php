<?php 
$title = get_sub_field('title');
$image = get_sub_field('image');
$link = get_sub_field('button');
$buttons_list = get_sub_field('buttons_list');

?>
<section>
    <div class="container">
        <div class="key_features" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0">
            <div class="product_title">
                <?php if($title):?>
                <h3><?php echo $title;?> </h3>
                <?php endif;?>
            </div>
            <div class="key_features_img">
                <?php if($image):?>
                <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
                <?php endif;?>
            </div>
            <div class="text-end">
                <?
//                 if( $link ): 
//                     $link_url = $link['url'];
//                     $link_title = $link['title'];
//                     $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
<!--                 <a class="btn green_btn" href="<?php // echo esc_url( $link_url ); ?>" target="<?php // echo esc_attr( $link_target ); ?>"><?php // echo esc_html( $link_title ); ?></a> -->
                <?php // endif;?>
				
				<?php
					if ($buttons_list && count($buttons_list) > 1) :
					?>
						<nav class="navbar" role="navigation" aria-label="menu">
							<ul class="button-list menu">
								<li class="button-list-item">
									<?php foreach ($buttons_list as $index => $list) :
										$title = $list['link']['title'];
										$url = $list['link']['url'];
										$target = $list['link']['target'];
										if ($index === 0) :
									?>
											<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
												<?php echo esc_html($title); ?>
												<!--SVG dropdown icon-->
												<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
													<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
												</svg>
											</a>
											<ul class="button-list-inner-list">
											<?php else : ?>
												<li>
													<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
														<?php echo esc_html($title); ?>
													</a>
												</li>
											<?php endif;
										if ($index === count($buttons_list) - 1) :
											?>
											</ul>
										<?php endif; ?>
									<?php endforeach; ?>
								</li>
							</ul>
						</nav>
					<?php
					elseif ($buttons_list && count($buttons_list) === 1) :
						$title = $buttons_list[0]['link']['title'];
						$url = $buttons_list[0]['link']['url'];
						$target = $buttons_list[0]['link']['target'];
					?>
						<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
							<?php echo esc_html($title); ?>
						</a>
					<?php endif; ?>
				
            </div>
        </div>
    </div>
</section>