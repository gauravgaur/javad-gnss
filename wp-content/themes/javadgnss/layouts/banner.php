<?php $image = get_sub_field('image');
$title = get_sub_field('title');
$center_text = get_sub_field('center_text');
$sub_title = get_sub_field('sub_title');
$jrl_buttons = get_field('jerci_rams_live_demo_buttons');
?>
<section class="sub_banner_content">
    <div class="sub_banner_img">
        <?php if($image):?>
        <img src="<?php echo $image['url'];?>" alt="<?php echo esc_attr($image['alt']);?>">
        <?php endif;?>
    </div>
    <div class="sub_content">
        <div class="container">
            <div class="banner_content<?php if($center_text) echo ' text-center aos-init aos-animate';?>" data-aos="fade-up" data-aos-duration="2000">
                <?php if($title):?>
                <h1><?php echo $title;?></h1>
                <?php endif;
				if($sub_title):?>
				<p>
					<?php echo $sub_title;?>
				</p>
				<?php endif;
                while(have_rows('buttons')):the_row();
                $link = get_sub_field('button');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                <a class="green_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                <?php endif; endwhile;
                if(get_sub_field('show_breadcrumb')):
                ?>
                <?php custom_breadcrumbs(); ?>
					<?php /*if(function_exists('yoast_breadcrumb')){
						yoast_breadcrumb('<p class="bread_nav">','</p>');
					}*/?>
                <?php endif;?>

            </div>
            <?php if(count($jrl_buttons) >=1):?>
                <div class="jrl_buttons"> 
                <?php foreach($jrl_buttons as $btn):?>
                    <a href="<?php echo $btn['button_links']['url'];?>" target="<?php echo $btn['button_links']['target'];?>">
                        <img src="<?php echo $btn['button_image'];?>" alt="" />
                    </a>
                <?php endforeach;?>
                </div>
            <?php endif;?>
        </div>
    </div>
</section>
