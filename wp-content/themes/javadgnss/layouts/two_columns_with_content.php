<?php
$title = get_sub_field('title');
$title_r = get_sub_field('title_r');
$description_r = get_sub_field('description_r');
$description = get_sub_field('description');
$buttons_list_r = get_sub_field('buttons_list_r');
$buttons_list = get_sub_field('buttons_list');
$reverse_columns = get_sub_field('reverse_columns');

?>
<section class="mt-5">
	<div class="container">
		<div class="row <?php if ($reverse_columns) echo 'flex-row-reverse'; ?>">
			<div class="col-md-12 col-lg-6">
				<div class="box_content2" data-aos="fade-up" data-aos-duration="1500">
					<?php if ($title) : ?>
						<h4><?php echo $title; ?></h4>
					<?php endif;
					if ($description) echo $description;
					if ($buttons_list && count($buttons_list) > 1) :
					?>
						<nav class="navbar" role="navigation" aria-label="menu">
							<ul class="button-list menu">
								<li class="button-list-item">
									<?php foreach ($buttons_list as $index => $list) :
										$title = $list['link']['title'];
										$url = $list['link']['url'];
										$target = $list['link']['target'];
										if ($index === 0) :
									?>
											<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
												<?php echo esc_html($title); ?>
												<!--SVG dropdown icon-->
												<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
													<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
												</svg>
											</a>
											<ul class="button-list-inner-list">
											<?php else : ?>
												<li>
													<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
														<?php echo esc_html($title); ?>
													</a>
												</li>
											<?php endif;
										if ($index === count($buttons_list) - 1) :
											?>
											</ul>
										<?php endif; ?>
									<?php endforeach; ?>
								</li>
							</ul>
						</nav>
					<?php
					elseif ($buttons_list && count($buttons_list) === 1) :
						$title = $buttons_list[0]['link']['title'];
						$url = $buttons_list[0]['link']['url'];
						$target = $buttons_list[0]['link']['target'];
					?>
						<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
							<?php echo esc_html($title); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-12 col-lg-6">
			<div class="box_content2" data-aos="fade-up" data-aos-duration="1500">
					<?php if ($title_r) : ?>
						<h4><?php echo $title_r; ?></h4>
					<?php endif;
					if ($description_r) echo $description_r;
					if ($buttons_list_r && count($buttons_list_r) > 1) :
					?>
						<nav class="navbar" role="navigation" aria-label="menu">
							<ul class="button-list menu">
								<li class="button-list-item">
									<?php foreach ($buttons_list_r as $index => $list) :
										$title = $list['link']['title'];
										$url = $list['link']['url'];
										$target = $list['link']['target'];
										if ($index === 0) :
									?>
											<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
												<?php echo esc_html($title); ?>
												<!--SVG dropdown icon-->
												<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
													<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
												</svg>
											</a>
											<ul class="button-list-inner-list">
											<?php else : ?>
												<li>
													<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
														<?php echo esc_html($title); ?>
													</a>
												</li>
											<?php endif;
										if ($index === count($buttons_list_r) - 1) :
											?>
											</ul>
										<?php endif; ?>
									<?php endforeach; ?>
								</li>
							</ul>
						</nav>
					<?php
					elseif ($buttons_list_r && count($buttons_list_r) === 1) :
						$title = $buttons_list_r[0]['link']['title'];
						$url = $buttons_list_r[0]['link']['url'];
						$target = $buttons_list_r[0]['link']['target'];
					?>
						<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
							<?php echo esc_html($title); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>