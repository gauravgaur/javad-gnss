<section class="simple_banner">
    <div class="container">
        <div class="banner_content" data-aos="fade-up" data-aos-duration="1500">
            <?php if(get_sub_field('title')):?>
            <h1><?php echo get_sub_field('title');?></h1>
            <?php endif;?>
            <?php if(have_rows('images')):?>
            <div class="img_grid">
                <?php while(have_rows('images')):the_row();
                    $image = get_sub_field('image');
                    if($image):
                ?>
                <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
                <?php endif; endwhile;?>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>