<section class="banner_bg">
			<div id="mainSlider" class="carousel slide" data-bs-ride="carousel">
				<?php
				if(have_rows('slide')):
					$i = 0;?>
				<div class="carousel-indicators">
				<?php while(have_rows('slide')):the_row();?>
					<button type="button" data-bs-target="#mainSlider" data-bs-slide-to="<?php echo $i;?>" class="active" aria-current="true" aria-label="Slide <?php echo $i+1;?>"></button>
				<?php $i++; endwhile;?>
				</div>
				<?php endif;
				if(have_rows('slide')):
					$i = 0;
					?>
				<div class="carousel-inner">
					<?php while(have_rows('slide')):the_row();
					$image = get_sub_field('image');
					$text = get_sub_field('text');?>
					<div class="carousel-item <?php if($i ==0 ) echo 'active';?>">
					<?php if($image):?>
						<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
					<?php endif;?>
						<div class="carousel-caption">
							<?php if($text):?>
							<h2 data-aos="fade-up" data-aos-duration="2000"><?php echo $text;?></h2>
							<?php endif;?>
						</div>
					</div>
					<?php $i++;
					endwhile;?>
				</div>
				<?php endif;?>
				<!-- <button class="carousel-control-prev" type="button" data-bs-target="#mainSlider" data-bs-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</button>
				<button class="carousel-control-next" type="button" data-bs-target="#mainSlider" data-bs-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</button> -->
			</div>
		</section>