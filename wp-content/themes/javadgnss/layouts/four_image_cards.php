<section>
	<div class="container">
		<div class="traning_post_box" data-aos="fade-up" data-aos-duration="1500">
			<?php if(have_rows('cards')):?>
			<ul>
				<?php while(have_rows('cards')):the_row();
				$title = get_sub_field('title');
				$image = get_sub_field('image');
				$link = get_sub_field('link');
				if( $link ): $link_url = $link['url']; $link_target = $link['target'] ? $link['target'] : '_self'; endif;
				?>
				<li>
					<div class="combine_box">
						<div class="post_img">
							<?php if($image):?>
							<a href="<?php echo $link_url; ?>"target="<?php echo $link_target; ?>">
<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
</a>
							<?php endif;?>
						</div>
						<div class="gray_content">
							<?php if($title):?>
							<h4><a href="<?php echo $link_url; ?>"target="<?php echo $link_target; ?>"><?php echo $title;?></a></h4>
							<?php endif;?>
							<div class="text-end">
								<?php
								if( $link ): 
								 $link_url = $link['url'];
								 $link_title = $link['title'];
								 $link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
								<?php endif;?>
							</div>
						</div>
					</div>
				</li>
				<?php endwhile;?>
			</ul>
			<?php endif;?>
		</div>
	</div>
</section>