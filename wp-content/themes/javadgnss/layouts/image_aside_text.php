<?php
$image = get_sub_field('image');
$description = get_sub_field('description');?>
<section class="mt-5">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="aline_logo">
                            <?php if($image):?>
							<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
                            <?php endif;?>
						</div>
					</div>
					<div class="col-md-8">
						<div class="overlap_Content icon_content" data-aos="fade-up" data-aos-duration="1500">
                            <?php echo $description;?>
						</div>
					</div>
				</div>
			</div>
		</section>