<section class="support_grid">
    <div class="full_img" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="500" data-aos-offset="0">
        <?php $image = get_sub_field('image');
        if($image):?>
        <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
        <?php endif;?>
    </div>
    <div class="gray_bg">
        <div class="container">
            <div class="support_Content" data-aos="fade-up" data-aos-duration="1000">
                <?php if(get_sub_field('title')):?>
                <h4><?php echo get_sub_field('title');?></h4>
                <?php endif;
                echo get_sub_field('description');
                $link = get_sub_field('button');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a class="green_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>