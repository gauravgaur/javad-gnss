<?php 
$columns = get_sub_field('inner_columns');
$col_count = sizeof($columns);
$size = ceil(12 / sizeof($columns));
if($col_count<2) $size = 12;?>
<section class="box_bg">
    <div class="row justify-content-center align-items-start text-center">
    <?php 
    foreach($columns as $column):?>
        <div class="col-md-<?php echo $size;?> col-lg-<?php echo $size;?>">
            <div class="innerContent">
                <h4><?php echo $column['title'];?></h4>
                <?php echo $column['content'];?>
            </div>
        </div>
    <?php endforeach;?>
    </div>
</section>