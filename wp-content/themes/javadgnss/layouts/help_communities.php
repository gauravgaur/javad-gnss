<?php
 $main_title = get_sub_field('main_title');
 $main_description = get_sub_field('main_description');
 $image = get_sub_field('image');
 $icon = get_sub_field('icon');
 $inner_heading = get_sub_field('inner_heading');
 $link = get_sub_field('link');
 ?>
<section class="helping_section" data-aos="fade-up" data-aos-duration="1500">
			<div class="container">
				<div class="help_communities">
                    <?php if($main_title):?>
					<h2><?php echo $main_title;?></h2>
                    <?php endif;
                    if($main_description) echo $main_description;?>
				</div>
				<div class="row g-0 dark_gray_bg">
					<div class="col-sm-5 col-md-5 col-lg-4 col-xl-4">
                        <?php if($image):?>
						<div class="news_img"><img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>"></div>
                        <?php endif;?>
					</div>
					<div class="col-sm-7 col-md-7 col-lg-8 col-xl-8">
						<div class="info_content_box">
							<div class="helping_title">
								<div class="helping_icon">
                                    <?php if($icon):?>
									<img src="<?php echo $icon['url'];?>" alt="<?php echo $icon['alt'];?>">
                                    <?php endif;?>
								</div>
                                <?php if($inner_heading):?>
								<h3><?php echo $inner_heading;?></h3>
                                <?php endif;?>
							</div>
                            <?php while(have_rows('content')):the_row();
                            $title = get_sub_field('title');
                            $description = get_sub_field('description');
                            if($title):?>
							<strong><?php echo $title;?></strong>
                            <?php endif;if($description) echo $description;
                             endwhile;?>
							<div class="end_text">
                                <?php
                                if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
								<a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="linkText"><?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
                                <?php endif;?>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>