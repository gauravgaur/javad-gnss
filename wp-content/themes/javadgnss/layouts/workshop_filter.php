<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 
	'post_type' => 'workshop',
	'post_status' => 'publish',
	'posts_per_page' => 6,
	'paged' => $paged,
);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ):
$countPosts = $loop->found_posts;
?>
		<section class="">
			<div class="container">
				<div class="search_head">
					<div class="row">
						<div class="col-md-6 col-lg-6">
							<div class="btn_filter_group">
								<a class="green_btn filter_tab_all" data-pagenum=1 >All</a>
								<?php
								$post_tags = get_tags(array(
								  'taxonomy' => 'workshop_categories',
								  'orderby' => 'name',
								  'hide_empty' => true, // for development
								));
								$evt = "'"."event"."'";
								foreach ($post_tags as $tag) { echo '<a class="green_btn filter_tab" data-pagenum=1 data-catID = '. $tag->term_id .'>' . $tag->name . '</a>'; }
								?>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 text-end">
							<div class="search_right">
								<input type="text" name="search_workshop" placeholder="Search" id="search_workshop"> 
								<button class="search_btn" onclick="filterworkshop(1)"><i class="fas fa-search"></i></button>
							</div>
						</div>
					</div>
				</div>

				<div class="cards_body" data-aos="fade-up" data-aos-duration="2000">
					<div class="row load_workshop_html">
						<?php while ( $loop->have_posts() ) : $loop->the_post();
						$post_excerpt = get_the_excerpt($loop->ID);
						$ws_start_date = get_field('ws_start_date', $loop->ID);
						$cta_type = get_field('cta_type', $loop->ID);
						$upload_image = get_field('upload_image', $loop->ID);
						$cta_btn_class="";
						if($cta_type=='register'){
							$cta_btn_class="green_btn";
						}elseif($cta_type=='available_soon'){
							$cta_btn_class="gray_btn";
						}elseif($cta_type=='view_workshop'){
							$cta_btn_class="gray_btn border_gray";
						}
						$posttags = get_the_terms( $loop->ID, 'workshop-tag');
						$i=0;
						if( have_rows('add_cta') ):
						while( have_rows('add_cta') ) : the_row(); $i++;
							if($i==1):
								$cta_label = get_sub_field('cta_label');
								$ctaURL = "javascript:void(0)";
								if($cta_type=='register'){ $ctaURL = get_sub_field('cta_url'); }
								if($cta_type=='view_workshop'){ $ctaURL = get_permalink($loop->ID); }
							endif;
						endwhile; endif;
						?>
						<div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 col-xxl-4">
							<div class="card_grid">
								<?php if(!empty($upload_image)): ?><div class="card_img">
									<a href="<?php echo $ctaURL ?>"><img src="<?php echo $upload_image['url']; ?>" alt="<?php echo $upload_image['alt']; ?>" /></a>
								</div><?php endif; ?>
								<div class="card_content">
									<div class="tag_title">
										<?php  foreach($posttags as $tag) : ?>
											<span><?php echo $tag->name; ?></span>
										<?php endforeach; ?>
											
									</div>
									<h3><a href="<?php echo $ctaURL ?>"><?php echo get_the_title(); ?></a></h3>
									<?php if($ws_start_date): ?><h5><?php echo $ws_start_date; ?></h5><?php endif; ?>
									<?php if( have_rows('add_speakers') ): ?>
									<ul class="listing_title">
										<li><strong>Speakers</strong></li>
										<?php while( have_rows('add_speakers') ) : the_row();
        									$speaker_name = get_sub_field('speaker_name'); ?>
										<li><?php echo $speaker_name; ?></li>
										<?php endwhile; ?>
									</ul><?php endif; ?>

									<p><?php echo $post_excerpt; ?></p>
									<div class="card_btn">
										<?php if( have_rows('add_cta') ): ?>
											<?php while( have_rows('add_cta') ) : the_row();
											$cta_label = get_sub_field('cta_label');
											
											$ctaURL = "javascript:void(0)";
											if($cta_type=='register'){ $ctaURL = get_sub_field('cta_url'); }
											if($cta_type=='view_workshop'){ $ctaURL = get_permalink($loop->ID); }
										?>
											<?php if($cta_label): ?><a class="<?php echo $cta_btn_class; ?>" href="<?php echo $ctaURL; ?>"><?php echo $cta_label; ?></a><?php endif; ?>
											<?php endwhile; ?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						
						<?php endwhile; wp_reset_postdata(); ?>
						<div class="breadcrum_grid"><nav class="pagination">
							<?php echo javad_workshop_pagination( $loop->max_num_pages,2, $paged ); ?>
						</nav></div>
					</div>
					
				</div>
				
				
			</div>
		</section>
<?php endif; ?>