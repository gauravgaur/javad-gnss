<?php $top_padding = (get_sub_field('top_padding')) ? get_sub_field('top_padding') : 0;
$left_padding = (get_sub_field('left_padding')) ? get_sub_field('left_padding') : 0;
$right_padding = (get_sub_field('right_padding')) ? get_sub_field('right_padding') : 0;
$bottom_padding = (get_sub_field('bottom_padding')) ? get_sub_field('bottom_padding') : 0;
$top_margin = (get_sub_field('top_margin')) ? get_sub_field('top_margin') : 0;
$left_margin = (get_sub_field('left_margin')) ? get_sub_field('left_margin') : 0;
$right_margin = (get_sub_field('right_margin')) ? get_sub_field('right_margin') : 0;
$bottom_margin = (get_sub_field('bottom_margin')) ? get_sub_field('bottom_margin') : 0;

$margin_style = "";
$padding_style = "";
if($top_padding || $left_padding || $right_padding || $bottom_padding){
    $padding_style = "padding:".$top_padding."px ".$right_padding."px ".$bottom_padding."px ". $left_padding."px !important";
}
if($top_margin || $left_margin || $right_margin || $bottom_margin){
    $margin_style = "margin:".$top_margin."px ".$right_margin."px ".$bottom_margin."px ". $left_margin."px !important";
}
?>
<section class="spacer_content" style="<?php echo $padding_style.';'.$margin_style;?>"></section>