<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$strong_text = get_sub_field('strong_text');
$link = get_sub_field('link');
?>
<section class="mt-5">
    <div class="container">
        <div class="help_communities" data-aos="fade-up" data-aos-duration="2000">
            <?php if($title):?>
            <h2><?php echo $title;?></h2>
            <?php endif;
            if($description) echo $description;?>
            <div class="border_info">
                <?php if($strong_text):?>
                <strong><?php echo $strong_text;?></strong>	
                <?php endif;
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                <a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                    <span class="linkText"><i class="fas fa-globe-americas"></i> <?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
                </a>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>