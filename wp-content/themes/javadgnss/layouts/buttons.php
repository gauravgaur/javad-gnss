<section class="mt-5 mb-4">
    <div class="container">
        <div class="d-sm-flex justify-content-center text-center" data-aos="fade-up" data-aos-duration="1800">
            <?php if(have_rows('buttons')):while(have_rows('buttons')):the_row();
            $button = get_sub_field('button');
            $colored = get_sub_field('colored');
			$buttons_list = get_sub_field('buttons_list');
			$count = 1;
			
//             if($button):
//             $link_url = $button['url'];
//             $link_title = $button['title'];
//             $link_target = $button['target'] ? $button['target'] : '_self';
            ?>
<!--             <a class="<?php // echo ($colored) ? 'green_btn' : 'transparent_btn gray_btn';?>" href="<?php // echo esc_url( $link_url ); ?>" target="<?php // echo esc_attr( $link_target ); ?>"><?php // echo esc_html( $link_title ); ?></a> -->
            <?php // endif;endwhile;endif;?>
			
			<?php if ($buttons_list && count($buttons_list) > 1) : ?>
			<nav class="navbar me-md-3" role="navigation" aria-label="menu">
				<ul class="button-list menu">
					<li class="button-list-item">
						<?php foreach ($buttons_list as $index => $list) :
						$title = $list['link']['title'];
						$url = $list['link']['url'];
						$target = $list['link']['target'];
						if ($index === 0) :
						?>
						<a class="green_btn mr-md-3" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
							<?php echo esc_html($title); ?>
							<!--SVG dropdown icon-->
							<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
								<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
							</svg>
						</a>
						<ul class="button-list-inner-list">
							<?php else : ?>
							<li>
 								<a class="<?php echo ($colored) ? 'green_btn' : 'transparent_btn gray_btn';?>"  href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
									<?php echo esc_html($title); ?>
								</a>
								

							</li>
							<?php endif;
							if ($index === count($buttons_list) - 1) :
							?>
						</ul>
						<?php endif; ?>
						<?php endforeach; ?>
					</li>
				</ul>
			</nav>
			<?php
			elseif ($buttons_list && count($buttons_list) === 1) :
				$title = $buttons_list[0]['link']['title'];
				$url = $buttons_list[0]['link']['url'];
				$target = $buttons_list[0]['link']['target'];
			?>
			<a class="<?php echo ($colored) ? 'green_btn' : 'transparent_btn gray_btn'; ?>" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
				<?php echo esc_html($title); ?>
			</a>
			<?php endif; endwhile; endif; ?>
			
        </div>
    </div>
</section>
