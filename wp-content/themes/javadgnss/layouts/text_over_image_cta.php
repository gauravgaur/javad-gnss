<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$link = get_sub_field('button');
$image = get_sub_field('image');
$link_url = $link['url'];
$link_title = $link['title'];
$link_target = $link['target'] ? $link['target'] : '_self';
?>
<section class="outer__design">
			<div class="container">
				<div class="full_content <?php if(get_sub_field('reverse_columns')) echo ' flip_strip clearfix';?>">
					<div class="oneThird_img">
						<?php if($image):?>
						<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"></a>
							<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
						
						<?php endif;?>
					</div>
					<div class="overlap_grid">
						<div class="container">
							<div class="overlap_Content" data-aos="fade-up" data-aos-duration="1500">
							<?php if(get_sub_field('top_right_tag')):?>	
							<div class="add_new clearfix">
								<h4><?php echo get_sub_field('top_right_tag');?></h4>
							</div>
							<?php endif;?>
								<div class="shift_content">
									<?php if($title):?>
									<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><h4><?php echo $title;?></h4></a>
									<?php endif;
									if($description) echo $description;
									if( $link ): 
										?>
								</div>
								<a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
									<span class="linkText"><?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
								</a>
                                <?php endif;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>