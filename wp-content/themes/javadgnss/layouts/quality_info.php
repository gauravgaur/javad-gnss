<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$list_title = get_sub_field('list_title');
$bottom_text = get_sub_field('bottom_text');
$button = get_sub_field('button');
?>
<section class="quality_info">
    <div class="container">
        <div class="quality_content" data-aos="fade-up" data-aos-duration="1500">
            <?php if($title):?>
            <h4><?php echo $title;?></h4>
            <?php endif;
            if($description) echo $description;
            if($list_title):
            ?>
            <h5><?php echo $list_title;?></h5>
            <?php endif;
            if(have_rows('list')):
            ?>
            <ul class="doc_number">
                <?php while(have_rows('list')):the_row();
                $link = get_sub_field('link');
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';            
                ?>
                <li><a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><i class="fas fa-chevron-right"></i> <?php echo esc_html( $link_title ); ?></a></li>
                <?php endwhile;?>
            </ul>
            <?php endif;?>
        </div>
        <div class="contact_infoGrid" data-aos="fade-up" data-aos-duration="1500">
            <?php if($bottom_text):?>
            <h4><?php echo $bottom_text;?></h4>
            <?php endif;?>
            <div class="contact_us_btn">
                <?php if($button):
                    $button_link_url = $button['url'];
                    $button_link_title = $button['title'];
                    $button_link_target = $button['target'] ? $button['target'] : '_self';
                ?>
                <a class="green_btn" href="<?php echo esc_url( $button_link_url ); ?>" target="<?php echo esc_attr( $button_link_target ); ?>"><?php echo esc_html( $button_link_title ); ?></a>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>