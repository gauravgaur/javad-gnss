<section class="">
<div class="container">
    <div class="dealer_app_grid">
        <div class="t_grid" data-aos="fade-up" data-aos-duration="2000">
            <div class="row g-0">
                <div class="col-md-6 col-lg-6">
                    <div class="address_box left_box">
                        <?php if(have_rows('left_list')):?>
                        <ul>
                        <?php while(have_rows('left_list')):the_row();
                        $title = get_sub_field('title');
                        $link = get_sub_field('link');
                        ?>
                            <li>
                                <?php if($title):?>
                                <h4><?php echo $title;?></h4>
                                <?php endif;
                                if( $link ): 
                                    $link_url = $link['url'];
                                    $link_title = $link['title'];
                                    $link_target = $link['target'] ? $link['target'] : '_self';
                                    ?>
                                <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                                <?php endif;?>
                            </li>
                            <?php endwhile;?>
                        </ul>
                        <?php endif;
                        $title_beside_button = get_sub_field('title_beside_button');
                        $left_button = get_sub_field('left_button');
                        ?>
                        <div class="top_border">
                            <div class="view_box dark_gray_bg">
                                <?php if($title_beside_button):?>
                                <h4><?php echo $title_beside_button;?></h4>
                                <?php endif;
                                if( $left_button ): 
                                    $left_button_url = $left_button['url'];
                                    $left_button_title = $left_button['title'];
                                    $left_button_target = $left_button['target'] ? $left_button['target'] : '_self';
                                
                                ?>
                                <a class="arrow_btn" href="<?php echo esc_url( $left_button_url ); ?>" target="<?php echo esc_attr( $left_button_target ); ?>">
                                    <span class="linkText"><?php echo esc_html( $left_button_title ); ?> <i class="fas fa-arrow-right"></i></span>
                                </a>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <div class="address_box right_box">
                        <?php if(have_rows('right_list')):?>
                        <ul>
                            <?php while(have_rows('right_list')):the_row();
                                $content = get_sub_field('list');
                            ?>
                            <li>
                                <?php echo $content;?>
                            <li>
                            <?php endwhile;?>
                        </ul>
                        <?php endif;
                        $title_beside_button_right = get_sub_field('title_beside_button_right');

                        $right_button = get_sub_field('right_button');
                        ?>
                        <div class="top_border">
                            <div class="view_box dark_gray_bg">
                                <?php if($title_beside_button_right):?>
                                <h4><?php echo $title_beside_button_right;?></h4>
                                <?php endif;
                                if( $right_button ): 
                                    $link_url = $right_button['url'];
                                    $link_title = $right_button['title'];
                                    $link_target = $right_button['target'] ? $right_button['target'] : '_self';
                                    ?>
                                <a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                    <span class="linkText"><?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
                                </a>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>					
    </section>