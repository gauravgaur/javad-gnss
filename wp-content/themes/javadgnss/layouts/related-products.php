<?php
$title = get_sub_field('title');
$description = get_sub_field('description');
$products = get_sub_field('products');
$link = get_sub_field('button');
?>
<section>
    <div class="container">
        <div class="product_grid" data-aos="fade-up" data-aos-duration="2000">
            <div class="product_text_content">
                <?php if($title):?>
                <h4><?php echo $title;?></h4>
                <?php endif;
                if($description) echo $description;?>
            </div>
            <?php if($products):?>
            <ul class="product_list">
            <?php foreach( $products as $product ):
				$image = get_field('image', $product);
				?>
                <li>
                    <a href="<?php echo esc_url( get_term_link( $product ) ); ?>">
                        <div class="product_proofile">
							<?php if($image):?>
							<img src="<?php echo $image['url'];?>" alt="<?php echo $product->name;?> Image" />
							<?php endif;?>
                        </div>
                        <span><?php echo $product->name;?></span>
                    </a>
                </li>
            <?php endforeach;?>
            </ul>
            <?php endif;?>
            <?php if($link):
            $link_url = $link['url'];
            $link_title = $link['title'];
            $link_target = $link['target'] ? $link['target'] : '_self';
            ?>
            <div class="text-center">
                <a class="gray_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>