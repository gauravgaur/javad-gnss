<?php $image = get_sub_field('background_image');
$image_url = $image['url'];
$link = get_sub_field('button');?>
<section class="technology_bg" style="background-image:url(<?php if($image) echo $image_url;?>)">
    <div class="container">
        <div class="story_info" data-aos="fade-up" data-aos-duration="1000">
            <?php echo get_sub_field('description');
            if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
                <a class="green_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
            <?php endif; ?>
        </div>
    </div>
</section>