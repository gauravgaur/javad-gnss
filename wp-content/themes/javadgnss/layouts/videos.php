<?php if(get_sub_field('videos')):?>
<section>
    <div class="container">
        <?php
        $videos = get_sub_field('videos');
        $col_count = sizeof($videos);
        $size = ceil(12 / sizeof($videos));
        if($col_count<2) $size = 12;
        $alignment = get_sub_field('video_alignment');
        ?>
        <div class="mt-5">            
            <div class="videoGridSingle justify-content-<?php echo $alignment;?> align-items-start text-center">
                <?php foreach($videos as $video):?>
                    <?php 
                    $videoTitle = $video['video_title'];
                    $enable_popup = $video['enable_popup'];
                    $videoType = $video['video_type'];
                   // $video_id = $video['video_id'];
                    //$urlArr = explode("/",$videoURL);
                    //$urlArrNum = count($urlArr);

                    // Youtube video ID
                    //$youtubeVideoId = $urlArr[$urlArrNum - 1];
                            
                    if($videoType == 'youtube'){    
                        $video_id = $video['youtube_id'];    
                        $embed_code = '<iframe src="//www.youtube.com/embed/'.$video_id.'" allowfullscreen></iframe>';
                        // Generate youtube thumbnail url
                        $thumbURL = 'http://img.youtube.com/vi/'.$video_id.'/0.jpg';
						$videoURL = "https://www.youtube.com/watch?v=".$video_id;
                    } else {                         
                        $video_id = $video['vimeo_id'];     
                        //$vimeo_Arr = explode('/', $videoURL); 
                        //$vimeo_id = array_pop($vimeo_Arr);
                        $embed_code = convertVimeo($video_id);
                        $thumbURL = getVimeoVideoThumbnailByVideoId($video_id, 'large');
						$videoURL = "https://vimeo.com/".$video_id;
                    }?>
                    <div class="video_container">
                        <div class="video_box">
                            <div class="video_img">
                                <img src="<?php echo $thumbURL;?>" alt="<?php echo $videoType;?>">
                                <?php if($enable_popup == true):?> 
                                    <a data-bs-toggle="modal" href="#video_popup_<?php echo $video_id;?>" role="button">
                                <?php else:?>
                                    <a href="<?php echo $videoURL;?>" target="_blank">
                                <?php endif;?>  
                                    <span><img src="<?php echo get_template_directory_uri();?>/_images/video_icon.png" alt="video_icon"></span>
                                </a>
                            </div>  

                            <?php if($videoTitle):?>                           
                                <div class="video_title_inner">
                                    <?php echo $videoTitle;?>
                                </div>
                            <?php endif;?>
                        </div>
                    <?php if($enable_popup == true):?>
                        <div class="modal fade" id="video_popup_<?php echo $video_id;?>" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                            <div class="modal-dialog modal-dialog-centered modal-lg">
                                <div class="modal-content">
                                    <div class="modal_header">
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
                                    </div>
                                    <div class="modal_body">
                                        <div class="o-video">
                                            <?php echo $embed_code;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
                <?php endforeach;?>                
            </div>
        </div>
    </div>
</section>
<?php endif;?>