<section class="py-5 my-3 product_category-section">
    <div class="container">
        <div class="product_content" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="800" data-aos-offset="0">
            <div class="row">
                <div class="col-md-4 col-lg-3 col-xl-3">
                    <div class="product_info">
                        <h3><?php echo get_sub_field('title');?></h3>
                        <?php
                        $link = get_sub_field('link');
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                        <a class="arrow_btn" href="<?php echo $link_url;?>" target="<?php echo $link_target;?>">
                            <span class="earth_icon"><i class="fas fa-globe-americas"></i></span>
                            <span class="linkText"><?php echo $link_title;?> <i class="fas fa-arrow-right"></i></span>
                        </a>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-md-8 col-lg-9 col-xl-9">
                    <?php echo get_sub_field('description');?>
                </div>
            </div>
        </div>
        <div class="row">
        <?php //$custom_terms = get_sub_field('categories');
		// 	$taxonomy = 'categories';
		// 	$args = array(); // whatever arguments you're using
		// 	//$terms = get_terms($taxonomy, $args);
		// 	$count = count($custom_terms);
		// 	for ($i=0; $i<$count; $i++) {
		//   $custom_terms[$i]->homepage_order = get_field('homepage_order',$taxonomy.'_'.$custom_terms[$i]->term_id);
		// }
		//usort($custom_terms, 'my_sort_terms_function');
            //foreach($custom_terms as $custom_term):
               // $term_id = $custom_term->term_id;
               // $image = get_field('illustrated_image', $custom_term);
               while(have_rows('products')):the_row();
               $product_title = get_sub_field('product_title');
               $product_image = get_sub_field('product_image');
               $product_link = get_sub_field('product_link');
               ?>
            <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 col-xxl-2">
                <div class="product_items" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0">
                <a href="<?php echo $product_link; ?>">
                <?php if($product_image):?>
					<img src="<?php echo $product_image['url'];?>" alt="<?php echo $product_title;?> Image" />
                    <?php endif;
                    if($product_title):
                    ?>
                    <h5><?php echo $product_title;?></h5>
                <?php endif;?>
                </a>
                </div>
            </div>
            <?php //endforeach;
            endwhile;?>
        </div>
    </div>
</section>