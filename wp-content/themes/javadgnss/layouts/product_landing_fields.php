<?php 
$heading = get_sub_field('heading');
$pro_categories = get_sub_field('product_categories');
$show_all = get_sub_field('show_all');
//show all categories
if($show_all == true){
    $term = get_term_by('name', get_the_title(), 'categories');
    $termchildren = get_term_children( $term->term_id, 'categories' );?>
    <section class="line_grid">
        <div class="container">
            <div class="product_inner_info" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                <h4><?php echo strip_tags($heading);?></h4>                
            </div>
        </div>
    </section>
    <?php 
    foreach ( $termchildren as $child ) {
        $term = get_term_by( 'id', $child, 'categories' );?>
        <section class="line_grid" id ="<?php echo $term->term_id;?>">
            <div class="container">
                <div class="product_inner_info">
                    <div data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                        <h3><?php echo $term->name;?></h3>
                        <p><?php echo $term->description;?></p>
                    </div>
                    <div class="row">
                        <?php 
                        $args = array( 
                            'post_type' => 'products',
                            'showposts' => -1,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'categories',
                                    'terms' => $child,
                                    'field' => 'term_id',
                                )
                            ),
                            'orderby' => 'title',
                            'order' => 'ASC' 
                        );           
                        
                        $the_query = new WP_Query( $args );
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();?>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxl-3">
                                    <div class="see_product" data-aos="fade-up" data-aos-duration="1000">
                                        <?php /*if(get_the_post_thumbnail()):?>
                                            <div class="product_img">
                                                <a href="<?php echo get_the_permalink();?>"> <?php echo get_the_post_thumbnail();?> </a>
                                            </div>
                                        <?php endif;*/?>

                                        <div class="product_title_info">
                                            <a href="<?php echo get_field('external_product_link',get_the_ID());?>"><strong><?php echo get_the_title();?></strong> </a>  
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            wp_reset_postdata();
                        }?>
                    </div>
                </div>
            </div> 
        </section>
        <?php }?>
<?php } else {
    //show manually added categories?>
    <section class="line_grid">
        <div class="container">
            <div class="product_inner_info" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                <h4><?php echo $heading;?></h4>
                <?php if(count($pro_categories) >= 1):?>
                <div class="btn_info">
                    <?php
                    foreach ( $pro_categories as $child ) {
                        $unique_id = str_replace(" ", "_", strtolower($child['category_title']));
                        echo '<a class="green_btn" href="#'.$unique_id.'">' . $child['category_title'] . '</a>';
                    }?>
                </div>
                <?php endif;?>
            </div>
        </div>
    </section>
    <?php 
    if(count($pro_categories) >= 1){
        foreach ( $pro_categories as $category ) {
        $unique_id = str_replace(" ", "_", strtolower($category['category_title']));?>
        <section class="line_grid" id ="<?php echo $unique_id;?>">
            <div class="container">
                <div class="product_inner_info">
                    <div data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                        <h3><?php echo $category['category_title'];?></h3>
                        <p><?php echo $category['category_description'];?></p>
                    </div>
                    <div class="row">
                        <?php 
                        $args = array( 
                            'post_type' => 'products',                  
                            'orderby' => 'title',
                            'order' => 'ASC',
                            'post__in' => $category['products']
                        );            
                        
                        $the_query = new WP_Query( $args );
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();?>
                                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxl-3">
                                    <div class="see_product" data-aos="fade-up" data-aos-duration="1000">
                                        <div class="product_img">
                                        <a href="<?php echo get_the_permalink();?>"> <?php echo get_the_post_thumbnail();?> </a>
                                        </div>
                                        <div class="product_title_info">
                                        <a href="<?php echo get_the_permalink();?>"><strong><?php echo get_the_title();?></strong> </a>
                                            <a class="arrow_btn" href="<?php echo get_the_permalink();?>">
                                                <span class="linkText">Explore <i class="fas fa-arrow-right"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            wp_reset_postdata();
                        }?>
                    </div>
                </div>
            </div>
        </section>
        <?php }
    }?>
<?php }?>
