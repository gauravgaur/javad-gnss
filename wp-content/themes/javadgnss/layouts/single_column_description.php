<?php $description = get_sub_field('description');?>
<section class="mt-5 mb-5">
			<div class="container">
				<div class="traning_grid text-center" data-aos="fade-up" data-aos-duration="1500">
					<?php if($description) echo $description;?>
				</div>
			</div>
		</section>