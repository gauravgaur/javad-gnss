<?php
$form_title = get_sub_field('form_title');
$form_description = get_sub_field('form_description');
?>
<section class="">
	<div class="container">
		<div class="dealer_app_grid">
			<div class="form_grid">
				<div class="form_title text-start" data-aos="fade-up" data-aos-duration="2000">
					<?php if($form_title):?>
					<h4 <?php if(get_sub_field('center_align_title')) echo 'class="text-center"';?>><?php echo $form_title;?></h4>
					<?php endif;
					if($form_description) echo $form_description;?>
				</div>
				<?php echo get_sub_field('form_shortcode');?>
			</div>
		</div>
	</div>
</section>