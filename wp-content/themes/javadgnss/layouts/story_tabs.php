<?php $title = get_sub_field('title');?>
<section class="timeLine_section">
    <div class="container">
        <div class="time_line_content">
            <?php if($title):?>
            <h3><?php echo $title;?></h3>
            <?php endif;?>
            <div class="timeLine_tabs">
                <div class="arrow_line">
                    <span class="prev_btn"><i class="fas fa-chevron-left"></i></span>
                    <?php if(have_rows('tabs')):
                        $i = 0;
                        ?>
                    <ul class="nav">
                        <?php while(have_rows('tabs')):the_row();
                            $title = get_sub_field('title');
                        ?>
                        <li><button class="<?php if($i==0) echo 'active';?>" data-bs-toggle="pill" data-bs-target="#year-<?php echo $i;?>" type="button" role="tab" aria-selected="<?php echo ($i==0) ? 'true' : 'false';?>"><?php echo $title;?></button></li>
                        <?php 
                        $i++;
                        endwhile;?>
                    </ul>
                    <?php endif;?>
                    <span class="next_btn"><i class="fas fa-chevron-right"></i></span>
                </div>
                <?php if(have_rows('tabs')):
                        $j = 0;
                        ?>
                <div class="tab-content">
                <?php while(have_rows('tabs')):the_row();
                    $title = get_sub_field('title');
                    $description = get_sub_field('description');
                ?>
                    <div class="tab-pane fade <?php if($j==0) echo 'show active';?>" id="year-<?php echo $j;?>" role="tabpanel" aria-labelledby="pills-year-<?php echo $j;?>">
                        <div class="tab_content_inner">
                            <h4><?php echo $title;?></h4>
                            <?php echo $description;?>
                        </div>
                    </div>
                <?php $j++; endwhile;?>
                </div>
                <?php endif;?>
            </div>

        </div>
    </div>
</section>