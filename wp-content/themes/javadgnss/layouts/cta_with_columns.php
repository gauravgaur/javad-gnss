<?php $image_url = get_sub_field('background_image');
$columns = get_sub_field('inner_columns');
$col_count = sizeof($columns);
$size = ceil(12 / sizeof($columns));
if($col_count<2) $size = 12;?>
<section class="box_bg">
    <div class="full_img">
        <?php if($image_url):?>
        <img src="<?php echo $image_url;?>" alt="img">
        <?php endif;?>
    </div>
    <div class="three_box_outer">
        <div class="row">
        <?php 
        foreach($columns as $column):
        	$link = $column['button'];
        	?>
            <div class="col-md-<?php echo $size;?> col-lg-<?php echo $size;?>">
                <div class="service_Content">
                    <h4><?php echo $column['title'];?></h4>
                    <?php 
                    if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <span class="linkText"><?php echo esc_html( $link_title ); ?><i class="fas fa-arrow-right"></i></span>
                        </a>
                    <?php endif; ?>                    
                </div>
            </div>
        <?php endforeach;?>
        </div>
    </div>
</section>