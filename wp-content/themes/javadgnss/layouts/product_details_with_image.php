<?php
$img_url = get_sub_field('image');
$image = aq_resize($img_url['url'],600,600, true);
$reverse_columns = get_sub_field('reverse_columns');
$title = get_sub_field('title');
$description = get_sub_field('description');
// $link = get_sub_field('button');
$buttons_list = get_sub_field('buttons_list');

?>
<!--<div class="button_grid">
	<div class="left_btn"></div>
	<div class="right_btnGroup d-sm-flex">
		<?php
		/**
		*   Datasheets
		*/
		if(is_array(get_field('files_list', get_the_ID()))){
		    $get_files = array_filter(get_field('files_list', get_the_ID()));        
		    if(!empty($get_files)){
		        echo '<div class="datasheets_gallery"><ul>'; 
		        foreach($get_files as $key => $file) {
		            foreach( $file as $key1 => $sub_file):?>
						<?php 
						$pdf_id = '';
						$pdf_name =''; 
						if($key == 'data_sheets_group'){
							$tileName = 'Data Sheet';
							$pdf_name = $sub_file['data_sheets'];
							$pdf_id = $sub_file['data_sheets_file_upload'];
						} elseif($key == 'user_manual_group'){
							$tileName = 'User Manual';
							$pdf_name = $sub_file['user_manual'];
							$pdf_id = $sub_file['user_manual_file_upload'];
						} else {
							$tileName = 'Software Updates';
							$pdf_name = $sub_file['software_updates'];
							$pdf_id = $sub_file['software_updates_file_upload'];
						}

						if($pdf_id):?>
						<li class="file_items" >
							<a class="btn green_btn darkColor" href="<?php echo wp_get_attachment_url($pdf_id);?>" target="_blank"><?php echo $tileName;?></a>
						</li>    
		            <?php endif;
		            endforeach;
		        }
		        echo '</ul></div>';  
		    }
		}?>
	</div>
</div>-->
<section class="mt-5 <?php if ($reverse_columns) echo 'rotated_row'; ?>">
	<div class="container"> 
		<div class="row align-items-center">
			<div class="col-md-12 col-lg-6">
				<div class="box_content2" data-aos="fade-up" data-aos-duration="1500">
					<?php if ($title) : ?>
						<h4><?php echo $title; ?></h4>
					<?php endif;
					if ($description) echo $description;					
						if ($buttons_list && count($buttons_list) > 1) : ?>
						<nav class="navbar" role="navigation" aria-label="menu">
							<ul class="button-list menu">
								<li class="button-list-item">
									<?php foreach ($buttons_list as $index => $list) :
										$title = $list['link']['title'];
										$url = $list['link']['url'];
										$target = $list['link']['target'];
										$unique_id = $list['unique_ids'];
										if ($index === 0) :
									?>
											<a class="green_btn" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
												<?php echo esc_html($title); ?>
												<!--SVG dropdown icon-->
												<svg class="icon" width="14" height="16" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
													<path d="M151.5 347.8L3.5 201c-4.7-4.7-4.7-12.3 0-17l19.8-19.8c4.7-4.7 12.3-4.7 17 0L160 282.7l119.7-118.5c4.7-4.7 12.3-4.7 17 0l19.8 19.8c4.7 4.7 4.7 12.3 0 17l-148 146.8c-4.7 4.7-12.3 4.7-17 0z" />
												</svg>
											</a>
											<ul class="button-list-inner-list">
											<?php else : ?>
												<li>
													<a class="green_btn" id="<?php echo $unique_id;?>" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
														<?php echo esc_html($title); ?>
													</a>
												</li>
											<?php endif;
										if ($index === count($buttons_list) - 1) :
											?>
											</ul>
										<?php endif; ?>
									<?php endforeach; ?>
								</li>
							</ul>
						</nav>
					<?php
					elseif ($buttons_list && count($buttons_list) === 1) :
						$title = $buttons_list[0]['link']['title'];
						$url = $buttons_list[0]['link']['url'];
						$target = $buttons_list[0]['link']['target']; ?>
						<a class="green_btn 7" id="asasasas" href="<?php echo esc_url($url); ?>" target="<?php esc_attr_e($target); ?>">
							<?php echo esc_html($title); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-md-12 col-lg-6">
				<div class="box_img">
					<?php if ($image) : ?>
						<img src="<?php echo $image; ?>" alt="<?php echo $img_url['alt']; ?>">
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>