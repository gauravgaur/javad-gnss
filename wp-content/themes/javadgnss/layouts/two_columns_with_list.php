<section class="mt-5 mb-4">
    <div class="row g-0">
        <div class="col-sm-12 col-md-7 col-lg-8 col-xl-9">
            <div class="description_info" data-aos="fade-up" data-aos-duration="2000">
                <div class="dark_gray">
                    <div class="inner_aline">
                        <?php 
                            if(have_rows('list')):
                        ?>
                        <ul>
                            <?php 
                                while(have_rows('list')):the_row();
                                $title = get_sub_field('title');
                                $description = get_sub_field('description');
                            ?>
                            <li>
                                <?php if($title):?>
                                <h4><?php echo $title;?></h4>
                                <?php endif;
                                if($description) echo $description;?>
                            </li>
                            <?php endwhile;?>
                        </ul>
                        <?php endif;?>
                        <?php 
                        $link = get_sub_field('left_link');
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                        <div class="text-end">
                            <a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                                <span class="linkText"><?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
                            </a>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-4 col-xl-3">
            <div class="story_imgGrid" data-aos="fade-up" data-aos-duration="2000">
                <?php $image = get_sub_field('image');
                if($image):?>
                <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
                <?php endif;
                 $right_link = get_sub_field('right_link');
                 if( $right_link ): 
                     $right_link_url = $right_link['url'];
                     $right_link_title = $right_link['title'];
                     $right_link_target = $right_link['target'] ? $right_link['target'] : '_self';
                     ?>
                <a class="arrow_btn" href="<?php echo esc_url( $right_link_url ); ?>" target="<?php echo esc_attr( $right_link_target ); ?>">
                    <span class="linkText"><?php echo esc_html( $right_link_title ); ?>  <i class="fas fa-arrow-right"></i></span>
                    <?php endif;?>
                </a>
            </div>
        </div>
    </div>
</section>