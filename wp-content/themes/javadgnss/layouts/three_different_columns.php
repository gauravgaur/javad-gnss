<?php 
$title = get_sub_field('title');
$description = get_sub_field('description');
$image = get_sub_field('image');
?>
<section class="mt-5 mb-5">
			<div class="container">
				<div class="inovation_grid">
					<?php if($title):?>
					<h4 data-aos="fade-up" data-aos-duration="1000"><?php echo $title;?></h4>
					<?php endif;?>
					<div class="row">
						<div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="inovatiob_content" data-aos="fade-up" data-aos-duration="1000">
								<?php if($description) echo $description;
								if($image):
								?>
								<div class="inovation_img">
									<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
								</div>
								<?php endif;?>
							</div>
						</div>
						<div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="inovatiob_content" data-aos="fade-up" data-aos-duration="1500">
								<?php if(get_sub_field('center_title')):?>
								<h5><?php echo get_sub_field('center_title');?></h5>
								<?php endif;
								if(get_sub_field('center_description')) echo get_sub_field('center_description');?>
							</div>
						</div>
						<?php 
							$right_title = get_sub_field('right_title');
							$link = get_sub_field('link');
							$background_image = get_sub_field('background_image');
						?>
						<div class="col-sm-6 col-md-6 col-lg-6 col-xl-4">
							<div class="inovatiob_content" data-aos="fade-up" data-aos-duration="2000">
								<div class="inovation_img">
									<?php if($background_image):?>
									<img src="<?php echo $background_image['url'];?>" alt="<?php echo $background_image['alt'];?>">
									<?php endif;?>
									<div class="story_explore">
										<?php if($right_title):?>
										<strong><?php echo $right_title;?></strong>
										<?php endif;
										if( $link ): 
											$link_url = $link['url'];
											$link_title = $link['title'];
											$link_target = $link['target'] ? $link['target'] : '_self';
											?>
										<a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
											<span class="linkText"><i class="fas fa-globe-americas"></i> <?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
										</a>
										<?php endif;?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>