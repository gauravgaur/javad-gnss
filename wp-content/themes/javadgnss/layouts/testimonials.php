<section class="lightGary_bg pt-4 pb-4 mt-5 testimonial_section">
    <div class="container">
        <div class="testimonal_silder" data-aos="fade-up" data-aos-duration="1500">
            <?php if(get_sub_field('main_title')):?>
            <h4><?php echo get_sub_field('main_title');?></h4>
            <?php endif;?>
            <?php if(have_rows('cards')):?>
            <div id="testimonal_silder" class="owl-carousel owl-theme">
                <?php while(have_rows('cards')):the_row();
                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                ?>
                <div class="item">
                    <div class="user_info">
                        <div class="user_img">
                            <?php if($image):?>
                            <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
                            <?php endif;?>
                        </div>
                        <strong><?php echo $title;?></strong>
                        <div class="scroll_content">
                            <?php echo $description;?>
                        </div>
                    </div>
                </div>
                <?php endwhile;?>
            </div>
            <?php endif;?>
        </div>
    </div>
</section>