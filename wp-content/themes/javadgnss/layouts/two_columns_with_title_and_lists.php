<section class="mt-5">
    <div class="container">
        <div class="help_communities text-start application_dealer" data-aos="fade-up" data-aos-duration="2000">
            <div class="row">
                <div class="col-md-6 col-lg-6">
                    <div class="border_info">
                        <h2><?php echo get_sub_field('title');?></h2>
                        <strong><?php echo get_sub_field('sub_title');?></strong>
                        <?php
                        $link = get_sub_field('button');
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_title = $link['title'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                            ?>
                            <a class="arrow_btn" href="<?php echo $link_url;?>" target="<?php echo $link_target;?>">
                                <span class="linkText"><i class="fas fa-globe-americas"></i> <?php echo $link_title;?> <i class="fas fa-arrow-right"></i></span>
                            </a>
                        <?php endif;?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6">
                    <?php echo get_sub_field('description');?> 
                </div>
            </div>
        </div>
    </div>
</section>