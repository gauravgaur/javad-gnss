<section class="mb-5">
			<div class="container">
				<div class="row">
                    <?php
                    if(have_rows('buttons')):while(have_rows('buttons')):the_row();
                     $title = get_sub_field('title');
                     $link = get_sub_field('link');
                     $full_width = get_sub_field('full_width');?>
					<div class="col-sm-12 <?php echo ($full_width) ? 'col-md-12' : 'col-md-6';?>">
						<div class="view_box dark_gray_bg" data-aos="fade-up" data-aos-duration="1800">
                            <?php if($title):?>
							<h4><?php echo $title;?></h4>
                            <?php endif;
                            if( $link ): 
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                                ?>
							<a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
								<span class="linkText"><?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
							</a>
                            <?php endif;?>
						</div>
					</div>
                    <?php endwhile;endif;?>
				</div>
			</div>
		</section>