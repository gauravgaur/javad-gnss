<?php

/* Better Image Tags

bp_setup_image($image,$size,$class='',$null_alt = false,$no_lazy=false,$altalt=false,$gifalt=false,$no_srcset=false)

This is very similar to the default WordPress function wp_get_attachment_image, but adds a little more functionality. Notably it gets the width and height of svgs which WordPress doesn't do. It also allows for gifs to use the original file since animated gifs do not work at different sizes in WordPress.

*/

function bp_setup_image($image,$size,$class='',$null_alt = false,$altalt=false,$gifalt=false,$no_srcset=false){
  if($image){   
    $append_src = '';
    $img_src = wp_get_attachment_image_src($image, $size);
    $srcset = '';
    $sizes = '';
    if($altalt){
      $alt_text = $altalt;
    }else{
      $alt_text = !$null_alt?get_post_meta($image, '_wp_attachment_image_alt',$size):'';
    }
    $add_class = '';
    if($class){
      $add_class .= ' class="'.$class.'"';
    }
    if (strpos($img_src[0], '.gif') !== false && $gifalt) {
      $img_src = wp_get_attachment_image_url( $image, 'full' );
      return '<img src="'.esc_url( $img_src ).'" alt="'.$alt_text.'"'.$add_class.' loading="lazy">';
    }else{
			if (strpos($img_src[0], '.svg') !== false){
				$xmlget = simplexml_load_file($img_src[0]);
				$xmlattributes = $xmlget->attributes();
				$img_src[1] = round((int) $xmlattributes->width); 
				$img_src[2] = round((int) $xmlattributes->height);
				$lazy = '';
			}
			if(!$no_srcset){
				$srcset = wp_get_attachment_image_srcset($image,$size);
				if($srcset){
					$srcset = ' srcset="'.$srcset.'"';
				}
				$sizes = wp_get_attachment_image_sizes($image,$size);
				if($sizes){
					$sizes = ' sizes="'.$sizes.'"';
				}
			}
      return '<img src="'.esc_url( $img_src[0] ).'" alt="'.$alt_text.'"'.$add_class.$srcset.$sizes.' width="'.$img_src[1].'" height="'.$img_src[2].'">';
    }
  }
}

      //WALKER CLASS
			class submenu_wrap extends Walker_Nav_Menu {
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='dropdown_menu'><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}


// add_filter( 'walker_nav_menu_start_el', 'add_arrow',10,4);
// function add_arrow( $output, $item, $depth, $args ){

// //Only add class to 'top level' items on the 'primary' menu.
// if('primary_menu' == $args->theme_location && $depth === 0 ){
//     if (in_array("menu-item-has-children", $item->classes)) {
//         $output .='<span><i class="fas fa-chevron-down"></i></span>';
//     }
// }
//     return $output;
// }


class ACF_Layout {


  /**
   * Path of where the layout templates are found,
   * relative to the theme template directory.
   */
  const LAYOUT_DIRECTORY = '/layouts/';
  
  
  /**
   * Get Layout
   *
   * @param  {string} $file
   * @param  {array}  $data
   * @return {string}
   */
  static function get_layout($layout, $data = null)
  {
    
    $full_layout_directory = get_stylesheet_directory() . self::LAYOUT_DIRECTORY;
    $layout_file = '{{layout}}.php';
    $find = array('{{layout}}', '_');
    $replace = array($layout, '-');
  
    /* Find a file that matchs this_format */
    $new_layout_file = str_replace($find[0], $replace[0], $layout_file);
  
    if (file_exists($full_layout_directory . $new_layout_file))
    {
    include($full_layout_directory . $new_layout_file);
    return true;
    }
    else
    {
    /* Find a file that matchs this-format */
    $new_layout_file = str_replace($find, $replace, $layout_file);
  
    if (file_exists($full_layout_directory . $new_layout_file))
    {
      include($full_layout_directory . $new_layout_file);
      return true;
    }
    }
  
    /**
     * If no files can be matched,
     * and WP DEBUG is true: show a warning.
     */
    if (WP_DEBUG)
    {
    echo "<pre>ACF_Layout: No layout template found for $layout.</pre>";
    }
    
    return false;
  }
  
  /**
   * Render
   *
   * @param  {string} $layout
   * @return {string}
   */
  static function render($layout, $data = null)
  {
    return self::get_layout($layout, $data);
  }
  }



  function get_breadcrumb() {
    if (is_singular()) {
      ?>
//yoast_breadcrumb( '</p><p id=“breadcrumbs”>','</p><p>' );


        <li><a class ="text-uppercase"href="<?php echo get_page_link(54); ?>"><?php echo get_post_type();?> <i class="fas fa-chevron-right"></i></a></li> <li class="text-uppercase"><?php echo get_the_title();?></li>
<?php }
  }

function convertYoutube($string) {
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe src=\"//www.youtube.com/embed/$2\" allowfullscreen></iframe>",
        $string
    );
}

function convertVimeo($string) {
    return "<iframe src=\"//player.vimeo.com/video/$string\" allowfullscreen webkitAllowFullScreen mozallowfullscreen></iframe>";
}

function getVimeoVideoThumbnailByVideoId( $id = '', $thumbType = 'medium' ) {

    $id = trim( $id );

    if ( $id == '' ) {
        return FALSE; 
    }

    $apiData = unserialize( file_get_contents( "http://vimeo.com/api/v2/video/$id.php" ) );
    

    if ( is_array( $apiData ) && count( $apiData ) > 0 ) {

        $videoInfo = $apiData[ 0 ];

        switch ( $thumbType ) {
            case 'small':
                return $videoInfo[ 'thumbnail_small' ];
                break;
            case 'large':
                return $videoInfo[ 'thumbnail_large' ];
                break;
            case 'medium':
                return $videoInfo[ 'thumbnail_medium' ];
            default:
                break;
        }

    }

    return FALSE;

}

//Sort products homepage
function my_sort_terms_function($a, $b) {
  // this function expects that items to be sorted are objects and
  // that the property to sort by is $object->sort_order
  if ($a->homepage_order == $b->homepage_order) {
    return 0;
  } elseif ($a->homepage_order < $b->homepage_order) {
    return -1;
  } else {
    return 1;
  }
}
?>
