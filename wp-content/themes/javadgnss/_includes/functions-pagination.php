<?php
// use bp_pagination(); to display

/**
  * How to use this function

  * For default wordpress loop you can add the pagination function as 
  * bp_pagination($pages = '', $range = 2,"Posts"); // Posts is title aria label pagination as   * * required e.g. "aria-label=Pagination for Posts"
  * For Custom wordpress loop You will need to pass the query object as 
  * bp_pagination($additional_loop->max_num_pages, $range = 2,"Custom Post Type");
*/

function bp_pagination($pages = '', $range = 2, $cpt_title)
{  
     $showitems = ($range * 2)+1;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo '<nav class="bp_pagination" aria-labelledby="bp_pagination_title"><h2 id="bp_pagination_title" class="screen-reader-text">Pagination for '.$cpt_title.'</h2><ul><li><span>Page '.$paged.' of '.$pages.'</span></li>';
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link(1)."' aria-label='Go back to the first page'>&laquo; First</a></a>";
         if($paged > 1 && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged - 1)."' aria-label='Go back to the previous page'>&lsaquo;</a></li>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<li><span class='current' aria-label='Current page, page ".$i."' aria-current='true'>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class='inactive' aria-label='Goto Page ".$i."' >".$i."</a></li>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($paged + 1)."' aria-label='Next Page'>&rsaquo;</a></li>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li><a href='".get_pagenum_link($pages)."' aria-label='Go to to the last page'>Last &raquo; </a></li>";
         echo "</ul></nav>\n";
     }
}

// News Articles Pagination Starts
function javad_news_pagination($pages = '', $range = 2, $paged=1)
{
  $showitems = ($range * 2)+1;
  if($pages == '')
    $pages = 1;

  if(1 != $pages)
  {
	$disabled_prev_class=$disabled_next_class='';
	if($paged==1){ $disabled_prev_class = "disabled"; }
	if($paged == $pages){ $disabled_next_class = "disabled"; }
    $html = '<nav class="bp_pagination col-lg-12" aria-labelledby="bp_pagination_title"><ul>';
    if($paged > 1 && $showitems < $pages) {
      $html .= "<li class='previous'><a href='javascript:void();' aria-label='Previous page' onclick='filternews(". ($paged - 1) .")'></a></li>";
    }else{
      /*$html .= "<li class='previous ".$disabled_prev_class."'><a href='".get_pagenum_link($paged - 1)."' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";*/
	  $html .= "<li class='previous ".$disabled_prev_class."'><a href='javascript:void();' onclick='filternews(". ($paged - 1) .")' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";
    }


    for ($i=1; $i <= $pages; $i++)
    {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
      {
        $html .= ($paged == $i)? "<li class='current'><span aria-label='Current page, page ".$i."' aria-current='true'>".$i."</span></li>":"<li><a href='javascript:void();' class='inactive' aria-label='Goto Page ".$i."' onclick='filternews(". $i .")' >".$i."</a></li>";
      }
    }

    if ($paged < $pages && $showitems < $pages) {
      $html .= "<li class='next'><a href='javascript:void();' aria-label='Next Page' onclick='filternews(" . ($paged + 1) . ")'></a></li>";
    }else{
      /*$html .= "<li class='next ".$disabled_next_class."'><a href='".get_pagenum_link($paged + 1)."' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";*/
		$html .= "<li class='next ".$disabled_next_class."'><a href='javascript:void();'  onclick='filternews(" . ($paged + 1) . ")' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";
    }
    $html .= "</ul></nav>\n";
    return $html;
  }
}
// News Articles Pagination Ends


// Events Articles Pagination Starts
function javad_events_pagination($pages = '', $range = 2, $paged=1)
{
  $showitems = ($range * 2)+1;
  if($pages == '')
    $pages = 1;

  if(1 != $pages)
  {
	$disabled_prev_class=$disabled_next_class='';
	if($paged==1){ $disabled_prev_class = "disabled"; }
	if($paged == $pages){ $disabled_next_class = "disabled"; }
    $html = '<nav class="bp_pagination col-lg-12" aria-labelledby="bp_pagination_title"><ul>';
    if($paged > 1 && $showitems < $pages) {
      $html .= "<li class='previous'><a href='javascript:void();' aria-label='Previous page' onclick='filterevents(". ($paged - 1) .")'></a></li>";
    }else{
      /*$html .= "<li class='previous ".$disabled_prev_class."'><a href='".get_pagenum_link($paged - 1)."' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";*/
		$html .= "<li class='previous ".$disabled_prev_class."'><a href='javascript:void();' onclick='filterevents(". ($paged - 1) .")' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";
    }


    for ($i=1; $i <= $pages; $i++)
    {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
      {
        $html .= ($paged == $i)? "<li class='current'><span aria-label='Current page, page ".$i."' aria-current='true'>".$i."</span></li>":"<li><a href='javascript:void();' class='inactive' aria-label='Goto Page ".$i."' onclick='filterevents(". $i .")' >".$i."</a></li>";
      }
    }

    if ($paged < $pages && $showitems < $pages) {
      $html .= "<li class='next'><a href='javascript:void();' aria-label='Next Page' onclick='filterevents(" . ($paged + 1) . ")'></a></li>";
    }else{
      /*$html .= "<li class='next ".$disabled_next_class."'><a href='".get_pagenum_link($paged + 1)."' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";*/
		$html .= "<li class='next ".$disabled_next_class."'><a href='javascript:void();'  onclick='filterevents(" . ($paged + 1) . ")' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";
    }
    $html .= "</ul></nav>\n";
    return $html;
  }
}
// Events Articles Pagination Ends
// 


	// Workshop Pagination Starts
function javad_workshop_pagination($pages = '', $range = 2, $paged=1)
{
  $showitems = ($range * 2)+1;
  if($pages == '')
    $pages = 1;

  if(1 != $pages)
  {
	$disabled_prev_class=$disabled_next_class='';
	if($paged==1){ $disabled_prev_class = "disabled"; }
	if($paged == $pages){ $disabled_next_class = "disabled"; }
    $html = '<nav class="bp_pagination col-lg-12" aria-labelledby="bp_pagination_title"><ul>';
    if($paged > 1 && $showitems < $pages) {
      $html .= "<li class='previous'><a href='javascript:void();' aria-label='Previous page' onclick='filterworkshop(". ($paged - 1) .")'></a></li>";
    }else{
      /*$html .= "<li class='previous ".$disabled_prev_class."'><a href='".get_pagenum_link($paged - 1)."' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";*/
		$html .= "<li class='previous ".$disabled_prev_class."'><a href='javascript:void();' onclick='filterworkshop(". ($paged - 1) .")' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";
    }


    for ($i=1; $i <= $pages; $i++)
    {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
      {
        $html .= ($paged == $i)? "<li class='current'><span aria-label='Current page, page ".$i."' aria-current='true'>".$i."</span></li>":"<li><a href='javascript:void();' class='inactive' aria-label='Goto Page ".$i."' onclick='filterworkshop(". $i .")' >".$i."</a></li>";
      }
    }

    if ($paged < $pages && $showitems < $pages) {
      $html .= "<li class='next'><a href='javascript:void();' aria-label='Next Page' onclick='filterworkshop(" . ($paged + 1) . ")'></a></li>";
    }else{
     /* $html .= "<li class='next ".$disabled_next_class."'><a href='".get_pagenum_link($paged + 1)."' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li> */
	  $html .= "<li class='next ".$disabled_next_class."'><a href='javascript:void();' onclick='filterworkshop(" . ($paged + 1) . ")' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";
    }
    $html .= "</ul></nav>\n";
    return $html;
  }
}
// Workshop Pagination Ends

// // Workshop TAB Pagination Starts
function javad_workshop_tab_pagination($pages = '', $range = 2, $paged=1, $catID)
{
  $showitems = ($range * 2)+1;
  if($pages == '')
    $pages = 1;

  if(1 != $pages)
  {
	$disabled_prev_class=$disabled_next_class='';
	if($paged==1){ $disabled_prev_class = "disabled"; }
	if($paged == $pages){ $disabled_next_class = "disabled"; }
    $html = '<nav class="bp_pagination col-lg-12" aria-labelledby="bp_pagination_title"><ul>';
    if($paged > 1 && $showitems < $pages) {
      $html .= "<li class='previous'><a href='javascript:void();' aria-label='Previous page' class='filter_tab' data-pagenum=".($paged-1)."  data-catID=".$catID." ></a></li>";
    }else{
      /*$html .= "<li class='previous ".$disabled_prev_class."'><a href='".get_pagenum_link($paged - 1)."' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";*/
	  $html .= "<li class='previous ".$disabled_prev_class."'><a href='javascript:void();' class='filter_tab' data-pagenum=".($paged-1)."  data-catID=".$catID." aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";
    }


    for ($i=1; $i <= $pages; $i++)
    {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
      {
        $html .= ($paged == $i)? "<li class='current'><span aria-label='Current page, page ".$i."' aria-current='true'>".$i."</span></li>":"<li><a href='javascript:void();' class='inactive filter_tab' aria-label='Goto Page ".$i."' data-pagenum=".$i." data-catID=".$catID." >".$i."</a></li>";
      }
    }

    if ($paged < $pages && $showitems < $pages) {
      $html .= "<li class='next'><a href='javascript:void();' aria-label='Next Page' class='filter_tab' data-pagenum=".($paged+1)."  data-catID=".$catID." ></a></li>";
    }else{
      /*$html .= "<li class='next ".$disabled_next_class."'><a href='".get_pagenum_link($paged + 1)."' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";*/
		$html .= "<li class='next ".$disabled_next_class."'><a href='javascript:void();' class='filter_tab' data-pagenum=".($paged+1)." data-catID=".$catID." aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";
    }
    $html .= "</ul></nav>\n";
    return $html;
  }
}
// Workshop TAB Pagination Ends


	// // Workshop TAB Pagination Starts
function javad_workshop_tab_all_pagination($pages = '', $range = 2, $paged=1)
{
  $showitems = ($range * 2)+1;
  if($pages == '')
    $pages = 1;

  if(1 != $pages)
  {
	$disabled_prev_class=$disabled_next_class='';
	if($paged==1){ $disabled_prev_class = "disabled"; }
	if($paged == $pages){ $disabled_next_class = "disabled"; }
    $html = '<nav class="bp_pagination col-lg-12" aria-labelledby="bp_pagination_title"><ul>';
    if($paged > 1 && $showitems < $pages) {
      $html .= "<li class='previous'><a href='javascript:void();' aria-label='Previous page' class='filter_tab_all' data-pagenum=".($paged-1)." ></a></li>";
    }else{
      /* $html .= "<li class='previous ".$disabled_prev_class."'><a href='".get_pagenum_link($paged - 1)."' aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>"; */
	  $html .= "<li class='previous ".$disabled_prev_class."'><a href='javascript:void();' class='filter_tab_all' data-pagenum=".($paged-1)." aria-label='Go back to the previous page'><i class='fa fa-chevron-left'></i></a></li>";
    }


    for ($i=1; $i <= $pages; $i++)
    {
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
      {
        $html .= ($paged == $i)? "<li class='current'><span aria-label='Current page, page ".$i."' aria-current='true'>".$i."</span></li>":"<li><a href='javascript:void();' class='inactive filter_tab_all' aria-label='Goto Page ".$i."' data-pagenum=".$i." >".$i."</a></li>";
      }
    }

    if ($paged < $pages && $showitems < $pages) {
      $html .= "<li class='next'><a href='javascript:void();' aria-label='Next Page' class='filter_tab_all' data-pagenum=".($paged+1)." ></a></li>";
    }else{
      /*$html .= "<li class='next ".$disabled_next_class."'><a href='".get_pagenum_link($paged + 1)."' aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";*/
	  $html .= "<li class='next ".$disabled_next_class."'><a href='javascript:void();' class='filter_tab_all' data-pagenum=".($paged+1)." aria-label='Next Page'>Next <i class='fa fa-chevron-right'></i></a></li>";
    }
    $html .= "</ul></nav>\n";
    return $html;
  }
}
// Workshop TAB Pagination Ends