<?php
/***************************************************************
	Enqueue all scripts and styles.
	1. JS In the footer of course
  2. Load local versions of CSS, JS, and fonts rather than CDNs
  3. Do not use time-based cache busting for file versioning. Ever.
***************************************************************/
/**
	*    Table of Contents
	*1.  File Versioning for deployment
  *2.  Enqueue everything
**/

//1. File Versioning for deployment
  //The versioning below can be used to cache bust files either using webhook to the BluePrint Refresh plugin or manually in /wp-admin/options.php via "bp_wp_version" option. Plugin also clears common caches used by BP.

global $bp_version;
if(get_option( 'bp_wp_version')){
  $bp_version = get_option( 'bp_wp_version');
}else{
  add_option( 'bp_wp_version', 'local_1' );
  $bp_version = get_option( 'bp_wp_version');
}
// 2. Enqueue everything
function bp_nq_scripts() {
	global $bp_version;
	wp_enqueue_style( 'style', get_stylesheet_directory_uri(),null, time());
	wp_enqueue_style( 'font-awesome','https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css',null,$bp_version);
	wp_enqueue_style( 'typekit','https://use.typekit.net/sqi2xaj.css',null,$bp_version);
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri().'/css/bootstrap.min.css',null,$bp_version);
	wp_enqueue_style( 'owl-carousel', get_stylesheet_directory_uri().'/css/owl.carousel.min.css',null,$bp_version);
	wp_enqueue_style( 'owl-theme', get_stylesheet_directory_uri().'/css/owl.theme.default.min.css',null,$bp_version);
	wp_enqueue_style( 'aos', get_stylesheet_directory_uri().'/css/aos.css',null,$bp_version);
	wp_enqueue_style( 'custom-style', get_stylesheet_directory_uri().'/css/style.css',null,$bp_version);
	//   wp_enqueue_style( 'responsive', get_stylesheet_directory_uri().'/css/responsive.css',null,$bp_version);

	wp_enqueue_script ('bp-script', get_stylesheet_directory_uri() . '/_js/js.js', array('jquery'), $bp_version, true);
	wp_enqueue_script ('popper', get_stylesheet_directory_uri() . '/_js/popper.min.js', array('jquery'), $bp_version, true);
	wp_enqueue_script ('bootstrap_js', get_stylesheet_directory_uri() . '/_js/bootstrap.min.js', array('jquery'), $bp_version, true);
	wp_enqueue_script ('jquery', get_stylesheet_directory_uri() . '/_js/jquery-3.6.0.min.js', array('jquery'), $bp_version, true);
	wp_enqueue_script ('aos_js', get_stylesheet_directory_uri() . '/_js/aos.js', array('jquery'), $bp_version, true);
	wp_enqueue_script ('owl-carousel-js', get_stylesheet_directory_uri() . '/_js/owl.carousel.min.js', array('jquery'), $bp_version, true);
	wp_enqueue_script ('custom_js', get_stylesheet_directory_uri() . '/_js/custom.js', array('jquery'), $bp_version, true);
	wp_enqueue_script ('callout_ajax_js', get_stylesheet_directory_uri() . '/_js/callout_ajax.js', array('jquery'), $bp_version, true);
	wp_localize_script('custom_js', 'ajax_object', array(
		'ajaxurl' => admin_url('admin-ajax.php'),
		'loading_img'=> site_url().'/wp-content/uploads/2022/03/loading.gif'
	));
	wp_localize_script('wpsl-js', 'ajax_object', array(
		'ajaxurl' => admin_url('admin-ajax.php')
	));	
}
add_action('wp_enqueue_scripts', 'bp_nq_scripts');