<?php
/***************************************************************
Menus, Custom Post Types, Sidebars, etc...
    1. Menus
    2. Custom Posts Types	
    3. Sidebars		
***************************************************************/

// 1.  Menus
add_action( 'after_setup_theme', 'bp_register_menus' );
function bp_register_menus() {
	register_nav_menus( array(
		'primary_menu' => 'Primary Menu',
		'footer_menu' => 'Footer Menu',
	) );
}
// 3. Register Custom Post Types

// 4. Sidebars

// 5. ACF Options

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Applications Settings',
		'menu_title'	=> 'Applications Settings',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Products Settings',
		'menu_title'	=> 'Products Settings',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'News & Events Settings',
		'menu_title'	=> 'News & Events Settings',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Workshop Settings',
		'menu_title'	=> 'Workshop Settings',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Resources',
		'menu_title'	=> 'Resources',
		'parent_slug'	=> 'general-resources',
	));

}

add_theme_support( 'custom-logo' );


//Registering Custom Post Products
function add_product_custom_post_type() {
 
	// Set UI labels for Custom Post Type
		$labels = array(
			'name'                => _x( 'Products', 'Post Type General Name', 'javadgnss' ),
			'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'javadgnss' ),
			'menu_name'           => __( 'Products', 'javadgnss' ),
			'parent_item_colon'   => __( 'Parent Product', 'javadgnss' ),
			'all_items'           => __( 'All Products', 'javadgnss' ),
			'view_item'           => __( 'View Product', 'javadgnss' ),
			'add_new_item'        => __( 'Add New Product', 'javadgnss' ),
			'add_new'             => __( 'Add New', 'javadgnss' ),
			'edit_item'           => __( 'Edit Product', 'javadgnss' ),
			'update_item'         => __( 'Update Product', 'javadgnss' ),
			'search_items'        => __( 'Search Product', 'javadgnss' ),
			'not_found'           => __( 'Not Found', 'javadgnss' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'javadgnss' ),
		);
		 
	// Set other options for Custom Post Type
		 
		$args = array(
			'label'               => __( 'Products', 'javadgnss' ),
			'description'         => __( 'Product news and reviews', 'javadgnss' ),
			'labels'              => $labels,
			// Features this CPT supports in Post Editor
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'page-attriutes' ),
			// You can associate this CPT with a taxonomy or custom taxonomy. 
			'taxonomies'          => array( 'categories' ),
			/* A hierarchical CPT is like Pages and can have
			* Parent and child items. A non-hierarchical CPT
			* is like Posts.
			*/ 
			'rewrite' => array("slug" => "product/%category%" ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'show_in_rest' => true,
	 	
		);
		 
		// Registering your Custom Post Type
		register_post_type( 'Products', $args );
	 
	}


	
	 
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
	
add_action( 'init', 'add_product_custom_post_type', 0 );


//Taxnonomy For Products
add_action( 'init', 'create_products_taxonomy', 0 );
 
//create a custom taxonomy name it "type" for your posts
function create_products_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Category' ), 
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add New Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'menu_name' => __( 'Categories' ),
  ); 	
 
  register_taxonomy('categories',array('products'), array(
    'hierarchical' => true, 
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'products_cat' ),
  ));


  $labels1 = array(
    'name' => _x( 'Product Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Taga' ),
    'all_items' => __( 'All Taga' ),
    'parent_item' => __( 'Parent Tag' ),
    'parent_item_colon' => __( 'Parent Tag:' ),
    'edit_item' => __( 'Edit Tag' ), 
    'update_item' => __( 'Update Tag' ),
    'add_new_item' => __( 'Add New Tag' ),
    'new_item_name' => __( 'New Tag Name' ),
    'menu_name' => __( 'Tags' ),
  ); 	

  register_taxonomy('product_tag',array('products'), array(
    'hierarchical' => true,
    'labels' => $labels1,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'product_tag' ),
  ));
}

//Events CPT
add_action('init', 'events_init');
function events_init() {
	$args = array(
		'labels' => array(
			'name' => __('Events'),
			'singular_name' => __('Event'),
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array("slug" => "events"), 
		'supports' => array('thumbnail','editor','title','custom-fields','excerpt')
	);
	register_post_type( 'events' , $args );
}

add_action('init', 'news_init');
function news_init() {
	$args = array(
		'labels' => array(
			'name' => __('News'),
			'singular_name' => __('News'),
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array("slug" => "news"), 
		'supports' => array('thumbnail','editor','title','custom-fields','excerpt')
	);
	register_post_type( 'news' , $args );
	
	//Taxnonomy For News
	$labels = array(
    'name' => _x( 'News Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'News Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search News Categories' ),
    'all_items' => __( 'All News Categories' ),
    'parent_item' => __( 'Parent News Category' ),
    'parent_item_colon' => __( 'Parent News Category:' ),
    'edit_item' => __( 'Edit News Category' ), 
    'update_item' => __( 'Update News Category' ),
    'add_new_item' => __( 'Add News Ca Category' ),
    'new_item_name' => __( 'New News Category Name' ),
    'menu_name' => __( 'News Categories' ),
  ); 	
 
  register_taxonomy('news_categories',array('news'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'news_categories' ),
  ));
	
  // News Tag
  $labels = array(
    'name' => _x( 'News Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'News Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search News  Tags' ),
    'popular_items' => __( 'Popular News Tags' ),
    'all_items' => __( 'All News Tags' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit News Tag' ), 
    'update_item' => __( 'Update News Tag' ),
    'add_new_item' => __( 'Add New Tag' ),
    'new_item_name' => __( 'New Tag Name' ),
    'separate_items_with_commas' => __( 'Separate news tags with commas' ),
    'add_or_remove_items' => __( 'Add or remove news tags' ),
    'choose_from_most_used' => __( 'Choose from the most used news tags' ),
    'menu_name' => __( 'News Tags' ),
  ); 

  register_taxonomy('news-tag','news',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
	'show_admin_column' => true,
	'query_var' => true,
    'rewrite' => array( 'slug' => 'news-tag' ),
  ));
}



//Registering Custom Post Applications
add_action('init', 'custom_post_type_application');
function custom_post_type_application() {
 
	// Set UI labels for Custom Post Type
		$labels = array(
			'name'                => _x( 'Applications', 'Post Type General Name', 'javadgnss' ),
			'singular_name'       => _x( 'Application', 'Post Type Singular Name', 'javadgnss' ),
			'menu_name'           => __( 'Applications', 'javadgnss' ),
			'parent_item_colon'   => __( 'Parent Application', 'javadgnss' ),
			'all_items'           => __( 'All Applications', 'javadgnss' ),
			'view_item'           => __( 'View Application', 'javadgnss' ),
			'add_new_item'        => __( 'Add New Application', 'javadgnss' ),
			'add_new'             => __( 'Add New', 'javadgnss' ),
			'edit_item'           => __( 'Edit Application', 'javadgnss' ),
			'update_item'         => __( 'Update Application', 'javadgnss' ),
			'search_items'        => __( 'Search Applications', 'javadgnss' ),
			'not_found'           => __( 'Not Found', 'javadgnss' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'javadgnss' ),
		);
		 
	// Set other options for Custom Post Type
		 
		$args = array(
			'label'               => __( 'Applications', 'javadgnss' ),
			'description'         => __( 'Applications', 'javadgnss' ),
			'labels'              => $labels,
			// Features this CPT supports in Post Editor
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
			// You can associate this CPT with a taxonomy or custom taxonomy. 
			'taxonomies'          => array( 'categories' ),
			'rewrite' => array("slug" => "application"),
			/* A hierarchical CPT is like Pages and can have
			* Parent and child items. A non-hierarchical CPT
			* is like Posts.
			*/ 
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'show_in_rest' => true,
	 
		);
		 
		// Registering your Custom Post Type
		register_post_type( 'Applications', $args );
	 
	}

add_action('init', 'workshop_init');
function workshop_init() {
	$args = array(
		'labels' => array(
			'name' => __('Workshop'),
			'singular_name' => __('Workshop'),
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array("slug" => "workshop"), 
		'menu_icon' => 'dashicons-clipboard',
		'supports' => array('thumbnail','editor','title','custom-fields','excerpt')
	);
	register_post_type( 'workshop' , $args );
	
	//Taxnonomy For News
	$labels = array(
    'name' => _x( 'Workshop Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Workshop Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Workshop Categories' ),
    'all_items' => __( 'All Workshop Categories' ),
    'parent_item' => __( 'Parent Workshop Category' ),
    'parent_item_colon' => __( 'Parent Workshop Category:' ),
    'edit_item' => __( 'Edit Workshop Category' ), 
    'update_item' => __( 'Update Workshop Category' ),
    'add_new_item' => __( 'Add New Workshop Category' ),
    'new_item_name' => __( 'New Workshop Category Name' ),
    'menu_name' => __( 'Workshop Categories' ),
  ); 	
 
  register_taxonomy('workshop_categories',array('workshop'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'workshop_categories' ),
  ));
	
	// Workshop Tag
  $labels = array(
    'name' => _x( 'Workshop Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'Workshop Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Workshop Tags' ),
    'popular_items' => __( 'Popular Workshop Tags' ),
    'all_items' => __( 'All Workshop Tags' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Workshop Tag' ), 
    'update_item' => __( 'Update Workshop Tag' ),
    'add_new_item' => __( 'Add New Tag' ),
    'new_item_name' => __( 'New Tag Name' ),
    'separate_items_with_commas' => __( 'Separate workshop tags with commas' ),
    'add_or_remove_items' => __( 'Add or remove workshop tags' ),
    'choose_from_most_used' => __( 'Choose from the most used workshop tags' ),
    'menu_name' => __( 'Workshop Tags' ),
  ); 

  register_taxonomy('workshop-tag','workshop',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
	'show_admin_column' => true,
	'query_var' => true,
    'rewrite' => array( 'slug' => 'workshop-tag' ),
  ));
	
}

add_action('init', 'product_training_init');
function product_training_init() {
	$args = array(
		'labels' => array(
			'name' => __('Product Training Videos'),
			'singular_name' => __('Product Training Videos'),
		),
		'public' => true,
		'has_archive' => true,
		'rewrite' => array("slug" => "product_training_videos"), 
		'menu_icon' => 'dashicons-clipboard',
		'supports' => array('thumbnail','editor','title','custom-fields','excerpt')
	);
	register_post_type( 'product_training' , $args );
	
	//Taxnonomy For News
	$labels = array(
    'name' => _x( 'Video Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Video Categories', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Video Category' ),
    'all_items' => __( 'All Video Categories' ),
    'parent_item' => __( 'Parent Videos Category' ),
    'parent_item_colon' => __( 'Parent Video Category:' ),
    'edit_item' => __( 'Edit Video Category' ), 
    'update_item' => __( 'Update Video Category' ),
    'add_new_item' => __( 'Add New Video Category' ),
    'new_item_name' => __( 'New Video Category' ),
    'menu_name' => __( 'Product Training Categories' ),
  ); 	
 
  register_taxonomy('video_categories',array('product_training'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'video_categories' ),
  ));
}



//Registering Custom Post Products
function add_testimonials_custom_post_type() {
 
	// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Testimonials', 'Post Type General Name', 'javadgnss' ),
		'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'javadgnss' ),
		'menu_name'           => __( 'Testimonials', 'javadgnss' ),
		'parent_item_colon'   => __( 'Parent Testimonial', 'javadgnss' ),
		'all_items'           => __( 'All Testimonials', 'javadgnss' ),
		'view_item'           => __( 'View Testimonial', 'javadgnss' ),
		'add_new_item'        => __( 'Add New Testimonial', 'javadgnss' ),
		'add_new'             => __( 'Add New', 'javadgnss' ),
		'edit_item'           => __( 'Edit Testimonial', 'javadgnss' ),
		'update_item'         => __( 'Update Testimonial', 'javadgnss' ),
		'search_items'        => __( 'Search Testimonial', 'javadgnss' ),
		'not_found'           => __( 'Not Found', 'javadgnss' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'javadgnss' ),
	);
		 
	// Set other options for Custom Post Type
		 
	$args = array(
		'label'               => __( 'Testimonials', 'javadgnss' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 15,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'show_in_rest' => true,
	
	);
		
	// Registering your Custom Post Type
	register_post_type( 'testimonials', $args );
	
}	
add_action( 'init', 'add_testimonials_custom_post_type', 0 );