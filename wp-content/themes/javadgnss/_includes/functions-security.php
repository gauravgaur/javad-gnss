<?php
/**
  *Should match BluePrint Stand-alone scripts
  *Table of Contents
  *1.  Disable XML-RPC
  *2.  Remove Rest API from <head> and headers
  *3.  Restrict access to the Rest API
  *4.  Disable application passwords
**/

//1. Disable XML-RPC
add_filter('xmlrpc_enabled', '__return_false');

//2. Remove Rest API from <head> and headers
remove_action('template_redirect', 'rest_output_link_header', 11);
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');

//3. Restrict access to the Rest API to logged in users only
add_filter( 'rest_authentication_errors', function( $result ) {
  if ( ! empty( $result ) ) {
    return $result;
  }
  if ( ! is_user_logged_in() ) {
    return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
  }
  return $result;
});

//4. Disable application passwords
add_filter( 'wp_is_application_passwords_available', '__return_false' );
