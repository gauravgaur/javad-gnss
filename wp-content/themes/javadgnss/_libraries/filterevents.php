<?php
/******Events Ajax Action For Pagination/Filter Start********/
  add_action( 'wp_ajax_filter_events', 'filter_events' );
  add_action( 'wp_ajax_nopriv_filter_events', 'filter_events' );

  function filter_events()
  {
    if ($_POST) {
      extract($_POST);
    }
    $paged = 1;
    if( isset($pagenumber) ){
      if($pagenumber):
        $paged = $pagenumber;
      endif;
    }
    $args = array(
    'post_type' => 'events',
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'paged' => $paged,
    'meta_key' => 'event_start_date',
    'orderby' => 'meta_value',
    'order'	=> 'DESC'
  );
?>
    <!---Loader Container Starts--->
    <div class="col-12" id="loading-container">
      <img class="loading-image" src="<?php echo get_template_directory_uri(); ?>/_images/spinner.svg" />
    </div>
    <!---Loader Container Ends--->
      <?php
      $loopEvents = new WP_Query( $args );
	    $maxNumPages = $loopEvents->max_num_pages;
      if ( $loopEvents->have_posts() ) :
        while ($loopEvents->have_posts()) : $loopEvents->the_post();
		   $event_start_date = get_field('event_start_date', $loopEvents->ID);
		   $event_end_date = get_field('event_end_date', $loopEvents->ID);
		   $events_excerpt = get_the_excerpt($loopEvents->ID);
      ?>
        <div class="col-md-12 col-lg-6">
			<div class="row g-0 dark_gray_bg">
				<div class="col-sm-6 col-md-6">
					<?php if (has_post_thumbnail( $loopEvents->ID ) ): ?>
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loopEvents->ID ), 'single-post-thumbnail' ); ?>
					<div class="news_img"><a href="<?php echo get_the_permalink($loopEvents->ID); ?>"><img src="<?php echo $image[0]; ?>" alt="img"></a></div>
					<?php endif; ?>										
				</div>
				<div class="col-sm-6 col-md-6">
					<div class="info_content_box">
						<?php if($event_start_date): ?><h5><?php echo $event_start_date."-".$event_end_date; ?></h5><?php endif; ?>
						<h4><a href="<?php echo get_the_permalink($loopEvents->ID); ?>"><?php echo get_the_title($loopEvents->ID); ?></a></h4>
						<?php if($events_excerpt): ?><p><?php echo $events_excerpt; ?></p><?php endif; ?>
						<a class="events_excerpt" href="<?php echo get_the_permalink($loopEvents->ID); ?>">
							<span class="linkText">Read More <i class="fas fa-arrow-right"></i></span>
						</a>
					</div>
				</div>
			</div>
</div>
        <?php endwhile; ?>
<div class="col-md-12 col-lg-12">
	<div class="text-center more_post">
		<div class="breadcrum_grid">
        <nav class="pagination">
          <?php $pagination = javad_events_pagination($maxNumPages,2, $paged );
            echo $pagination; ?>
        </nav>
		</div></div></div>
        <?php wp_reset_postdata();
      else: ?>
        <h2><?php echo "No data found! "; ?></h2>
      <?php endif;
    wp_die();
  }
/******Events Ajax Action For Pagination/Filter Ends********/

