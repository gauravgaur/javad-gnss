<?php
/******News Ajax Action For Pagination/Filter Start********/
  add_action( 'wp_ajax_filter_news', 'filter_news' );
  add_action( 'wp_ajax_nopriv_filter_news', 'filter_news' );

  function filter_news()
  {
    if ($_POST) {
      extract($_POST);
    }
    $paged = 1;
    if( isset($pagenumber) ){
      if($pagenumber):
        $paged = $pagenumber;
      endif;
    }
    $args = array(
    'post_type' => 'news',
    'post_status' => 'publish',
    'posts_per_page' => 5,
    'paged' => $paged,
    'meta_key' => 'news_article_start_date',
    //'meta_key' => 'news_article_date',
    'orderby' => 'meta_value',
    'order'	=> 'DESC'
  );
?>
    <!---Loader Container Starts--->
    <div class="col-12" id="loading-container">
      <img class="loading-image" src="<?php echo get_template_directory_uri(); ?>/_images/spinner.svg" />
    </div>
    <!---Loader Container Ends--->
      <?php
      $loopNews = new WP_Query( $args );
	    $maxNumPages = $loopNews->max_num_pages;
      if ( $loopNews->have_posts() ) :
        while ($loopNews->have_posts()) : $loopNews->the_post();
        $news_article_start_date = get_field('news_article_start_date', $loopNews->ID);
        $news_excerpt = get_the_excerpt($loopNews->ID);
      ?>
        <div class="col-md-12 col-lg-6">
            <div class="row g-0 dark_gray_bg">
              <div class="col-sm-6 col-md-6">
              <?php if (has_post_thumbnail( $loopNews->ID ) ): ?>
                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loopNews->ID ), 'single-post-thumbnail' ); ?>
                <div class="news_img"><a href="<?php echo get_the_permalink($loopNews->ID); ?>"><img src="<?php echo $image[0]; ?>" alt="img"></a></div>
                <?php endif; ?>
              </div>
              <div class="col-sm-6 col-md-6">
                <div class="info_content_box">
                  <?php if($news_articleDate): ?><h5><?php echo $news_articleDate; ?></h5><?php endif; ?>
		  <?php if($news_article_start_date): ?><h5><?php echo $news_article_start_date; ?></h5><?php endif; ?>
                  <h4><a href="<?php echo get_the_permalink($loopNews->ID); ?>"><?php echo get_the_title($loopNews->ID); ?></a></h4>
                  <?php if($news_excerpt): ?><p><?php echo $news_excerpt; ?></p><?php endif; ?>
                  <a class="arrow_btn" href="<?php echo get_the_permalink($loopNews->ID); ?>">
                    <span class="linkText">Read More <i class="fas fa-arrow-right"></i></span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        <?php endwhile; wp_reset_postdata(); ?>
<div class="col-md-12 col-lg-12">
	<div class="text-center more_post">
		<div class="breadcrum_grid">
        <nav class="pagination">
          <?php $pagination = javad_news_pagination($maxNumPages,2, $paged );
            echo $pagination; ?>
        </nav>
		</div></div></div>
        <?php 
      else: ?>
        <h2><?php echo "No data found! "; ?></h2>
      <?php endif;
    wp_die();
  }
/******News Ajax Action For Pagination/Filter Ends********/

