<?php 
add_action('wp_ajax_nopriv_javad_product_search_filter', 'javad_product_search_filter');
add_action('wp_ajax_javad_product_search_filter', 'javad_product_search_filter');
function javad_product_search_filter(){
    $tax_query = array();
    $tag_id = (isset($_POST['tag_id'])) ? $_POST['tag_id'] : 'all';
    $product_id = ($_POST['product_id']) ? $_POST['product_id'] : '';
    if($_POST['cat_id']){
        $tax_query[] = array(
            'taxonomy' => 'categories',
            'field'    => 'term_id',
            'terms'    => array($_POST['cat_id']),
            'include_children' => true,
        );
    }
    
	$args = array(
        'post_type'              => array( 'products' ),
        'posts_per_page'         => -1,
        'paged' => $_POST['page'],
        'order'                  => 'DESC',
        'orderby'                => 'id',
        'ignore_sticky_posts'=>true,
        'fields'=>'ids'
    );
    if(count($tax_query)){
        $args['tax_query'] = $tax_query;
    }

    if($product_id){
        $args['post__in'] = array($product_id);
    }
    
    // The Query
    $query = new WP_Query( $args );
    // The Loop
    if ( $query->have_posts() ) {
        $product_ids = $query->posts;
        $count=0;
        foreach($product_ids as $proId){
            if(is_array(get_field('files_list', $proId))){
                $get_files = array_filter(get_field('files_list', $proId)); 
            }
            if(!empty($get_files)){
                foreach($get_files as $key => $file) {
                    foreach( $file as $sub_file):
                       
                    $class = "";
                    // if($tag_id === 'all'){
                    // 	$class = "active";
                    // } elseif($key == trim($tag_id)){
                    // 	$class = "active";
                    // } else {
                    // 	$class = "hidden";
                    // }
                    if($key == trim($tag_id) || $tag_id === 'all'){
                         $count++;
                    ?>
                   <li data-type="<?php echo $key;?>" data-tag="<?php echo $tag_id;?>" class="file_items <?php echo $class;?>" data-index="<?php echo $count;?>">
                        <div class="main_box">
                            <div class="user_head">
                                <div class="cross_img">
                                    <?php if(has_post_thumbnail($proId)):?>
                                        <?php $imageUrl = get_the_post_thumbnail_url($proId, 'full');?>
                                        <img src="<?php echo $imageUrl;?>" alt="<?php echo get_the_title($proId);?>" />
                                      <?php else:?>
                                        <img src="http://javadgnss.com/wp-content/uploads/2022/03/jdrop.png" alt="img">
                                      <?php endif;?>
                                </div>              
                                <span>
                                <?php 
                                    $pdf_id = '';
                                    $pdf_name ='';
                                    if($key == 'data_sheets_group'){
                                        echo 'Data Sheet';
                                        $pdf_name = $sub_file['data_sheets'];
                                        $pdf_id = $sub_file['data_sheets_file_upload'];
                                    } elseif($key == 'user_manual_group'){
                                        echo 'User Manual';
                                        $pdf_name = $sub_file['user_manual'];
                                        $pdf_id = $sub_file['user_manual_file_upload'];
                                    } else {
                                        echo 'Software Updates';
                                        $pdf_name = $sub_file['software_updates'];
                                        $pdf_id = $sub_file['software_updates_file_upload'];
                                    }?>
                                </span>
                            </div>
                            <div class="about_user">
                                <h4><?php echo get_the_title($proId);?></h4>
                                <p><a class="btn green_btn px-3 py-1" href="<?php echo wp_get_attachment_url($pdf_id);?>" target="_blank">
                                <?php echo ($pdf_name) ? $pdf_name : 'Download Pdf';?>
                                </a></p>
                            </div>
                        </div>
                    </li>    
                <?php }
                    endforeach;
                }
            } 
        }
    } else {
        wp_send_json_error('No product found');
    }
    
    $output_string = ob_get_contents();
	ob_end_clean();
    wp_send_json_success($output_string);
	//wp_reset_postdata();
}


add_action('wp_ajax_javad_products_fetch' , 'javad_products_fetch');
add_action('wp_ajax_nopriv_javad_products_fetch','javad_products_fetch');
function javad_products_fetch(){
    $the_query = new WP_Query( 
        array( 
            'posts_per_page' => -1, 
            's' => esc_attr( $_POST['keyword'] ), 
            'post_type' => 'products' 
        ) 
    );
    ob_start();
    if( $the_query->have_posts() ) :
        while( $the_query->have_posts() ): $the_query->the_post(); ?>
            <h2><a href="#" data-product-id="<?php echo get_the_ID();?>" class="searchBox"><?php the_title();?></a></h2>
        <?php endwhile;
    else:
        wp_send_json_error('No data found');
    endif;
    $output_string = ob_get_contents();
	ob_end_clean();
    wp_send_json_success($output_string);
	wp_reset_postdata();
}