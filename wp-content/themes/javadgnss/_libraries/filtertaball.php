<?php
/******News Ajax Action For Pagination/Filter Start********/
  add_action( 'wp_ajax_filter_tab_all', 'filter_tab_all' );
  add_action( 'wp_ajax_nopriv_filter_tab_all', 'filter_tab_all' );

  function filter_tab_all()
  {
    if ($_POST) {
      extract($_POST);
    }
    $paged = 1;
    if( isset($pagenumber) ){
      if($pagenumber):
        $paged = $pagenumber;
      endif;
    }
	  
	 $args = array( 
	'post_type' => 'workshop',
	'post_status' => 'publish',
	'posts_per_page' => 6,
	'paged' => $paged,
	);
	  
	if(isset($keyword) && trim($keyword) !== '')
    {
      $args['s'] = $keyword;
    }
      $loop = new WP_Query( $args );
	  $maxNumPages = $loop->max_num_pages;
      if ( $loop->have_posts() ) :
        while ($loop->have_posts()) : $loop->the_post();
		$post_excerpt = get_the_excerpt($loop->ID);
	  $select_date = get_field('select_date', $loop->ID);
	  $cta_type = get_field('cta_type', $loop->ID);
	  $upload_image = get_field('upload_image', $loop->ID);
	  $cta_btn_class="";
	  if($cta_type=='register'){
		  $cta_btn_class="green_btn";
	  }elseif($cta_type=='available_soon'){
		  $cta_btn_class="gray_btn";
	  }elseif($cta_type=='view_workshop'){
		  $cta_btn_class="gray_btn border_gray";
	  }
	  $posttags = get_the_terms( $loop->ID, 'workshop-tag');
	$i=0;
	if( have_rows('add_cta') ):
	while( have_rows('add_cta') ) : the_row(); $i++;
	if($i==1):
	$cta_label = get_sub_field('cta_label');
	$ctaURL = "javascript:void(0)";
	if($cta_type=='register'){ $ctaURL = get_sub_field('cta_url'); }
	if($cta_type=='view_workshop'){ $ctaURL = get_permalink($loop->ID); }
	endif;
	endwhile; endif;
?>
	  <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 col-xxl-4">
		  <div class="card_grid">
			  <?php if(!empty($upload_image)): ?><div class="card_img">
			  <a href="<?php echo $ctaURL; ?>"><img src="<?php echo $upload_image['url']; ?>" alt="<?php echo $upload_image['alt']; ?>" /></a>
			  </div><?php endif; ?>
			  <div class="card_content">
				  <div class="tag_title">
					 <?php  foreach($posttags as $tag) : ?>
					  <span><?php echo $tag->name; ?></span>
					  <?php endforeach; ?>
				  </div>
				  <h3><a href="<?php echo $ctaURL; ?>"><?php echo get_the_title(); ?></a></h3>
				  <?php if($select_date): ?><h5><?php echo $select_date; ?></h5><?php endif; ?>
				  <?php if( have_rows('add_speakers') ): ?>
				  <ul class="listing_title">
					  <li><strong>Speakers</strong></li>
					  <?php while( have_rows('add_speakers') ) : the_row();
	 					 $speaker_name = get_sub_field('speaker_name'); ?>
					  <li><?php echo $speaker_name; ?></li>
					  <?php endwhile; ?>
				  </ul><?php endif; ?>

				  <p><?php echo $post_excerpt; ?></p>
				  <div class="card_btn">
					  <?php if( have_rows('add_cta') ): ?>
					  <?php while( have_rows('add_cta') ) : the_row();
						  $cta_label = get_sub_field('cta_label');
						  $ctaURL = "javascript:void(0)";
						  if($cta_type=='register'){ $ctaURL = get_sub_field('cta_url'); }
						  if($cta_type=='view_workshop'){ $ctaURL = get_permalink($loop->ID); }
					  ?>
					  <?php if($cta_label): ?><a class="<?php echo $cta_btn_class; ?>" href="<?php echo $ctaURL; ?>"><?php echo $cta_label; ?></a><?php endif; ?>
					  <?php endwhile; ?>
					  <?php endif; ?>
				  </div>
			  </div>
		  </div>
</div>
      <?php endwhile; ?>
<div class="breadcrum_grid">
	<nav class="pagination">
		<?php echo javad_workshop_tab_all_pagination( $loop->max_num_pages,2, $paged ); ?>
	</nav>
</div>
        <?php wp_reset_postdata();
      else: ?>
        <h2><?php echo "No data found! "; ?></h2>
      <?php endif;

    wp_die();
  }
/******News Ajax Action For Pagination/Filter Ends********/

