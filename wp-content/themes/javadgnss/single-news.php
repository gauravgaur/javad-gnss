<?php get_header();

$all_cats = get_the_terms( get_the_ID(), 'news_categories' );
if ( $all_cats[0]->slug == 'news-articles' ) { get_template_part('template_parts/single', 'newsarticles'); }
if ( $all_cats[0]->slug == 'press-release' ) { get_template_part('template_parts/single', 'pressreleases'); }

get_footer(); ?>