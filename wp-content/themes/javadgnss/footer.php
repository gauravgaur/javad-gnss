<?php 
if(is_tax()){
	$termTx = get_queried_object();
	$enable_testimonials = get_field('show_testimonials', $termTx);
} else {
	$enable_testimonials = get_field('show_testimonials');
}
if($enable_testimonials){

	$test_args = array(
		'post_type' =>'testimonials',
		'orderby' => 'rand',
		'posts_per_page' => -1,
	);

	// The Query
	$tst_query = new WP_Query( $test_args );

	// The Loop
	if ( $tst_query->have_posts() ) {?>
<section class="lightGary_bg pt-4 pb-4 mt-5">
	<div class="container">
		<div class="testimonal_silder" data-aos="fade-up" data-aos-duration="1500">
			<h4>Field Tested + Proven</h4>
			<div id="testimonal_silder" class="owl-carousel owl-theme testimonial_wrapper">
				<?php 
		while ( $tst_query->have_posts() ) {
			$tst_query->the_post();?>
				<div class="item">
					<div class="user_info">
						<div class="user_img">
							<?php the_post_thumbnail();?>
						</div>
						<strong><?php the_title();?></strong>
						<div class="scroll_content">
							<?php the_content();?>
						</div>
					</div>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
</section>
<?php wp_reset_postdata();
	}
}?>
</main>
<footer>
	<div class="footer_top">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-5 col-lg-4 col-xl-5">
					<div class="footer_logo">
						<?php if(get_field('footer_logo','option')):
						$image = get_field('footer_logo','option');?>
						<a href="<?php echo home_url( '/' ); ?>">
							<img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
						</a>
						<?php endif;?>
					</div>
				</div>
				<div class="col-sm-12 col-md-7 col-lg-8 col-xl-7">
					<div class="row">
						<?php if(have_rows('menus','option')):while(have_rows('menus','option')):the_row();
						$title = get_sub_field('title');
						$links = get_sub_field('links');
						?>
						<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="footer_link">
								<?php if($title):?>
								<h4><?php echo $title;?></h4>
								<?php endif;
								if(have_rows('links','option')):?>
								<ul>
									<?php while(have_rows('links','option')):the_row();
									$link = get_sub_field('link');
									if( $link ): 
									$link_url = $link['url'];
									$link_title = $link['title'];
									$link_target = $link['target'] ? $link['target'] : '_self';
									?>
									<li><a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a></li>
									<?php endif; ?>
									<?php endwhile;?>
								</ul>
								<?php endif;?>
							</div>
						</div>
						<?php endwhile;endif;
						$headquaters_details = get_field('headquaters_details','option');
						if($headquaters_details):?>
						<div class="col-sm-6 col-md-6 col-lg-3">
							<div class="headquaters">
								<?php if(get_field('headquaters_heading','option')):?>
								<h4><?php echo get_field('headquaters_heading','option');?><h4>
									<?php endif;?>
									<?php echo $headquaters_details;?>
									</div>
									</div>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer_bottom">
				<div class="container">
					<div class="d-flex">
						<?php if(get_field('copyright_text','option')):?>
						<p><?php echo get_field('copyright_text','option');?></p>
						<?php endif;?>
						<div class="d-flex">
							<?php if(have_rows('bottom_footer_links','option')):?>
							<ul class="bottom_links">
								<?php while(have_rows('bottom_footer_links','option')):the_row();
								$link = get_sub_field('link');
								if( $link ): 
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
								?>
								<li><a class="button" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a></li>
								<?php endif; ?>
								<?php endwhile;?>
							</ul>
							<?php endif;?>
							<ul class="footer_social">
								<?php if(get_field('linked_in','option')):?>
								<li><a href="<?php echo get_field('linked_in','option');?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
								<?php endif;
								if(get_field('twitter','option')):?>
								<li><a href="<?php echo get_field('twitter','option');?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
								<?php endif;
								if(get_field('facebook','option')):?>
								<li><a href="<?php echo get_field('facebook','option');?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
								<?php endif;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- Back to top button -->
			<a id="back-button"><i class="fa fa-chevron-up"></i></a>
			<!-- Back to top button End	 -->
			</footer>
		<?php wp_footer(); ?>
	</div>
	</body>
</html>