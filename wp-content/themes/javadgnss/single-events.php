<?php get_header();
$banner_small_heading = get_field('banner_small_heading');
$event_start_date = get_field('event_start_date');
$event_end_date = get_field('event_end_date');
$event_banner_cta = get_field('event_banner_cta');
$info_heading = get_field('info_heading');
$info_copy = get_field('info_copy');
$info_cta = get_field('info_cta');
$content_side_image = get_field('content_side_image');
$event_desc=get_the_excerpt();
$cal_evt_start=date("Ymd", strtotime($event_start_date));
$cal_evt_end=date("Ymd", strtotime($event_end_date));
$single_events_back_cta = get_field('single_events_back_cta', 'option');
?>
<section class="simple_banner">
			<div class="container">
				<div class="banner_content press_banner" data-aos="fade-up" data-aos-duration="1500">
					<?php if($banner_small_heading): ?><span><?php echo $banner_small_heading; ?></span><?php endif; ?>
					<h1><?php echo get_the_title(); ?></h1>
					<p><?php echo date("F j", strtotime($event_start_date)); ?> - <?php echo $event_end_date; ?></p>
					<?php if(!empty($event_banner_cta)): ?><a class="green_btn" href="<?php echo $event_banner_cta['url']; ?>" target="<?php echo $event_banner_cta['target']; ?>"> <?php echo $event_banner_cta['title']; ?> </a><?php endif; ?>
					<ul class="bread_nav">
						<li><a href="/news-events/">Events <i class="fas fa-chevron-right"></i></a></li>
						<li><?php echo get_the_title(); ?></li>
					</ul>
				</div>
			</div>
		</section>

		<section class="press_contents">
			<div class="container">
				<div class="press_information" data-aos="fade-up" data-aos-duration="1500">
					<div class="button_grid">
						<div class="left_btn">
							<a class="green_btn" href="<?php echo $single_events_back_cta['url']; ?>" target="<?php echo $single_events_back_cta['target']; ?>"><i class="fas fa-chevron-left"></i> <?php echo $single_events_back_cta['title']; ?></a>
						</div>
						<div class="right_btnGroup">
							<a class="gray_btn" href="http://www.google.com/calendar/event?action=TEMPLATE&text=<?php echo urlencode(get_the_title()); ?>&dates=<?php echo $cal_evt_start; ?>T010000Z/<?php echo $cal_evt_end; ?>T020000Z&details=<?php echo urlencode($event_desc); ?>&trp=false&sprop=&sprop=name:" target="_blank" rel="nofollow"> + Google Calendar</a>
							
							<a class="gray_btn" download="" title="+ Add to iCalendar" href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0ADTSTART:<?php echo $cal_evt_start; ?>T134500Z%0ADTEND:<?php echo $cal_evt_end; ?>T141500Z%0ASUMMARY:<?php echo rawurlencode(get_the_title()); ?>%0ADESCRIPTION:<?php echo rawurlencode($event_desc); ?>%0AEND:VEVENT%0AEND:VCALENDAR%0A" target="_blank" rel="noopener noreferrer" >+ Add to iCalendar</a>
							
							
							   
						</div>
					</div>

					<div class="inner_design row">
						<div class="col-md-8"><?php echo get_the_content(); ?></div>
						<div class="col-md-4">
							<?php if(!empty($content_side_image)): ?><div class="img_right">
								<img src="<?php echo $content_side_image['url']; ?>" alt="<?php echo $content_side_image['alt']; ?>">
							</div><?php endif; ?>
						</div>
					</div>
					<?php if($info_heading): ?><h4><?php echo $info_heading; ?></h4><?php endif; ?>
					<?php if($info_copy): echo $info_copy; endif; ?>
					<?php if(!empty($info_cta)): ?><a class="green_btn" href="<?php echo $info_cta['url']; ?>"><?php echo $info_cta['title']; ?></a><?php endif; ?>
				</div>
			</div>
		</section>
<?php get_footer(); ?>