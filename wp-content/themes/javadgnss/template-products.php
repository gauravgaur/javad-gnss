<?php
/*Template Name: Products*/
 get_header();
$banner_image = get_field('banner_image_product');
$description = get_field('products_description');
$link = get_field('products_link');
?>
<section class="sub_banner_content">
    <div class="sub_banner_img">
        <?php if($banner_image):?>
        <img src="<?php echo $banner_image['url'];?>" alt="<?php echo $banner_image['alt'];?>">
        <?php endif;?>
    </div>
    <div class="sub_content">
        <div class="container">
            <div class="banner_content" data-aos="fade-up" data-aos-duration="2000">
                <h1>Products</h1>
                <!-- <a class="green_btn" href="javascript:void(0);">Contact Us</a>
                <ul class="bread_nav">
                    <li><a href="javascript:void(0);">APPLICATIONS</a></li>
                    <li><a href="javascript:void(0);">LAND SURVEY</a></li>
                </ul> -->
            </div>
        </div>
    </div>
</section>

<section class="mt-5">
    <div class="container">
        <div class="product_head_grid" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0">
            <?php if($description) echo $description; ?>
            <?php
            if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
<a class="green_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
            <?php endif;?>
        </div>
    </div>
</section>
<?php
 $args = array(
    'taxonomy' => 'categories',
    'orderby' => 'name',
    'order'   => 'ASC',
    'parent' => 0
);

$categories = get_categories($args);
    foreach($categories as $category) {
        $category_image = get_field( 'image', $category);
        $category_link = get_category_link($category->cat_ID);
        ?>
<section class="outer__design product_desgin">
    <div class="container">
        <div class="full_content">
            <div class="oneThird_img">
		<a href="<?php echo $category_link;?>"></a>
                <img src="<?php echo $category_image['url'];?>" alt="<?php echo $category_image['alt'];?>">
                
            </div>
            <div class="overlap_grid">
                <div class="container">
                    <div class="overlap_Content" data-aos="fade-up" data-aos-duration="1500">
                        <h4> <a href="<?php echo $category_link;?>"><?php echo $category->name;?></a></h4>
                        <p><?php echo $category->description;?></p>
                                    <div class="text-right"> <a class="arrow_btn" href="<?php echo $category_link;?>">
                            <span class="linkText">Explore <?php echo $category->name;?> <i class="fas fa-arrow-right"></i></span>
                        </a> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<?php get_footer();?>