<?php
$postID = get_the_ID();
$news_article_start_date = get_field('news_article_start_date');
//$news_article_end_date = get_field('news_article_end_date');
$all_tags = get_the_tags($postID);
$tag_list = wp_get_post_terms($postID, 'news-tag', array("fields" => "all"));
if ( $tag_list ) {
    foreach ( $tag_list as $tag ) {
        $tag_names[] = $tag->name;
    }
}
$news_banner_cta = get_field('banner_cta_news');
$content_side_image_news = get_field('content_side_image_news');
$go_back_cta = get_field('single_events_back_cta', 'option');
$info_heading_news = get_field('info_heading_news');
$info_copy_news = get_field('info_copy_news');
$info_cta_news = get_field('info_cta_news');
$cal_news_start=date("Ymd", strtotime($news_article_start_date));
//$cal_news_end=date("Ymd", strtotime($news_article_end_date));
$news_desc=get_the_excerpt();
?>
<section class="simple_banner">
	<div class="container">
		<div class="banner_content press_banner" data-aos="fade-up" data-aos-duration="1500">
			<?php if(!empty($tag_names)): ?><span><?php echo implode( ', ', $tag_names ); ?></span><?php endif; ?>
			<h1><?php echo get_the_title(); ?></h1>
			<p><?php echo $news_article_start_date; ?></p>
			<?php if(!empty($news_banner_cta)): ?><a class="green_btn" href="<?php echo $news_banner_cta['url']; ?>" target="<?php echo $news_banner_cta['target']; ?>"> <?php echo $news_banner_cta['title']; ?> </a><?php endif; ?>
			<ul class="bread_nav">
				<li><a href="javascript:void(0);">Newsroom <i class="fas fa-chevron-right"></i></a></li>
				<li><?php echo get_the_title(); ?></li>
			</ul>
		</div>
	</div>
</section>
<section class="press_contents">
	<div class="container">
		<div class="press_information" data-aos="fade-up" data-aos-duration="1500">
			<div class="button_grid">
				<div class="left_btn">
					<a class="green_btn" href="<?php echo $go_back_cta['url']; ?>" target="<?php echo $go_back_cta['target']; ?>"><i class="fas fa-chevron-left"></i> <?php echo $go_back_cta['title']; ?></a>
				</div>
				<div class="right_btnGroup">
					<?php if ( get_field( 'hide_add_to_calendar_buttons' ) == 1 ) : ?>
						<?php // Hides Add To Calendar Buttons ?>
					<?php else : ?>
						<a class="gray_btn" href="http://www.google.com/calendar/event?action=TEMPLATE&text=<?php echo urlencode(get_the_title()); ?>&dates=<?php echo $cal_news_start; ?>T010000Z/<?php echo $cal_news_start; ?>T020000Z&details=<?php echo urlencode($news_desc); ?>&trp=false&sprop=&sprop=name:" target="_blank" rel="nofollow"> + Google Calendar</a>
						<a class="gray_btn" download="" title="+ Add to iCalendar" href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0ADTSTART:<?php echo $cal_news_start; ?>T134500Z%0ADTEND:<?php echo $cal_news_start; ?>T141500Z%0ASUMMARY:<?php echo rawurlencode(get_the_title()); ?>%0ADESCRIPTION:<?php echo rawurlencode($news_desc); ?>%0AEND:VEVENT%0AEND:VCALENDAR%0A" target="_blank" rel="noopener noreferrer" >+ Add to iCalendar</a>
					<?php endif; ?>
				</div>
			</div>

			<div class="inner_design row">
				<div class="col-md-8">
					<h3><?php echo get_the_title(); ?></h3>
					<?php echo get_the_content(); ?>
				</div>
				<div class="col-md-4">
					<?php if(!empty($content_side_image_news)): ?>
					<div class="img_right">
						<img src="<?php echo $content_side_image_news['url']; ?>" alt="<?php echo $content_side_image_news['alt']; ?>">
					</div><?php endif; ?>
				</div>
			</div>

			<?php if($info_heading_news): ?><h4><?php echo $info_heading_news; ?></h4><?php endif; ?>
			<?php if($info_copy_news): echo $info_copy_news; endif; ?>

			<?php if(!empty($info_cta_news)): ?><a class="green_btn" href="<?php echo $info_cta_news['url']; ?>" target="<?php echo $info_cta_news['target']; ?>"><?php echo $info_cta_news['title']; ?></a><?php endif; ?>
		</div>
	</div>
</section>