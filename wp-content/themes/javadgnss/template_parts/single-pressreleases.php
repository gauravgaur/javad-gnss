<?php
$press_releases_date = get_field('press_releases_date');
$go_back_cta = get_field('single_events_back_cta', 'option');
$all_cats = get_the_terms( get_the_ID(), 'news_categories' );
$info_heading_news = get_field('info_heading_news');
$info_copy_news = get_field('info_copy_news');
$info_cta_news = get_field('info_cta_news');
?>

<section class="simple_banner">
	<div class="container">
		<div class="banner_content press_banner" data-aos="fade-up" data-aos-duration="1500">
			<span><?php echo $all_cats[0]->name; ?></span>
			<h1><?php echo get_the_title(); ?></h1>
			<p><?php echo $press_releases_date; ?></p>
			<ul class="bread_nav">
				<li><a href="javascript:void(0);">Newsroom <i class="fas fa-chevron-right"></i></a></li>
				<li><?php echo get_the_title(); ?></li>
			</ul>
		</div>
	</div>
</section>

<section class="press_contents">
	<div class="container">
		<div class="press_information" data-aos="fade-up" data-aos-duration="1500">
			<a class="green_btn" href="<?php echo $go_back_cta['url']; ?>" target="<?php echo $go_back_cta['target']; ?>"><i class="fas fa-chevron-left"></i> <?php echo $go_back_cta['title']; ?></a>
			<strong><?php echo $press_releases_date; ?></strong>
			<h3><?php echo get_the_title(); ?></h3>
			<?php echo get_the_content(); ?>
			<?php if($info_heading_news): ?><h4><?php echo $info_heading_news; ?></h4><?php endif; ?>
			<?php if($info_copy_news): echo $info_copy_news; endif; ?>
			<?php if(!empty($info_cta_news)): ?><a class="green_btn" href="<?php echo $info_cta_news['url']; ?>" target="<?php echo $info_cta_news['target']; ?>"><?php echo $info_cta_news['title']; ?></a><?php endif; ?>
		</div>
	</div>
</section>