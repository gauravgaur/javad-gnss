<?php

/***************************************************************
BluePrint Boilerplate functions.php
	TABLE OF CONTENTS
	1. Includes for Specific Purposes
		1a. Add All Custom Post Types, menus, sidebars
		1b. Enqueue all scripts and styles
		1c. Clean up backend and frontend
		1d. Pagination
		1e. Security
	2. Theme Setups
	3. Theme Supports
	4. Custom Code
***************************************************************/

// 1. Includes
//	Please use these files which are grouped by type before adding to main functions.php file

// 1a. Add All Custom Post Types, menus, sidebars
require_once( __DIR__ . '/_includes/functions-register.php');

// 1.b. Enqueue all scripts and styles
require_once( __DIR__ . '/_includes/functions-enqueue.php');

// 1c. Clean up backend and frontend. Generally do not edit
require_once( __DIR__ . '/_includes/functions-clean-up.php');

// 1.d. Pagination
require_once( __DIR__ . '/_includes/functions-pagination.php');

// 1.e. Security
require_once( __DIR__ . '/_includes/functions-security.php');

// 1.f. ACF Sync - Warn if ACF field sync is needed
require_once( __DIR__ . '/_includes/functions-acf-sync.php');

// 1.e. Useful Functions
require_once( __DIR__ . '/_includes/functions-useful-functions.php');

//AQ Resizer
require_once( __DIR__ . '/_includes/aq_resizer.php');

//1.g. Require Featured Image
// Required for valid structured data. Does not need to display on website frontend
add_theme_support('post-thumbnails'); // this needs to be defined before the following include

// 1.h. Filter Files
require_once( __DIR__ . '/_libraries/filternews.php');
require_once( __DIR__ . '/_libraries/filterevents.php');
require_once( __DIR__ . '/_libraries/filtertab.php');
require_once( __DIR__ . '/_libraries/filterworkshop.php');
require_once( __DIR__ . '/_libraries/ajax-product-filters.php');
require_once( __DIR__ . '/_libraries/filtertaball.php');


// 2. Theme setups
add_action('after_setup_theme', 'bp_theme_setup');
function bp_theme_setup(){
    // Internationalization
    load_theme_textdomain( 'bp-theme', TEMPLATEPATH.'/languages' );
    // HTML5 Support
    add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script' ) );
}

// 3. Theme Supports
add_theme_support( "title-tag" );

// 4. Custom Code
/* Hover Module Effect */
function my_acf_admin_head() {
	$theme_folder = '_images/_admin/previews/';
	$root_folder = get_bloginfo('template_url').'/'.$theme_folder;
	$default = $root_folder.'preview_default.jpg';
	$files = scandir(dirname(__FILE__).'/'.$theme_folder);
  $js_array = [];
	if($files){
	  foreach($files as $file){
		if($file != '..' && $file != '.'){
		  $file_explode = explode('.',$file);
		  $js_array[] = '"'.$file_explode[0].'":"'.$root_folder.$file.'"';
		}
	  }
	}
  $js_array_js = implode(',',$js_array);
  ?>
  <style type="text/css">
  .imagePreview { position:absolute; right:100%; top:0px; z-index:999999; border:1px solid #f2f2f2; box-shadow:0px 0px 3px #b6b6b6; background-color:#fff; padding:20px;}
  .imagePreview img { width:300px; height:auto; display:block; }
  .acf-tooltip li:hover { background-color:#0074a9; }
  .acf-tooltip li a {position: relative; overflow: visible;}
  </style>
  <script>
  jQuery(document).ready(function($) {
	  var previewImages = {<?php echo $js_array_js ?>};
	  $('a[data-name=add-layout]').click(function(){
		  waitForEl('.acf-tooltip li', function() {
			  $('.acf-tooltip li a').hover(function(){
				if (typeof previewImages[$(this).attr('data-layout')] !== 'undefined') {
				  $(this).append('<div class="imagePreview"><img src="'+previewImages[$(this).attr('data-layout')]+'"></div>');
				}else {
				  $(this).append('<div class="imagePreview"><img src="<?php echo $default ?>"></div>');
				};
			  }, function(){
				  $('.imagePreview').remove();
			  });
			});
		  })
		  var waitForEl = function(selector, callback) {
			  if (jQuery(selector).length) {
				  callback();
			  } else {
				  setTimeout(function() {
				  waitForEl(selector, callback);
			  }, 100);
		  }
	  };
  })
  </script>
  <?php
  }
  add_action('acf/input/admin_head', 'my_acf_admin_head');

add_filter( 'wpsl_templates', 'custom_templates' );

function custom_templates( $templates ) {
    $templates[] = array (
        'id'   => 'custom',
        'name' => 'Custom template',
        'path' => get_stylesheet_directory() . '/' . 'wpsl-templates/custom.php',
    );

    return $templates;
}

add_filter('wpsl_listing_template', 'wpsl_listing_template_custom');
function wpsl_listing_template_custom(){
	global $wpsl, $wpsl_settings;
    if ( $wpsl_settings['new_window'] ) {
        $new_window = ' target="_blank"';
    } else {
        $new_window = '';
    }
    $listing_template = '<li data-store-id="<%= id %>">' . "\r\n";
    $listing_template .= "\t\t" . '<div class="wpsl-store-location">' . "\r\n";
    $listing_template .= "\t\t\t" . '<div class="title-wpsl"><%= thumb %>' . "\r\n";
    $listing_template .= "\t\t\t\t" . wpsl_store_header_template( 'listing' ) . "\r\n"; 
    $listing_template .= "\t\t\t" . '</p>' . "\r\n";

	// Include the category names.
    $listing_template .= "\t\t\t" . '<% if ( terms ) { %>' . "\r\n";
	$listing_template .= "\t\t\t" . '<p class="wpsl-category"><%= terms %></p>' . "\r\n";
	$listing_template .= "\t\t\t" . '<% } %>' . "\r\n";

    // Show the phone, fax or email data if they exist.
    if ( $wpsl_settings['show_contact_details'] ) {
        $listing_template .= "\t\t\t" . '<p class="wpsl-contact-details">' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( phone ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><%= formatPhoneNumber( phone ) %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% if ( fax ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><strong>' . esc_html( $wpsl->i18n->get_translation( 'fax_label', __( 'Fax', 'wpsl' ) ) ) . '</strong><%= fax %></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
		$listing_template .= "\t\t\t" . '<% if ( url ) { %>' . "\r\n";
        $listing_template .= "\t\t\t" . '<span><a' . $new_window . ' href="<%= url %>">Link to website</a></span>' . "\r\n";
        $listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
        $listing_template .= "\t\t\t" . '</p>' . "\r\n";
    }

    $listing_template .= "\t\t\t" . wpsl_more_info_template() . "\r\n"; // Check if we need to show the 'More Info' link and info
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t\t" . '<div class="wpsl-direction-wrap">' . "\r\n";
	$listing_template .= "\t\t\t" . '<% if ( email ) { %>' . "\r\n";
	$listing_template .= "\t\t\t" . '<a href="mailto:<%= email %>" class="btn btn-success">' . esc_html( $wpsl->i18n->get_translation( 'email_label', __( 'Email', 'wpsl' ) ) ) . '</a>' . "\r\n";
	$listing_template .= "\t\t\t" . '<% } %>' . "\r\n";
    if ( !$wpsl_settings['hide_distance'] ) {
        $listing_template .= "\t\t\t" . '<strong>Distance</strong>: <%= distance %> ' . esc_html( $wpsl_settings['distance_unit'] ) . '' . "\r\n";
    }
    $listing_template .= "\t\t" . '</div>' . "\r\n";
    $listing_template .= "\t" . '</li>';
    return $listing_template;
}

add_filter( 'wpsl_store_meta', 'custom_store_meta', 10, 2 );

function custom_store_meta( $store_meta, $store_id ) {
	$terms = get_the_terms( $store_id, 'wpsl_store_category' );
	$store_meta['terms'] = '';
	if ( $terms ) {
		if ( ! is_wp_error( $terms ) ) {
			if ( count( $terms ) > 1 ) {
				$location_terms = array();
				foreach ( $terms as $term ) {
					$location_terms[] = $term->name;
				}
				$store_meta['terms'] = implode( ', ', $location_terms );
			} else {
				$store_meta['terms'] = $terms[0]->name;
			}
		}
	}
	return $store_meta;
}


add_filter( 'wpsl_info_window_template', 'custom_info_window_template' );

function custom_info_window_template() {
   	$info_window_template = '<div data-store-id="<%= id %>" class="wpsl-info-window">' . "\r\n";
    $info_window_template .= "\t\t" . '<p>' . "\r\n";
    $info_window_template .= "\t\t\t" .  wpsl_store_header_template() . "\r\n";  // Check which header format we use
    $info_window_template .= "\t\t\t" . '<span class="address"><%= address %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% if ( address2 ) { %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span class="address"><%= address2 %></span>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t\t" . '<span class="address">' . wpsl_address_format_placeholders() . '</span>' . "\r\n"; // Use the correct address format
    $info_window_template .= "\t\t" . '</p>' . "\r\n";
    $info_window_template .= "\t\t" . '<% if ( phone ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<span><a href="tel:<%= formatPhoneNumber( phone ) %>"><%= formatPhoneNumber( phone ) %></a></span>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";   
    $info_window_template .= "\t\t" . '<% if ( email ) { %>' . "\r\n";
    $info_window_template .= "\t\t" . '<span><a href="mailto:<%= formatEmail( email ) %>"><%= formatEmail( email ) %></a></span>' . "\r\n";
    $info_window_template .= "\t\t" . '<% } %>' . "\r\n";
    $info_window_template .= "\t\t" . '<%= createInfoWindowActions( id ) %>' . "\r\n";
    $info_window_template .= "\t" . '</div>';   
    return $info_window_template;
}

function wpsl_get_countries() {
	$countries = array("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");
	return $countries;
}

function javad_product_filters_shortcode($atts, $content = null) {
  extract(shortcode_atts(array(
   'categories' => 'all',
   'count' => 4,
  ), $atts));
  ob_start();
  
  $product_categories = get_terms( array( 
    'taxonomy' => 'categories',
    'hide_empty' => false,
    'include' => $categories
  ) );?>
  <style>
    #productsSearch .product_items, .productTagItems li{
      padding-bottom: 20px;	
      cursor:pointer;
      border-bottom: 2px solid transparent;
    }
    #productsSearch .product_items.active,#productsSearch .product_items:hover, .productTagItems li.active,  .productTagItems li:hover {
      border-bottom: 2px solid #8EC74C;
    }
    #searchProduct{
      position:relative;
    }
    #productsFetched{
      position: absolute;
      padding: 10px 20px 10px 10px ;
      background: #fff;
      width: 100%;
      left: 0px;
      visibility:hidden;
      transition:width 2s, height 4s;
      opacity: 0;
      transition-delay: 0s, 0s, 0.3s;
      transition: all 0.3s ease-in-out 0s, visibility 0s linear 0.3s, z-index 0s linear 0.01s;
      height: 200px;
      overflow-x: auto;
    }
    #productsFetched.active{
      visibility:visible;
      opacity: 1;
    }
    #productsFetched h2{
      font-size: 18px;
      line-height: 26px;
      border-bottom: 1px solid #8EC74C;
      padding: 5px 0;
    }
    #productsFetched h2 a{
      color:#8EC74C;
    }
    .search_product .form_group {
      position: relative;
    }
    .search_product .form_group input {
      width: 100%;
      height: 55px;
      color: #000000;
      font-size: 18px;
      padding: 5px 45px 5px 15px;
      outline: none;
      border: 6px solid #54544c;
      border-radius: 10px;
      transition-duration: 0.4s;
    }
    .search_product .form_group .search_btn {
      position: absolute;
      bottom: 0;
      top: 0;
      right: 0;
      z-index: 555;
      outline: none;
      background-color: transparent;
      border: none;
      color: #8EC74C;
      font-size: 20px;
      padding: 8px 12px;
    }
    .search_product .form_group input:focus {
      border-color: #8EC74C;
    }
    .file_items.active{
      display:block !important;
    }
    .file_items.hidden{
      display:none !important;
    }
    
.loadingImg{
	position:absolute;
	top:15px;
	right:45px;
	display:none;
	background:url(/wp-content/uploads/2022/03/loading.gif);
	width:24px;
	height:24px;
}
#closeLists {
    position: absolute;
    top: 4px;
    right: 6px;
    color: #fff;
    font-size: 20px;
    width: 25px;
    height: 25px;
    line-height: 20px;
    text-align: center;
    background: #8EC74C;
    border-radius: 50%;
    cursor:pointer;
}
.user_profile_gallery{
	position:relative;
}
.ajaxLoading{
	position:absolute;
    top:0px;
    width:100%;
    background:rgba(255,255,255,0.6);
    left:0px;
    display:none;
    bottom:0px;
}
.ajaxLoading .loader{
    width: 100px;
    height: 100px;
    display: inline-block;
    padding: 0px;
    position: absolute;
    top: 50px;
    left: 0px;
    right: 0px;
    margin: auto;
}
.ajaxLoading .loader span {
   position:absolute;
   display:inline-block;
   width:100px;
   height:100px;
   border-radius:100%;
   background:#8EC74C;
   -webkit-animation:loader3 1.5s linear infinite;
   animation:loader3 1.5s linear infinite;
}
.ajaxLoading .loader span:last-child {
   animation-delay:-0.9s;
   -webkit-animation-delay:-0.9s;
}
.resetSearch{
	position: absolute;
    right: 55px;
    top: 10px;
    color: #000;
    font-size: 20px;
    font-weight: bold;
    width: 30px;
    height: 30px;
    line-height: 30px;
    text-align: center;
    cursor: pointer;
}
@keyframes loader3 {
   0% {transform: scale(0, 0);opacity:0.8;}
   100% {transform: scale(1, 1);opacity:0;}
}
@-webkit-keyframes loader3 {
   0% {-webkit-transform: scale(0, 0);opacity:0.8;}
   100% {-webkit-transform: scale(1, 1);opacity:0;}
}

  </style>
  <div class="support_min">
    <div class="row" id="productsSearch">
      <?php foreach($product_categories as $term):
        $image = get_field('illustrated_image', $term);?>
        <div class="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 col-xxl-2">
          <div class="product_items" data-product-cat-id="<?php echo $term->term_id;?>" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0">
            <div class="services_icon">
              <img src="<?php echo $image['url'];?>" alt="<?php echo $term->name;?> Image">
            </div>
            <h5><?php echo $term->name;?></h5>
          </div>
        </div>
      <?php endforeach;?>
    </div>
    <div class="search_product">
      <h4>Search For Product</h4> 
      <div id="searchProduct">
        <div class="form_group">
          <input type="text" name="keyword" id="searchProductField" data-searched-pro-id="" placeholder="SEARCH" autocomplete="off" onkeyup="fetchProducts()">
          <button class="search_btn"><i class="fas fa-search"></i></button>
        </div>
        <span class="loadingImg" style="display:none;"></span>
        <div id="productsFetched">Search results will appear here</div>
      </div>
      <div class="">        
        <ul class="icons_content productTagItems">
          <?php 
            //$group = get_field('files_list');
            //print_r( $group);
          ?>
          <li data-type="user_manual_group">
            <div class="icons_grid">
              <img src="https://www.javadgnss.com/wp-content/uploads/2022/03/User_Manuals.png" alt="User Manuals Image">
            </div>
            <h6>User Manuals</h6>
          </li>
          <li data-type="data_sheets_group">
            <div class="icons_grid">
              <img src="https://www.javadgnss.com/wp-content/uploads/2022/03/Data_Sheets.png" alt="Data Sheets Image">
            </div>
            <h6>Data Sheets</h6>
          </li>
          <li data-type="software_updates_group">
            <div class="icons_grid">
              <img src="https://www.javadgnss.com/wp-content/uploads/2022/03/Software_Updates.png" alt="Software Updates Image">
            </div>
            <h6>Software Updates</h6>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <?php 
  // WP_Query arguments
  $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

  $args = array(
    'post_type'              => array( 'products' ),
    'posts_per_page'         => -1,
    'order'                  => 'DESC',
    'orderby'                => 'id',
    'ignore_sticky_posts'=>true,
    'fields'=>'ids'
  );

  // The Query
  $query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {?>
  <div class="user_profile_gallery">
    <ul>
      <?php 
      $product_ids = $query->posts;
      $count=0;
      foreach($product_ids as $proId){
        if(is_array(get_field('files_list', $proId))){
          $get_files = array_filter(get_field('files_list', $proId)); 
        }
        
        if(!empty($get_files)){
          foreach($get_files as $key => $file) {
            foreach( $file as $sub_file):
              $count++;
              
              ?>
            <li class="file_items" data-type="<?php echo $key;?>" data-index="<?php echo $count;?>">
              <div class="main_box">
                <div class="user_head">
                  <div class="cross_img">
                    <?php if(has_post_thumbnail($proId)):?>
					  	<?php $imageUrl = get_the_post_thumbnail_url($proId, 'full');?>
                        <img src="<?php echo $imageUrl;?>" alt="<?php echo get_the_title($proId);?>" />
					  <?php else:?>
                    	<img src="http://javadgnss.com/wp-content/uploads/2022/03/jdrop.png" alt="img">
					  <?php endif;?>
                  </div>              
                  <span>
                      <?php 
                      $pdf_id = '';
                      $pdf_name ='';
                      if($key == 'data_sheets_group'){
                          echo 'Data Sheet';
                          $pdf_name = $sub_file['data_sheets'];
                          $pdf_id = $sub_file['data_sheets_file_upload'];
                      } elseif($key == 'user_manual_group'){
                          echo 'User Manual';
                          $pdf_name = $sub_file['user_manual'];
                          $pdf_id = $sub_file['user_manual_file_upload'];
                      } else {
                          echo 'Software Updates';
                          $pdf_name = $sub_file['software_updates'];
                          $pdf_id = $sub_file['software_updates_file_upload'];
                      }?>
                  </span>
                </div>
                <div class="about_user">
                  <h4><?php echo get_the_title($proId);?></h4>
                  <p><a class="btn green_btn px-3 py-1" href="<?php echo wp_get_attachment_url($pdf_id);?>" target="_blank">
                  <?php echo ($pdf_name) ? $pdf_name : 'Download Pdf';?>
                  </a></p>
                </div>
              </div>
            </li>    
            <?php 
            endforeach;
          }
        }      
      }?>
    </ul>
    <div class="ajaxLoading">
    	<div class="loader">
            <span></span>
            <span></span>
        </div>
    </div>
      <div class="text-center mt-4 loaderToggle">
        <a href="javascript:void(0)" class="green_btn btnLoadMore">Load More</a>
      </div>
  </div>
  <?php } else {?>
	  <p> Nothing found! </p>
  <?php }
  $output_string = ob_get_contents();
	ob_end_clean();
  //wp_reset_postdata();
	return $output_string;
	
}

add_shortcode('javad_products_filters', 'javad_product_filters_shortcode');


add_filter('wpsl_gmap_js', 'wpsl_gmap_js_override');
function wpsl_gmap_js_override($url){
  return get_stylesheet_directory_uri().'/_js/wpsl-gmap.js';
}

add_filter('wpsl_sql', 'wpsl_sql_override', 90);
function wpsl_sql_override($sql){
  global $wpdb, $wpsl;
  $args = $_GET;
  // Check if we need to filter the results by category.
  if ( isset( $args['filter'] ) && $args['filter'] ) {
      $filter_ids = array_map( 'absint', explode( ',', $args['filter'] ) );
      $cat_filter = "INNER JOIN $wpdb->term_relationships AS term_rel ON posts.ID = term_rel.object_id
                     INNER JOIN $wpdb->term_taxonomy AS term_tax ON term_rel.term_taxonomy_id = term_tax.term_taxonomy_id
                            AND term_tax.taxonomy = 'wpsl_store_category'
                            AND term_tax.term_id IN (" . implode( ',', $filter_ids ) . ")";
  } else {
      $cat_filter = '';
  }

  $countryJoin = $countryClause = '';
  if($args['countryName']){
    $countryName = $args['countryName'];
    $countryJoin = " INNER JOIN $wpdb->postmeta AS post_country ON post_country.post_id = posts.ID AND post_country.meta_key = 'wpsl_country' ";
    $countryClause = " AND post_country.meta_key = 'wpsl_country' AND post_country.meta_value = '".$countryName."'";
  }

  $zipJoin = $zipClause = '';
  if($args['zipCode']){
    $zipCode = $args['zipCode'];
    $zipJoin = " INNER JOIN $wpdb->postmeta AS post_zip ON post_zip.post_id = posts.ID AND post_zip.meta_key = 'wpsl_zip' ";
    $zipClause = " AND post_zip.meta_key = 'wpsl_zip' AND post_zip.meta_value = '".$zipCode."'";
  }

  if ( $wpsl->i18n->wpml_exists() ) {
      $group_by = 'GROUP BY lat';
  } else {
      $group_by = 'GROUP BY posts.ID';
  }

  if ( isset( $args['autoload'] ) && $args['autoload'] ) {
      $limit = '';
      if ( $wpsl_settings['autoload_limit'] ) {
          $limit = 'LIMIT %d';
      }
      if($args['countryName']){ 
        $sql_sort = 'ORDER BY posts.ID '. $limit;
      } else {
        $sql_sort = 'ORDER BY distance '. $limit;
      }
  } else {
      if($args['countryName']){
        $sql_sort = 'ORDER BY posts.ID '. $limit;
      } else {
        $sql_sort = 'HAVING distance < %d ORDER BY distance LIMIT 0, %d';
      }
  }
 
      
  $sql = "SELECT post_lat.meta_value AS lat,
    post_lng.meta_value AS lng,
    posts.ID,
    ( %d * acos( cos( radians( %s ) ) * cos( radians( post_lat.meta_value ) ) * cos( radians( post_lng.meta_value ) - radians( %s ) ) + sin( radians( %s ) ) * sin( radians( post_lat.meta_value ) ) ) )
    AS distance
    FROM $wpdb->posts AS posts
    INNER JOIN $wpdb->postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'wpsl_lat'
    INNER JOIN $wpdb->postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'wpsl_lng'
    $countryJoin
    $zipJoin
    $cat_filter 
    WHERE posts.post_type = 'wpsl_stores'
    AND posts.post_status = 'publish' 
    $countryClause
    $zipClause
    $group_by $sql_sort";
  return $sql;
}




// add_filter('wpsl_sql_placeholder_values','wpsl_sql_placeholder_values_override', 11, 1);
// function wpsl_sql_placeholder_values_override($placeholder_values){
// 	// WP_Query arguments
// 	if(!empty($_GET['zipCode'])){
// 		$args = array(
// 			'post_type'              => array( 'wpsl_stores' ),
// 			'posts_per_page'         => '1',
// 			'meta_query' => array(
// 				'relation' => 'OR',
// 				array(
// 					'key'     => 'wpsl_zip',
// 					'value'   => $_GET['zipCode'],
// 					'compare' => '=',
// 				),
// 			)
// 		);
// 		$query = new WP_Query( $args );
// 		if ( $query->have_posts() ) {
// 			while ( $query->have_posts() ) {
// 				$query->the_post();
// 				$latitude = get_post_meta(get_the_ID(), 'wpsl_lat', true);
// 				$longitude = get_post_meta(get_the_ID(), 'wpsl_lng', true);
// 				$placeholder_values[1] = $latitude;
// 				$placeholder_values[2] = $longitude;
// 				$placeholder_values[3] = $latitude;
// 			}
// 		}
// 		wp_reset_postdata();
// 	}
// 	return $placeholder_values;
// }


// add_action('wp_ajax_nopriv_wpsl_fetch_lat_lng', 'wpsl_fetch_lat_lng');
// add_action('wp_ajax_wpsl_fetch_lat_lng', 'wpsl_fetch_lat_lng');
// function wpsl_fetch_lat_lng(){
// 	global $wpdb;
// 	if(!empty($_POST['zipcode'])){
// 		$args = array(
// 			'post_type'              => array( 'wpsl_stores' ),
// 			'posts_per_page'         => '1',
// 			'meta_query' => array(
// 				'relation' => 'OR',
// 				array(
// 					'key'     => 'wpsl_zip',
// 					'value'   => $_POST['zipcode'],
// 					'compare' => '=',
// 				),
// 			)
// 		);
// 		$query = new WP_Query( $args );
// 		if ( $query->have_posts() ) {
// 			while ( $query->have_posts() ) {
// 				$query->the_post();
// 				$latitude = get_post_meta(get_the_ID(), 'wpsl_lat', true);
// 				$longitude = get_post_meta(get_the_ID(), 'wpsl_lng', true);
// 			}
// 		}
// 		wp_reset_postdata();
		
// 		wp_send_json_success(array( 'lat' => $latitude, 'lng' => $longitude ) );
// 	}	
// }

add_action('wp_footer','add_tocart_redirect');
function add_tocart_redirect(){?>
<script>
  jQuery(function($) {
    $("#TLSPlus1W").on("click", function (e) {
      e.preventDefault();
      $.post(
          "https://www.javad.com/dynamic/Shop/AddToCurrentBasketFromTemplate/?UUID=1cca12d6-8958-4f57-b5e0-4d530fe1809e&Editable=0"
          ,function () {
            window.location.replace("/dynamic/Shop/ShowCartStatic");
          }
      );
    });

    $("#TLSPlus35W").on("click", function (e) {
      e.preventDefault();
      $.post(
          "https://www.javad.com/dynamic/Shop/AddToCurrentBasketFromTemplate/?UUID=57fff368-3a8e-4e55-92be-61532d16b189&Editable=0"
          ,function () {
            window.location.replace("/dynamic/Shop/ShowCartStatic");
          }
      );
    });

    $("#TLSPlusSpread").on("click", function (e) {
      e.preventDefault();
      $.post(
          "https://www.javad.com/dynamic/Shop/AddToCurrentBasketFromTemplate/?UUID=02df0a13-6e2e-49bf-92ec-065b0c2798e8&Editable=0"
          ,function () {
            window.location.replace("/dynamic/Shop/ShowCartStatic");
          }
      );
    });

    $("#TLSPlus1W-portable").on("click", function (e) {
      e.preventDefault();
      $.post(
          "https://www.javad.com/dynamic/Shop/AddToCurrentBasketFromTemplate/?UUID=b88de22f-9df5-454a-9c9d-d83bef97c692&Editable=0"
          ,function () {
            window.location.replace("/dynamic/Shop/ShowCartStatic");
          }
      );
    });

    $("#TLSPlus35W-portable").on("click", function (e) {
      e.preventDefault();
      $.post(
          "https://www.javad.com/dynamic/Shop/AddToCurrentBasketFromTemplate/?UUID=aa73f7bb-bfb5-40a6-acd6-ef7c3ad90863&Editable=0"
          ,function () {
            window.location.replace("/dynamic/Shop/ShowCartStatic");
          }
      );
    });

    $("#TLSPlusSpread-portable").on("click", function (e) {
      e.preventDefault();
      $.post(
          "https://www.javad.com/dynamic/Shop/AddToCurrentBasketFromTemplate/?UUID=8628fde9-ca80-4e14-9466-775a5498ae9c&Editable=0"
          ,function () {
            window.location.replace("/dynamic/Shop/ShowCartStatic");
          }
      );
    });

  });
</script>
<?php 
}


/* Hook meta box to just the 'place' post type. */
add_action( 'add_meta_boxes_product', 'products_add_meta_boxes' );

/* Creates the meta box. */
function products_add_meta_boxes( $post ) {

  add_meta_box(
    'product-parent',
    __( 'Products', 'javadgnss' ),
    'product_parent_meta_box',
    $post->post_type,
    'side',
    'core'
  );
}

/* Displays the meta box. */
function product_parent_meta_box( $post ) {

  $parents = get_posts(
    array(
      'post_type'   => 'products',
      'orderby'     => 'title',
      'order'       => 'ASC',
      'numberposts' => -1
    )
  );

  if ( !empty( $parents ) ) {

    echo '<select name="parent_id" class="widefat">'; // !Important! Don't change the 'parent_id' name attribute.

    foreach ( $parents as $parent ) {
      printf( '<option value="%s"%s>%s</option>', esc_attr( $parent->ID ), selected( $parent->ID, $post->post_parent, false ), esc_html( $parent->post_title ) );
    }

    echo '</select>';
  }
}


add_filter( 'wpsl_js_settings', 'custom_js_settings' );
function custom_js_settings( $settings ) {
    $settings['startMarker'] = '';
    $settings['excludeStartFromCluster'] = true;
    return $settings;
}

add_filter('acf/format_value/type=page_link', 'remove_domain_from_page_link', 1, 1);
function remove_domain_from_page_link($value, $post_id, $field) {
  if (stripos($value, 'javadgnss.wpengine.com') === false) {
    return $value;
  }
  $value = preg_replace('#^https?://[^/]*#', '', $value);
  return $value;
}



add_filter( 'wpsl_meta_box_fields', 'custom_meta_box_fields' );

function custom_meta_box_fields( $meta_fields ) {
    
    $meta_fields[__( 'Additional Information', 'wpsl' )] = array(
        'fname' => array(
          'label' => __( 'First name', 'wpsl' )
        ),
        'lname' => array(
          'label' => __( 'Last name', 'wpsl' )
        ),
        'phone' => array(
            'label' => __( 'Tel', 'wpsl' )
        ),
        'fax' => array(
            'label' => __( 'Fax', 'wpsl' )
        ),
        'email' => array(
            'label' => __( 'Email', 'wpsl' )
        ),
        'url' => array(
            'label' => __( 'Url', 'wpsl' )
        )        
    );

    return $meta_fields;
}


add_filter( 'wpsl_frontend_meta_fields', 'custom_frontend_meta_fields' );

function custom_frontend_meta_fields( $store_fields ) {

    $store_fields['wpsl_fname'] = array( 
        'name' => 'fname',
        'type' => 'text'
    );
    $store_fields['wpsl_lname'] = array( 
      'name' => 'lname',
      'type' => 'text'
    );

    return $store_fields;
}


add_action( 'import_location_json', 'import_store_locator_data_hook' );
//if(isset($_REQUEST['import'])){
  //import_store_locator_data_hook();
  //add_action('init', 'import_store_locator_data_hook');
//}
function import_store_locator_data_hook(){ 
  $json_data = json_decode( file_get_contents( 'https://www.javad.com/dynamic/Locator/Search?CountryIndex=&LocationCountryIndex=RU&Range=100000&Units=Km&HQ=0&Lat=55.882613&Lng=37.4059081' ) );
  insert_or_update( $json_data );
}

function insert_or_update($json_data) {
  global $wpdb;	
	if ( ! $json_data)
  return false;
  $companies = $json_data->Companies;
  if(count($companies) >= 1){
    foreach($companies as $company){
      if($company->id){
        $args = array(
          'meta_query' => array(
            array(
              'key' => 'wpsl_store_id',
              'value' => $company->id
            )
          ),
          'post_type' => 'wpsl_stores',
          'post_status' => array('publish'),
        );


        $store = get_posts( $args );
        $store_id = '';

        //check if store already exists
        if ( $store ){
          
          $store_id = $store[0]->ID;

        } else {
          //insert store
          $update_store = array(
            'post_title' => $company->name,
            'post_content' => $company->description,
            'post_type' => 'wpsl_stores',
            'post_status'=>'publish'
          );

          $store_id = wp_insert_post( $update_store );
        
        }

        if ( $store_id ) {
          update_post_meta( $store_id, 'wpsl_store_id', $company->id );
          update_post_meta( $store_id, 'wpsl_store_type', $company->type );
          update_post_meta( $store_id, 'wpsl_lat', $company->coords->lat );
          update_post_meta( $store_id, 'wpsl_lng', $company->coords->lon );
          update_post_meta( $store_id, 'wpsl_email', $company->contact->email );
          update_post_meta( $store_id, 'wpsl_phone', $company->contact->phone );
          update_post_meta( $store_id, 'wpsl_fax', $company->contact->fax );
          update_post_meta( $store_id, 'wpsl_url', $company->contact->website );
          update_post_meta( $store_id, 'wpsl_fname', $company->contact->firstName );
          update_post_meta( $store_id, 'wpsl_lname', $company->contact->lastName );
          update_post_meta( $store_id, 'wpsl_country', $company->address->country );
          update_post_meta( $store_id, 'wpsl_country_iso', $company->address->countryISO );
          update_post_meta( $store_id, 'wpsl_zip', $company->address->zipcode );
          update_post_meta( $store_id, 'wpsl_state', $company->address->stateprovince );
          update_post_meta( $store_id, 'wpsl_city', $company->address->city );
          update_post_meta( $store_id, 'wpsl_address', $company->address->street );
          
          if(count($company->Markets) >= 1){
            foreach($company->Markets as $cat){
              $term = term_exists($cat->name, 'wpsl_store_category');
              if ( ! $term ) {
                $term = wp_insert_term( $cat->name, 'wpsl_store_category' );
              }
              wp_set_post_terms( $store_id,  array($term['term_id']) , 'wpsl_store_category', true );
            }
          }
        }
      }
    }    
  }
}

function wpa_course_post_link( $post_link, $id = 0 ){
  $post = get_post($id);  
  if ( is_object( $post ) ){
    $terms = wp_get_object_terms( $post->ID, 'categories' );
    if( $terms ){
      return str_replace( '%category%' , $terms[0]->slug , $post_link );
    }
  }
  return $post_link;  
}
add_filter( 'post_type_link', 'wpa_course_post_link', 1, 3 );



function callback_relative_url($buffer) {
  // Replace normal URLs
  $home_url = esc_url(home_url('/'));
  $home_url_relative = wp_make_link_relative($home_url);

  // Replace URLs in inline scripts
  $home_url_escaped = str_replace('/', '\/', $home_url);
  $home_url_escaped_relative = str_replace('/', '\/', $home_url_relative);

  $buffer = str_replace($home_url, $home_url_relative, $buffer);
  $buffer = str_replace($home_url_escaped, $home_url_escaped_relative, $buffer);

  return $buffer;
}

function buffer_start_relative_url() { ob_start('callback_relative_url'); }
function buffer_end_relative_url() { if (ob_get_length()) @ob_end_flush(); }

// http://codex.wordpress.org/Plugin_API/Action_Reference
add_action('registered_taxonomy', 'buffer_start_relative_url');
add_action('shutdown', 'buffer_end_relative_url');



// function change_link( $post_link, $id = 0 ) {
//   $post = get_post( $id );
//   if( $post->post_type == 'products' ) 
//   {
//      if ( is_object( $post ) ) {
//         # assume that 'categories' is slug of your taxonomy 
//         $terms = wp_get_object_terms( $post->ID, array('categories') );
//         if ( $terms ) {
//            return str_replace( '%category%', $terms[0]->slug, $post_link );
//        }
//     }
//   }
//   return   $post_link ;
// }
// add_filter( 'post_type_link', 'change_link', 1, 3 ); 

//  function generated_rewrite_rules() {
//      add_rewrite_rule(
//          '^products/(.*)/(.*)/?$',
//          'index.php?post_type=products&name=$matches[2]',
//          'top'
//      );

//   }
//   add_action( 'init', 'generated_rewrite_rules' ); 





// Breadcrumbs
function custom_breadcrumbs() {
       
  // Settings
  $separator          = '<i class="fas fa-chevron-right"></i>';
  $breadcrums_id      = 'breadcrumbs';
  $breadcrums_class   = 'breadcrumbs';
  $home_title         = 'Homepage';
    
  // If you have any custom post types with custom taxonomies, put the taxonomy name below (e.g. product_cat)
  $custom_taxonomy    = 'categories';
     
  // Get the query & post information
  global $post,$wp_query;
     
  // Do not display on the homepage
  if ( !is_front_page() ) {
     
      // Build the breadcrums
      echo '<ul id="' . $breadcrums_id . '" class="' . $breadcrums_class . '">';
         
      // Home page
      echo '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
      echo '<li class="separator separator-home"> ' . $separator . ' </li>';
         
      if ( is_archive() && !is_tax() && !is_category() && !is_tag() ) {
            
          echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . post_type_archive_title($prefix, false) . '</strong></li>';
            
      } else if ( is_archive() && is_tax() && !is_category() && !is_tag() ) {
            
          // If post is a custom post type
          $post_type = get_post_type();
            
          // If it is a custom post type display name and link
          if($post_type != 'post') {
                
              $post_type_object = get_post_type_object($post_type);
              $post_type_archive = get_post_type_archive_link($post_type);
            
              echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              echo '<li class="separator"> ' . $separator . ' </li>';
            
          }
            
          $custom_tax_name = get_queried_object()->name;
          echo '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . $custom_tax_name . '</strong></li>';
            
      } else if ( is_single() ) {
            
          // If post is a custom post type
          $post_type = get_post_type();
            
          // If it is a custom post type display name and link
          if($post_type != 'post') {
                
              $post_type_object = get_post_type_object($post_type);
              $post_type_archive = get_post_type_archive_link($post_type);
            
              echo '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . $post_type_object->labels->name . '">' . $post_type_object->labels->name . '</a></li>';
              echo '<li class="separator"> ' . $separator . ' </li>';
            
          }
            
          // Get post category info
          $category = get_the_category();
           
          if(!empty($category)) {
            
              // Get last category post is in
              $last_category = end(array_values($category));
                
              // Get parent any categories and create array
              $get_cat_parents = rtrim(get_category_parents($last_category->term_id, true, ','),',');
              $cat_parents = explode(',',$get_cat_parents);
                
              // Loop through parent categories and store in variable $cat_display
              $cat_display = '';
              foreach($cat_parents as $parents) {
                  $cat_display .= '<li class="item-cat">'.$parents.'</li>';
                  $cat_display .= '<li class="separator"> ' . $separator . ' </li>';
              }
           
          }
            
          // If it's a custom post type within a custom taxonomy
          $taxonomy_exists = taxonomy_exists($custom_taxonomy);
          if(empty($last_category) && !empty($custom_taxonomy) && $taxonomy_exists) {
                 
            $primary_cat_id = get_post_meta($post->id,'_yoast_wpseo_primary_' . $custom_taxonomy, true);            
            if($primary_cat_id){
              $primary_cat = get_term($primary_cat_id, $custom_taxonomy);
              if(isset($primary_cat->name)) {
                $cat_id         = $primary_cat->term_id;
                $cat_nicename   = $primary_cat->slug;
                $cat_link = home_url('products').'/'.$cat_nicename;
                $cat_name       = $primary_cat->name;
              }                  
            } else {
              $taxonomy_terms = get_the_terms( $post->ID, $custom_taxonomy );
              $cat_id         = $taxonomy_terms[0]->term_id;
              $cat_nicename   = $taxonomy_terms[0]->slug;
              //$cat_link       = get_term_link($taxonomy_terms[0]->term_id, $custom_taxonomy);
              $cat_link = home_url('products').'/'.$cat_nicename;
              $cat_name       = $taxonomy_terms[0]->name;
            } 
          }
            
          // Check if the post is in a category
          if(!empty($last_category)) {
              echo $cat_display;
              echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                
          // Else if post is in a custom taxonomy
          } else if(!empty($cat_id)) {
                
              echo '<li class="item-cat item-cat-' . $cat_id . ' item-cat-' . $cat_nicename . '"><a class="bread-cat bread-cat-' . $cat_id . ' bread-cat-' . $cat_nicename . '" href="' . $cat_link . '" title="' . $cat_name . '">' . $cat_name . '</a></li>';
              echo '<li class="separator"> ' . $separator . ' </li>';
              echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
            
          } else {
                
              echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
                
          }
            
      } else if ( is_category() ) {
             
          // Category page
          echo '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
             
      } else if ( is_page() ) {
             
          // Standard page
          if( $post->post_parent ){
                 
              // If child page, get parents 
              $anc = get_post_ancestors( $post->ID );
                 
              // Get parents in the right order
              $anc = array_reverse($anc);
                 
              // Parent page loop
              if ( !isset( $parents ) ) $parents = null;
              foreach ( $anc as $ancestor ) {
                  $parents .= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
                  $parents .= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
              }
                 
              // Display parent pages
              echo $parents;
                 
              // Current page
              echo '<li class="item-current item-' . $post->ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
                 
          } else {
                 
              // Just display current page if not parents
              echo '<li class="item-current item-' . $post->ID . '"><strong class="bread-current bread-' . $post->ID . '"> ' . get_the_title() . '</strong></li>';
                 
          }
             
      } else if ( is_tag() ) {
             
          // Tag page
             
          // Get tag information
          $term_id        = get_query_var('tag_id');
          $taxonomy       = 'post_tag';
          $args           = 'include=' . $term_id;
          $terms          = get_terms( $taxonomy, $args );
          $get_term_id    = $terms[0]->term_id;
          $get_term_slug  = $terms[0]->slug;
          $get_term_name  = $terms[0]->name;
             
          // Display the tag name
          echo '<li class="item-current item-tag-' . $get_term_id . ' item-tag-' . $get_term_slug . '"><strong class="bread-current bread-tag-' . $get_term_id . ' bread-tag-' . $get_term_slug . '">' . $get_term_name . '</strong></li>';
         
      } elseif ( is_day() ) {
             
          // Day archive
             
          // Year link
          echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
          echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
             
          // Month link
          echo '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link( get_the_time('Y'), get_the_time('m') ) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
          echo '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
             
          // Day display
          echo '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
             
      } else if ( is_month() ) {
             
          // Month Archive
             
          // Year link
          echo '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link( get_the_time('Y') ) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
          echo '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
             
          // Month display
          echo '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
             
      } else if ( is_year() ) {
             
          // Display year archive
          echo '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
             
      } else if ( is_author() ) {
             
          // Auhor archive
             
          // Get the author information
          global $author;
          $userdata = get_userdata( $author );
             
          // Display author name
          echo '<li class="item-current item-current-' . $userdata->user_nicename . '"><strong class="bread-current bread-current-' . $userdata->user_nicename . '" title="' . $userdata->display_name . '">' . 'Author: ' . $userdata->display_name . '</strong></li>';
         
      } else if ( get_query_var('paged') ) {
             
          // Paginated archives
          echo '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">'.__('Page') . ' ' . get_query_var('paged') . '</strong></li>';
             
      } else if ( is_search() ) {
         
          // Search results page
          echo '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
         
      } elseif ( is_404() ) {
             
          // 404 page
          echo '<li>' . 'Error 404' . '</li>';
      }
     
      echo '</ul>';
         
  }
     
}

// Disable Gutenberg on the back end.
add_filter( 'use_block_editor_for_post', '__return_false' );