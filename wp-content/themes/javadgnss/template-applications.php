<?php
/*Template Name: Applications*/
get_header();?>
<section class="sub_banner_content">
    <?php
    $banner_image = get_field('applications_banner_image');
    if($banner_image):?>
			<div class="sub_banner_img">
				<img src="<?php echo $banner_image['url'];?>" alt="applications banner image">
			</div>
            <?php endif;?>
			<div class="sub_content">
				<div class="container">
					<div class="banner_content" data-aos="fade-up" data-aos-duration="2000">
                        <?php if(get_field('applications_banner_title')):?>
						<h1><?php echo get_field('applications_banner_title');?></h1>
                        <?php endif;?>
					</div>
				</div>
			</div>
		</section>

		<section class="mt-5">
			<div class="container">
				<div class="content_title" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0">
                    <?php if(get_field('applications_description')) echo get_field('applications_description');?>
				</div>
			</div>
		</section>
        <?php 

$args = array(  
	'post_type' => 'applications',
	'post_status' => 'publish',
	'posts_per_page' => -1,  
	
	);

$loop = new WP_Query( $args );
        if ($loop->have_posts()) :
            $i = 0;
            while ($loop->have_posts()) :
				$loop->the_post();?>                    
		<section class="full_content <?php if($i%2 == 0) echo ' flip_strip clearfix';?>">
			<div class="oneThird_img">
		<a href="<?php echo get_the_permalink();?>"></a>
                <?php the_post_thumbnail();?>
			</div>
			<div class="overlap_grid">
				<div class="container">
					<div class="overlap_Content" data-aos="fade-up" data-aos-duration="1500">
						<h4><a href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></h4>
                        <?php if(get_field('sub_title',get_the_ID())):?>
						<h5><?php echo get_field('sub_title',get_the_ID());?></h5>
                        <?php endif;
                        the_excerpt();
                        $button_title = get_field('button_title',get_the_ID())
                        ?>
						<a class="arrow_btn" href="<?php echo get_the_permalink();?>">
							<span class="linkText"><?php echo ($button_title) ?  $button_title : 'Explore'.get_the_title(); ?> <i class="fas fa-arrow-right"></i></span>
						</a>
					</div>
				</div>
			</div>
		</section>
        <?php $i++; endwhile;  wp_reset_postdata(); endif;?>
		<!--<section class="lightGary_bg pt-4 pb-4 mt-5">
			<div class="container">
				<div class="testimonal_silder" data-aos="fade-up" data-aos-duration="1500">
					<h4>Field Tested + Proven</h4>
					<div id="testimonal_silder" class="owl-carousel owl-theme">
				<?php if(have_rows('testimonials')):
				while(have_rows('testimonials')):the_row();
				$image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                ?>
                <div class="item">
                    <div class="user_info">
                        <div class="user_img">
                            <?php if($image):?>
                            <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
                            <?php endif;?>
                        </div>
                        <strong><?php echo $title;?></strong>
                        <div class="scroll_content">
                            <?php echo $description;?>
                        </div>
                    </div>
                </div>
                <?php endwhile;endif;?>
					</div>
				</div>
			</div>
		</section>-->
<?php get_footer();?>