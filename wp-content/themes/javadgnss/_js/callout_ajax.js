////*****AJAX function for News Starts*****//
function filternews(paged = 1) {
  jQuery.ajax({
    type: 'POST',
    url: "/wp-admin/admin-ajax.php",
    data: {
      action: 'filter_news',
      pagenumber: paged,
    },
    beforeSend: function() {
    	jQuery("#loading-container").addClass("show_loader");
    },
    success: function(data) {
      jQuery(".load_news_html").html(data);
      jQuery("#loading-container").removeClass("show_loader");
      jQuery('html, body').animate({
        scrollTop: (jQuery(".load_news_html").offset().top) - (jQuery(".header").height() + 150)
      }, 500);
    }
  });
}
//*****AJAX function for News Ends*****//


//////*****AJAX function for Events Starts*****//
function filterevents(paged = 1) {
  jQuery.ajax({
    type: 'POST',
    url: "/wp-admin/admin-ajax.php",
    data: {
      action: 'filter_events',
      pagenumber: paged,
    },
    beforeSend: function() {
    	jQuery("#loading-container").addClass("show_loader");
    },
    success: function(data) {
      jQuery(".load_events_html").html(data);
      jQuery("#loading-container").removeClass("show_loader");
      jQuery('html, body').animate({
        scrollTop: (jQuery(".load_events_html").offset().top) - (jQuery(".header").height() + 150)
      }, 500);
    }
  });
}
//*****AJAX function for Events Ends*****//

//////*****AJAX function for Workshop Starts*****//
function filterworkshop(paged = 1) { 
 var search_workshop = jQuery('#search_workshop').val();
  jQuery.ajax({
    type: 'POST',
    url: "/wp-admin/admin-ajax.php",
    data: {
      action: 'filter_workshop',
      pagenumber: paged,
	  keyword: search_workshop,
    },
    beforeSend: function() {
    	jQuery("#loading-container").addClass("show_loader");
    },
    success: function(data) {
      jQuery(".load_workshop_html").html(data);
      jQuery("#loading-container").removeClass("show_loader");
      jQuery('html, body').animate({
        scrollTop: (jQuery(".load_workshop_html").offset().top) - (jQuery(".header").height() + 150)
      }, 500);
    }
  });
}
//*****AJAX function for Workshop Ends*****//
 jQuery(document.body).on('click', '.filter_tab', function(e) {
	 
	 e.preventDefault();
  var catID = jQuery(this).attr('data-catid'); 
  var paged = jQuery(this).attr('data-pagenum');


  var filterkeyword = jQuery('#search_workshop').val();
  jQuery.ajax({
    type: 'POST',
    url: "/wp-admin/admin-ajax.php",
    data: {
      action: 'filter_tab',
	  cat_ID: catID,
	  keyword: filterkeyword,
      pagenumber: paged,
    },
    beforeSend: function() {
    //jQuery("#loading-container").addClass("show_loader");
    },
    success: function(data) {
      jQuery(".load_workshop_html").html(data);
     // jQuery("#loading-container").removeClass("show_loader");
      jQuery('html, body').animate({
        scrollTop: (jQuery(".load_workshop_html").offset().top) - (jQuery(".header").height() + 150)
      }, 500);
    }
  });
 })

//*****AJAX function for Workshop Ends*****//
 jQuery(document.body).on('click', '.filter_tab_all', function(e) {
	 e.preventDefault();
  var paged = jQuery(this).attr('data-pagenum');  
  var filterkeyword = jQuery('#search_workshop').val();
  jQuery.ajax({
    type: 'POST',
    url: "/wp-admin/admin-ajax.php",
    data: {
      action: 'filter_tab_all',
	  keyword: filterkeyword,
      pagenumber: paged,
    },
    beforeSend: function() {
    //jQuery("#loading-container").addClass("show_loader");
    },
    success: function(data) {
      jQuery(".load_workshop_html").html(data);
     // jQuery("#loading-container").removeClass("show_loader");
      jQuery('html, body').animate({
        scrollTop: (jQuery(".load_workshop_html").offset().top) - (jQuery(".header").height() + 150)
      }, 500);
    }
  });
 })

