// ===== header ==== 

jQuery(function($) {
	var shrinkHeader = 65;
	// Class add remove on load with scroll value
	var scroll_add = getCurrentScroll();
	if ( scroll_add >= shrinkHeader ) {
		$('.header').addClass('shrink');
	}
	else {
		$('.header').removeClass('shrink');
	}
	// Scrolling
	$(window).scroll(function() {
		var scroll = getCurrentScroll();
		if ( scroll >= shrinkHeader ) {
			$('.header').addClass('shrink');
		}
		else {
			$('.header').removeClass('shrink');
		}
	});

	function getCurrentScroll() {
		return window.pageYOffset || document.documentElement.scrollTop;
	}

	var btn = $('#back-button');

	$(window).scroll(function() {
		if ($(window).scrollTop() > 300) {
			btn.addClass('show');
		} else {
			btn.removeClass('show');
		}
	});

	btn.on('click', function(e) {
		e.preventDefault();
		$('html, body').animate({scrollTop:0}, '300');
	});

});


jQuery("document").ready(function($){

	AOS.init({once:true})

	jQuery(document).on("click", ".menuLink > ul > li > a span", function(event){
		if($(window).width() < 993) {
			event.preventDefault();
			jQuery(this).parent().toggleClass("activeLink");
			jQuery(this).parent().next('.dropdown_menu').slideToggle();

			if(jQuery(this).hasClass('active-tab')){
				jQuery(this).removeClass('active-tab');
			}else {
				jQuery(this).addClass('active-tab');
			}
		}
	});

	var $owl = $('#testimonal_silder');

	$owl.children().each( function( index ) {
		$(this).attr( 'data-position', index ); 
	});

	$owl.owlCarousel({
		center: true,
		loop: true,
		items: 3,
		dots: false,
		nav: false,
		touchDrag: false,
		mouseDrag: false,
		autoplay: false,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
			},
			484: {
				items: 2,
			},
			850: {
				items: 2,
			},
			1024: {
				items: 3,
			},
			1250: {
				items: 3,
			}
		}
	});

	jQuery(document).on('click', '.owl-item>div', function() {
		var $speed = 300;  // in ms
		$owl.trigger('to.owl.carousel', [$(this).data( 'position' ), $speed] );
	});

	hideForLoadmore();

	jQuery(document).on('click','#productsSearch .product_items', function(){
		if(jQuery(this).hasClass('active')){
			jQuery(this).removeClass('active');
		} else {
			jQuery('#productsSearch .product_items').removeClass('active');
			jQuery(this).addClass('active');
		}    
		var cat_id = jQuery('#productsSearch .product_items.active').attr('data-product-cat-id'),
			tag_id = jQuery('.productTagItems li.active').attr('data-type'),
			product_id = jQuery('#searchProductField').attr('data-searched-pro-id');
		//console.log(cat_id);
		if(!cat_id)
			cat_id = '';
		if(!tag_id){
			tag_id = 'all';
		}
		ajaxProductSearch(cat_id, tag_id, product_id);
	})

	jQuery(document).on('click','.productTagItems li', function(){
		if(jQuery(this).hasClass('active')){
			jQuery(this).removeClass('active');
		} else {
			jQuery('.productTagItems li').removeClass('active');
			jQuery(this).addClass('active');
		} 
		var tag_id = jQuery('.productTagItems li.active').attr('data-type'),
			cat_id = jQuery('#productsSearch .product_items.active').attr('data-product-cat-id'),
			product_id = jQuery('#searchProductField').attr('data-searched-pro-id');
		if(!tag_id)
			tag_id = 'all';
		if(!cat_id){
			cat_id = '';
		}  
		ajaxProductSearch(cat_id, tag_id, product_id, 1);
	})	

	jQuery(document).on('click', '.btnLoadMore', function(e) {
		e.preventDefault();
		var width = jQuery(window).width(),
		loadMoreNum = 8;
		console.log(width);

		if(width >= 1367){
			loadMoreNum = 10;
		}
		jQuery(".user_profile_gallery .file_items:hidden").slice(0, parseInt(loadMoreNum)).removeClass('d-none');
		if(jQuery(".user_profile_gallery ul .file_items:hidden").length == 0) {
			jQuery(".btnLoadMore").hide();
		}
	});  

	jQuery(document).on('click', '.searchBox', function(e){
		e.preventDefault();
		if(jQuery('#searchProduct .resetSearch').length == 0){
			jQuery('#searchProduct').append('<span class="resetSearch">x</span>');
		}
		var pTitle = jQuery(this).text(),
			pId = jQuery(this).attr('data-product-id'),
			tag_id = jQuery('.productTagItems li.active').attr('data-type'),
			cat_id = jQuery('#productsSearch .product_items.active').attr('data-product-cat-id');
		if(!tag_id){
			tag_id = 'all';
		}
		if(pTitle){
			jQuery('#searchProductField').val(pTitle);
			jQuery('#searchProductField').attr('data-searched-pro-id', pId);
			jQuery('#productsFetched').removeClass('active');            
		}
		ajaxProductSearch(cat_id, tag_id, pId);
	})

	jQuery(document).on('click', '.timeLine_tabs .prev_btn', function() {
		jQuery('.nav li > .active').parent().prev('li').find('button').trigger('click');
	});

	jQuery(document).on('click', '.timeLine_tabs .next_btn', function() {
		jQuery('.nav li > .active').parent().next('li').find('button').trigger('click');
	});

	jQuery(document).on('click', '#closeLists', function(e){
		jQuery("#productsFetched").removeClass('active');
	});

	jQuery(document).on('click', '#searchProduct .resetSearch', function(e){
		jQuery(this).remove();
		jQuery("#searchProductField").val('');
		var tag_id = jQuery('.productTagItems li.active').attr('data-type'),
			cat_id = jQuery('#productsSearch .product_items.active').attr('data-product-cat-id'),
			pId = '';
		if(!tag_id){
			tag_id = 'all';
		}
		ajaxProductSearch(cat_id, tag_id, pId);
	});    
});

function fetchProducts(){
	var keyword = jQuery('#searchProductField').val();
	//if(keyword.length >= 2){
	jQuery.ajax({
		url: ajax_object.ajaxurl,
		type: 'post',
		data: { action: 'javad_products_fetch', keyword: keyword},
		beforeSend: function() {
			jQuery('#searchProduct .loadingImg').show();
		},
		success: function(response) {
			if(response.success){
				jQuery('#searchProduct .loadingImg').hide();
				jQuery('#productsFetched').addClass('active');  
				jQuery('#productsFetched').html( response.data );
				jQuery('#productsFetched').append('<span id="closeLists">x</span>');
			}   
		}
	});
	//} else {
	// jQuery('#productsFetched').removeClass('active');
	//}
}

//=====   Search Bar animation ========
// function searchToggle(obj, evt){
// 	var container = jQuery(obj).closest('.search-wrapper');
// 	if(!container.hasClass('active')){
// 		container.addClass('active');
// 		evt.preventDefault();
// 	}
// 	else if(container.hasClass('active') && jQuery(obj).closest('.input-holder').length == 0){
// 		container.removeClass('active');
// 		// clear input
// 		container.find('.search-input').val('');
// 	}
// }

// ================================================
jQuery("a[href ^= '#']").click(function(e) {
	e.preventDefault();
	var hrefHash = jQuery(this).attr("href"),
	is_modal = jQuery(this).attr('data-bs-toggle');
	if(is_modal != 'modal'){ 
		jQuery('html, body').animate({
			scrollTop: jQuery(hrefHash).offset().top - 150
		}, 500);
	}
});

//===== Close Search Bar animation ========

function ajaxProductSearch(cat_id='', tag_id='', p_id){
	jQuery.ajax({ 
		url: ajax_object.ajaxurl,
		type: "POST",
		dataType:"json",
		beforeSend: function() {
			jQuery('.user_profile_gallery .ajaxLoading').show();
		},
		data: {
			action: 'javad_product_search_filter',
			cat_id: cat_id,
			tag_id: tag_id,
			product_id : p_id
		},
		success: function(response) {        	
			jQuery('html, body').animate({
				scrollTop: jQuery(".user_profile_gallery").offset().top - 100
			}, 500);
			if(response.success == true){
				jQuery('.user_profile_gallery ul').html(response.data);
			} else {
				jQuery('.user_profile_gallery ul').html(response.data);
			} 
			jQuery('.user_profile_gallery .ajaxLoading').hide();
			hideForLoadmore();
		}
	});
}



function hideForLoadmore(){
	//console.log('triggered');
	var width = jQuery(window).width(),
		loadMoreNum = 8;
	console.log(width);

	if(width >= 1367){
		loadMoreNum = 10;
	}

	if(jQuery('.user_profile_gallery').length){
		var totalCount = jQuery( ".user_profile_gallery ul li").length;
		if(totalCount < loadMoreNum){
			jQuery('.btnLoadMore').hide();
		} else {
			jQuery('.btnLoadMore').show();
		}
		console.log(totalCount);
		jQuery( ".user_profile_gallery ul li").each(function( index ) {
			if(index >= loadMoreNum){
				jQuery(this).addClass('d-none');
			}
		})
	}
}