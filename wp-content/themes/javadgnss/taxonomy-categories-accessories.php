<?php get_header();
$term = get_queried_object();
$banner_image = get_field('banner_image',$term);
$termchildren = get_term_children( $term->term_id, 'categories' ); 
?>
<section class="sub_banner_content">
    <div class="sub_banner_img">
        <?php if($banner_image):?>
        <img src="<?php echo $banner_image['url'];?>" alt="<?php echo $banner_image['alt'];?>">
        <?php endif;?>
    </div>
    <div class="sub_content">
        <div class="container">
            <div class="banner_content" data-aos="fade-up" data-aos-duration="2000">
                <h1><?php echo $term->name;?></h1>
                <ul class="bread_nav">
                      <!-- <li><a class ="text-uppercase"href="<?php echo get_page_link(54); ?>"><?php echo get_post_type();?> <i class="fas fa-chevron-right"></i></a></li> <li class="text-uppercase"><?php echo get_the_title();?></li> -->
                      <li class="text-uppercase cat-breadcrumbs"><?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb( '</p><a href="#" id="breadcrumbs">', '</a><p>' );}?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="line_grid">
    <div class="container">
        <div class="product_inner_info" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
            <h4><?php echo $term->description;?></h4>
        </div>
    </div>
</section>
<?php
foreach ( $termchildren as $child ) {
    $term = get_term_by( 'id', $child, 'categories' );
    ?>
<section class="line_grid">
    <div class="container">
        <div class="product_inner_info">
            <div data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="300" data-aos-offset="0">
                <h3><?php echo $term->name;?></h3>
            </div>
            <div class="row">
                <?php
                    $args = array( 
                        'post_type' => 'products',
                        'showposts' => -1,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'categories',
                                'terms' => $child,
                                'field' => 'term_id',
                            )
                        ),
                        'orderby' => 'title',
                        'order' => 'ASC' );
                    
                        $the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) {
        $the_query->the_post();
                ?>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-3 col-xxl-3 mt-0">
                    <div class="see_product mt-3" data-aos="fade-up" data-aos-duration="1000">
                        <div class="product_title_info">
                            <a class="arrow_btn" href="<?php echo get_field('external_product_link',get_the_ID());?>">
                                <strong><?php echo get_the_title();?></strong>
                            </a>
                        </div>
                    </div>
                </div>
            <?php }
         }?>
            </div>
        </div>
    </div>
</section>
<?php }?>

<!-- <section class="mb-5">
    <div class="container">
        <div class="row">
            <?php 
                $buttons = get_field('buttons_with_heading','option');
                if($buttons):
                foreach($buttons as $button):
                    foreach($button as $single_button):
                        $title = $single_button['title'];
                        $link = $single_button['link'];
                        $full_width = $single_button['full_width'];
            ?>
            <div class="col-sm-12 <?php echo ($full_width) ? 'col-md-12' : 'col-md-6';?>">
                <div class="view_box dark_gray_bg" data-aos="fade-up" data-aos-duration="1800">
                    <?php if($title):?>
                    <h4><?php echo $title;?></h4>
                    <?php endif;
                    if( $link ): 
                        $link_url = $link['url'];
                        $link_title = $link['title'];
                        $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                    <a class="arrow_btn" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                        <span class="linkText"><?php echo esc_html( $link_title ); ?> <i class="fas fa-arrow-right"></i></span>
                    </a>
                    <?php endif;?>
                </div>
            </div>
            <?php endforeach; endforeach;endif;?>
        </div>
    </div>
</section>

<?php $cards = get_field('cards_slider','option');?>
		<section class="lightGary_bg pt-4 pb-4 mt-5">
			<div class="container">
				<div class="testimonal_silder" data-aos="fade-up" data-aos-duration="1500">
					<h4>Field Tested + Proven</h4>
					<div id="testimonal_silder" class="owl-carousel owl-theme">
						<?php if($cards):
						foreach($cards as $cardss):
						foreach($cardss as $card):
						$image = $card['image'];
                $title = $card['title'];
                $description = $card['description'];
                ?>
                <div class="item">
                    <div class="user_info">
                        <div class="user_img">
                            <?php if($image):?>
                            <img src="<?php echo $image['url'];?>" alt="<?php echo $image['alt'];?>">
                            <?php endif;?>
                        </div>
                        <strong><?php echo $title;?></strong>
                        <div class="scroll_content">
                            <?php echo $description;?>
                        </div>
                    </div>
                </div>
                <?php endforeach;endforeach;endif;?>
					</div>
				</div>
			</div>
		</section> -->
        <section class="mb-5">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="view_box dark_gray_bg" data-aos="fade-up" data-aos-duration="1800">
							<h4>View All Applications</h4>
							<a class="arrow_btn" href="javascript:void(0);">
								<span class="linkText">Explore Applications <i class="fas fa-arrow-right"></i></span>
							</a>
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="view_box dark_gray_bg" data-aos="fade-up" data-aos-duration="2000">
							<h4>View all Products</h4>
							<a class="arrow_btn" href="javascript:void(0);">
								<span class="linkText">Explore Applications <i class="fas fa-arrow-right"></i></span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		
<!--		<section class="lightGary_bg pt-4 pb-4 mt-5">
			<div class="container">
				<div class="testimonal_silder" data-aos="fade-up" data-aos-duration="1500">
					<h4>Field Tested + Proven</h4>
					<div id="testimonal_silder" class="owl-carousel owl-theme">
						<div class="item">
							<div class="user_info">
								<div class="user_img">
									<img src="<?php echo get_template_directory_uri();?>/_images/user_img.png" alt="user_img">
								</div>
								<strong>Name Surname</strong>
								<div class="scroll_content">
									<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="user_info">
								<div class="user_img">
									<img src="<?php echo get_template_directory_uri();?>/_images/user_img.png" alt="user_img">
								</div>
								<strong>Name Surname</strong>
								<div class="scroll_content">
									<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="user_info">
								<div class="user_img">
									<img src="<?php echo get_template_directory_uri();?>/_images/user_img.png" alt="user_img">
								</div>
								<strong>Name Surname</strong>
								<div class="scroll_content">
									<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="user_info">
								<div class="user_img">
									<img src="<?php echo get_template_directory_uri();?>/_images/user_img.png" alt="user_img">
								</div>
								<strong>Name Surname</strong>
								<div class="scroll_content">
									<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
<?php get_footer();?>